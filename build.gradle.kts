import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.nativeplatform.platform.internal.DefaultNativePlatform

plugins {
    java
    idea
    `java-library`
    `maven-publish`
    id("io.freefair.lombok") version "6.3.0"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.openjfx.javafxplugin") version "0.1.0"
}

group = "com.runemate"
version = "1.32.3-SNAPSHOT"

val docs: Configuration by configurations.creating
val runemate: Configuration by configurations.creating {
    configurations["compileOnly"].extendsFrom(this)
    configurations["testCompileOnly"].extendsFrom(this)
    configurations["testImplementation"].extendsFrom(this)

    attributes {
        val os = DefaultNativePlatform.getCurrentOperatingSystem().toFamilyName()
        val arch = DefaultNativePlatform.getCurrentArchitecture().name

        attribute(OperatingSystemFamily.OPERATING_SYSTEM_ATTRIBUTE, objects.named(OperatingSystemFamily::class, os))
        attribute(MachineArchitecture.ARCHITECTURE_ATTRIBUTE, objects.named(MachineArchitecture::class, arch))
        attribute(Usage.USAGE_ATTRIBUTE, objects.named(Usage::class, Usage.JAVA_RUNTIME))
    }
}

repositories {
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenCentral()
    mavenLocal()
}

dependencies {
    val bom = platform("com.runemate:runemate-client-bom:4.11.3.0")
    runemate(bom)

    runemate("com.runemate:runemate-client")
    runemate("com.google.code.gson:gson")
    runemate("org.jetbrains:annotations")
    runemate("com.google.guava:guava")
    runemate("org.apache.commons:commons-lang3")
    runemate("org.apache.commons:commons-math3:")
    runemate("org.apache.commons:commons-text")
    runemate("commons-io:commons-io")
    runemate("com.fasterxml.jackson.module:jackson-module-kotlin")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    runemate("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")

    docs(bom)
    docs(group = "com.runemate", name = "runemate-client", classifier = "javadoc")

    implementation("org.json:json:20211205")
    implementation("org.jblas:jblas:1.2.5")

    testImplementation("commons-cli:commons-cli:1.9.0")
    testImplementation("com.fasterxml.jackson.datatype:jackson-datatype-guava:2.18.2")

    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

val javafxVersion = "22"

javafx {
    version = javafxVersion
    configuration = "runemate"
    modules("javafx.base", "javafx.fxml", "javafx.controls", "javafx.media", "javafx.web", "javafx.graphics", "javafx.swing")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withJavadocJar()
}

gradle.taskGraph.whenReady {
    if (hasTask("launch")) {
        tasks.withType<Javadoc>().forEach { it.enabled = false }
    }
}

tasks {
    withType<Test>() {
        useJUnitPlatform()
    }

    withType<JavaCompile> {
        options.encoding = "UTF-8"
    }

    withType<Javadoc> {
        exclude("**/README.md")
        title = "RuneMate Game API $version"
        source = sourceSets.main.get().allJava

        options {
            showFromPublic()
            encoding = "UTF-8"
            (this as CoreJavadocOptions).addStringOption("Xdoclint:none", "-quiet")
        }
    }

    register<Jar>("testJar") {
        group = "build"
        from(sourceSets.test.get().output)
        archiveClassifier.set("test")
    }

    register<Copy>("combinedJavadoc") {
        dependsOn("javadoc")
        from(layout.projectDirectory.file(".gitlab/index.html"))
        from(zipTree(docs.singleFile)) {
            into("runemate-client")
        }
        from(layout.buildDirectory.dir("docs/javadoc")) {
            into("runemate-game-api")
        }
        destinationDir = file(layout.buildDirectory.dir("public"))
    }

    register<JavaExec>("launch") {
        group = "runemate"
        classpath = files(runemate, shadowJar)
        mainClass.set("com.runemate.client.boot.Boot")

        arrayOf(
                "java.base/java.lang.reflect",
                "java.base/java.nio",
                "java.base/sun.nio.ch",
                "java.base/java.util.regex",
                "java.base/java.util.concurrent",
        ).forEach {
            jvmArgs("--add-opens=${it}=ALL-UNNAMED")
        }
    }

    register<JavaExec>("generateDataClasses") {
        group = "generate"
        classpath = sourceSets.test.get().output +
                sourceSets.main.get().output +
                configurations.testRuntimeClasspath.get()
        mainClass = "com.runemate.test.cache.DataGenerator"
    }

    register<JavaExec>("generateItemVariations") {
        group = "generate"
        classpath = sourceSets.test.get().output +
                sourceSets.main.get().output +
                configurations.testRuntimeClasspath.get()
        mainClass = "com.runemate.test.cache.GenerateItemVariations"
    }

    register<JavaExec>("translateWildernessCalculation") {
        group = "other"
        classpath = sourceSets.test.get().output +
                sourceSets.main.get().output +
                configurations.testRuntimeClasspath.get()
        mainClass = "com.runemate.test.WildernessCS2Translator"
    }

    processResources {
        val tokens = mapOf(
                "runemate.version" to project.version
        )

        inputs.properties(tokens)

        filesMatching("runemate-game-api.version") {
            filter(ReplaceTokens::class, "tokens" to tokens)
            filteringCharset = "UTF-8"
        }

        filesMatching("**/*.fxml") {
            filter { it.replace(Regex("http://javafx.com/javafx/([0-9.]+)"), "http://javafx.com/javafx/$javafxVersion") }
            filteringCharset = "UTF-8"
        }
    }
}

val externalRepositoryUrl: String by project
val externalRepositoryCredentialsName: String? by project
val externalRepositoryCredentialsValue: String? by project

publishing {
    publications {
        register<MavenPublication>("maven") {
            from(components["java"])
        }
    }

    repositories.maven {
        name = "external"
        url = uri(externalRepositoryUrl)
        if (externalRepositoryCredentialsValue != null) {
            credentials(HttpHeaderCredentials::class) {
                name = externalRepositoryCredentialsName ?: "Private-Token"
                value = externalRepositoryCredentialsValue
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
