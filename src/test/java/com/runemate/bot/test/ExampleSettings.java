package com.runemate.bot.test;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.open.*;
import java.time.*;
import java.util.*;

@SettingsGroup(group = "group1")
public interface ExampleSettings extends Settings {

    @SettingsSection(title = "Section title", description = "Section description", order = 0)
    String customSection = "customSection";

    @DependsOn(group = "group1", key = "testCheckbox", value = "true")
    @SettingsSection(title = "Section title2", description = "Section description", order = 2)
    String customSection2 = "customSection2";

    @Setting(key = "testCheckbox", title = "Test Checkbox")
    default boolean testCheckbox() {
        return true;
    }

    @Suffix(Suffix.PERCENT)
    @Range(max = 10)
    @Setting(key = "testIntSpinner", title = "Test Integer")
    default int testIntSpinner() {
        return 1;
    }

    @Setting(key = "testIntSpinner", title = "Test Integer")
    void setIntSpinner(int value);

    @Setting(key = "testDoubleSpinner", title = "Test Double")
    default double testDoubleSpinner() {
        return 1.15;
    }

    @Setting(key = "testText", title = "Test TextField", section = customSection2)
    @DependsOn(group = "group1", key = "testIntSpinner", value = "[2-4]")
    default String testText() {
        return "Default Text";
    }

    @Setting(key = "testTextMultiline", title = "Test TextField", disabled = true, section = customSection2)
    @Multiline
    default String testTextMultiline() {
        return "Default Text";
    }

    @Setting(key = "testPassword", title = "Test Password 1111111111111", secret = true, section = customSection2)
    default String testPassword() {
        return "Default Text";
    }

    @Setting(key = "playerPosition", title = "Player Position", converter = CoordinateSettingConverter.class, section = customSection)
    default Coordinate testCoordinate() {
        return null;
    }

    @Setting(key = "testState", title = "Test Enum", section = customSection, hidden = true)
    default AbstractBot.State testEnum() {
        return AbstractBot.State.RESTARTING;
    }

    @Setting(key = "testEquipment", title = "Test Equipment", section = customSection, converter = EquipmentLoadout.SettingConverter.class)
    default EquipmentLoadout loadout() {
        return new EquipmentLoadout();
    }

    @Setting(key = "testDuration2", title = "Test Duration", section = customSection)
    default Duration duration() {
        return Duration.ZERO;
    }

    @Setting(key = "testCheckbox2", title = "Test Checkbox")
    default boolean testCheckbox2() {
        return true;
    }

    @Setting(key = "coordinate", title = "My Coordinate", converter = CoordinateSettingConverter.class)
    default Coordinate coordinateSetting() {
        return null;
    }

}
