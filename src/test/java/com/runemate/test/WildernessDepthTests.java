package com.runemate.test;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.extern.log4j.*;
import org.junit.jupiter.api.*;

@Log4j2(topic = "WildernessDepthTests")
public class WildernessDepthTests {

    @Test
    void testWildernessDepth() {
        Coordinate c = new Coordinate(3060, 9970, 0);
        int level = Wilderness.getDepth(c);
        log.info("Level of {} is {}", c, level);

        c = new Coordinate(3418, 10066, 0);
        level = Wilderness.getDepth(c);
        log.info("Level of {} is {}", c, level);

        c = new Coordinate(3224, 10129, 0);
        level = Wilderness.getDepth(c);
        log.info("Level of {} is {}", c, level);
    }

}
