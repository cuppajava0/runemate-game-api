package com.runemate.test;

import java.util.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;

public class WildernessCS2Translator {
    private static final Pattern COORD = Pattern.compile("(?<plane>[0-4])_(?<regionX>\\d{1,3})_(?<regionY>\\d{1,3})_(?<x>\\d{1,2})_(?<y>\\d{1,2})");
    private static final Pattern IN_ZONE = Pattern.compile("~inzone\\((?<coord1>[0-9_]+), (?<coord2>[0-9_]+), coord\\) = (?<test>\\d)");
    private static final Pattern SCALE = Pattern.compile("scale\\((?<expression>.+), (?<div>\\d+), (?<scale>\\d+)\\)");

    private static final List<String> AREA_CONSTANTS = new ArrayList<>();

    public static void main(String... args) {

        String input = """
            def_int $int0 = 0;
            if (~inzone(0_52_62_0_0, 3_54_64_63_63, coord) = 1 & ~inzone(0_53_63_21_21, 3_53_63_42_42, coord) = 0) {
            	$int0 = 5;
            } else if (~inzone(0_46_55_0_0, 3_52_67_63_63, coord) = 1) {
            	$int0 = calc((coordz(coord) - 55 * 64) / 8 + 1);
            } else if (~inzone(0_47_158_0_0, 3_47_158_63_63, coord) = 1) {
            	$int0 = calc((coordz(coord) - 155 * 64) / 8 - 1);
            } else if (~inzone(0_51_159_0_0, 3_51_159_63_63, coord) = 1) {
            	$int0 = 35;
            } else if (~inzone(0_53_159_0_0, 3_53_159_63_63, coord) = 1) {
            	$int0 = 35;
            } else if (~inzone(0_52_161_0_0, 3_52_161_63_63, coord) = 1) {
            	$int0 = 40;
            } else if (~inzone(0_27_180_0_0, 3_27_180_63_63, coord) = 1) {
            	$int0 = 21;
            } else if (~inzone(0_29_180_0_0, 3_29_180_63_63, coord) = 1) {
            	$int0 = 21;
            } else if (~inzone(0_25_180_0_0, 3_25_180_63_63, coord) = 1) {
            	$int0 = 29;
            } else if (~inzone(0_52_160_0_0, 3_52_160_63_63, coord) = 1) {
            	$int0 = calc(33 + scale(calc(coordz(coord) % 64 - 6), 50, 7));
            } else if (~inzone(0_46_155_0_0, 3_53_169_63_63, coord) = 1) {
            	$int0 = calc((coordz(coord) - 155 * 64) / 8 + 1);
            }
            return($int0);
            
            """;

        StringJoiner result = new StringJoiner(System.lineSeparator());
        input.lines().forEachOrdered(in -> {
            String line = in.replaceAll("\\$int0", "level");
            line = replaceScale(line);
            line = removeAllCalcWrappers(line);
            line = line.replaceAll("coordz\\(coord\\)", "position.getY()");
            line = line.replaceAll("return\\(level\\)", "return (short) level");
            line = line.replaceAll("def_int level", "int level");
            line = line.replaceAll("calc", "");
            line = replaceZoneChecks(line);

            result.add(line);
        });

        AREA_CONSTANTS.forEach(area -> {
            String name = "WILDERNESS_AREA_" + AREA_CONSTANTS.indexOf(area);
            System.out.println("private static final Area " + name + " = " + area + ";");
        });

        System.out.println();
        System.out.println(result);
    }

    private static String removeAllCalcWrappers(String input) {
        StringBuilder result = new StringBuilder();
        int currentIndex = 0;

        while (currentIndex < input.length()) {
            // Find the next occurrence of "calc("
            int startIndex = input.indexOf("calc(", currentIndex);

            if (startIndex == -1) {
                // No more "calc(" found; append the remainder of the string
                result.append(input.substring(currentIndex));
                break;
            }

            // Append everything before "calc(" to the result
            result.append(input, currentIndex, startIndex);

            // Start parsing right after "calc("
            int openParens = 0;
            int parsingIndex = startIndex + 5; // skip "calc("
            StringBuilder content = new StringBuilder();

            // Find the matching closing parenthesis, tracking nesting
            for (; parsingIndex < input.length(); parsingIndex++) {
                char c = input.charAt(parsingIndex);

                if (c == '(') {
                    openParens++;
                } else if (c == ')') {
                    if (openParens == 0) {
                        // Found the closing parenthesis that matches "calc("
                        break;
                    }
                    openParens--;
                }
                content.append(c);
            }

            // If parsingIndex reached the end, we never found a matching ')'
            if (parsingIndex == input.length()) {
                System.err.println("No matching closing parenthesis found for 'calc('.");
                // Append whatever was left and break
                // (or you could choose to discard it, depending on your needs)
                break;
            }

            // Append the unwrapped content of calc(...)
            result.append(content);

            // Move past the closing parenthesis for this calc
            currentIndex = parsingIndex + 1;
        }

        return result.toString();
    }

    private static @NotNull String replaceZoneChecks(final String input) {
        StringBuilder inZoneResult = new StringBuilder();
        Matcher inZoneMatcher = IN_ZONE.matcher(input);
        while (inZoneMatcher.find()) {
            String coord1 = inZoneMatcher.group("coord1");
            String coord2 = inZoneMatcher.group("coord2");
            boolean test = "1".equals(inZoneMatcher.group("test"));

            StringBuilder mappedCoord1 = new StringBuilder();
            Matcher coord1Matcher = COORD.matcher(coord1);
            while (coord1Matcher.find()) {
                replaceCoord(coord1Matcher, mappedCoord1);
            }
            coord1Matcher.appendTail(mappedCoord1);

            StringBuilder mappedCoord2 = new StringBuilder();
            Matcher coord2Matcher = COORD.matcher(coord2);
            while (coord2Matcher.find()) {
                replaceCoord(coord2Matcher, mappedCoord2);
            }
            coord2Matcher.appendTail(mappedCoord2);

            String replacement = String.format("new Area.Rectangular(new %s, new %s)", mappedCoord1, mappedCoord2);
            if (!AREA_CONSTANTS.contains(replacement)) {
                AREA_CONSTANTS.add(replacement);
            }
            replacement = "WILDERNESS_AREA_" + AREA_CONSTANTS.indexOf(replacement);

            if (!test) {
                replacement = "!" + replacement;
            }

            replacement += ".contains(position)";

            inZoneMatcher.appendReplacement(inZoneResult, replacement);
        }
        inZoneMatcher.appendTail(inZoneResult);
        return inZoneResult.toString();
    }

    private static void replaceCoord(final Matcher matcher, final StringBuilder result) {
        int plane = Integer.parseInt(matcher.group("plane"));
        int x = Integer.parseInt(matcher.group("regionX")) * 64 + Integer.parseInt(matcher.group("x"));
        int y = Integer.parseInt(matcher.group("regionY")) * 64 + Integer.parseInt(matcher.group("y"));
        String replacement = String.format("Coordinate(%s, %s, %s)", x, y, plane);
        matcher.appendReplacement(result, replacement);
    }

    private static String replaceScale(final String input) {
        Matcher matcher = SCALE.matcher(input);
        StringBuilder result = new StringBuilder();
        while (matcher.find()) {
            String expression = matcher.group("expression");
            String div = matcher.group("div");
            String scale = matcher.group("scale");

            String replacement = String.format("%s * %s / %s", expression, div, scale);
            matcher.appendReplacement(result, replacement);
        }

        matcher.appendTail(result);
        return result.toString();
    }

}
