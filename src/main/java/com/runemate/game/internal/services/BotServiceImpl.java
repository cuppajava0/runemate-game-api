package com.runemate.game.internal.services;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.client.embeddable.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.task.*;
import com.runemate.game.internal.events.*;
import com.runemate.game.internal.input.*;
import com.runemate.ui.*;
import java.util.*;
import java.util.concurrent.*;

public class BotServiceImpl implements BotService {

    private final AbstractBot bot;

    private final Task gameEventController;
    private final EventDispatcher eventDispatcher;
    private final BotInputService inputManager;

    public BotServiceImpl(AbstractBot bot) {
        this.bot = bot;
        this.gameEventController = new GameEventControllerImpl(bot);
        this.eventDispatcher = new EventDispatcherImpl(bot);
        this.inputManager = new BotInputServiceImpl();
    }

    @Override
    public Task getEventController() {
        return gameEventController;
    }

    @Override
    public EventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    @Override
    public BotInputService getInputManager() {
        return inputManager;
    }

    @Override
    public void close() {
        final ExecutorService cameraExecutor = Camera.executorMap.remove(bot);
        if (cameraExecutor != null) {
            cameraExecutor.shutdownNow();
        }
        for (final EventListener listener : eventDispatcher.getListeners()) {
            eventDispatcher.removeListener(listener);
        }
        eventDispatcher.shutdown();
        BotServiceProvider.getServices().remove(bot);
    }

    @Override
    public EmbeddableUI createDefaultUserInterface() {
        return new DefaultUI(bot);
    }
}
