package com.runemate.game.internal.events;

import com.runemate.client.game.account.open.*;
import com.runemate.client.game.events.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.game.api.script.framework.task.*;
import com.runemate.game.events.*;
import com.runemate.game.events.osrs.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@RequiredArgsConstructor
public final class GameEventControllerImpl extends Task implements GameEventController {
    private List<GameEventHandler> events;
    private GameEventHandler executing;
    private long executingStartTime;
    private EventListener listener;

    private final AbstractBot bot;

    public static void displayTrayNotification(
        final GameEventHandler solver, final String message,
        final TrayIcon.MessageType type
    ) {
        String caption = solver.getAPIEventInstance().getName();
        caption += " - " + Environment.getAccountAlias();
        ClientUI.sendTrayNotification(caption, message, type);
    }

    @Override
    public boolean validate() {
        for (final GameEventHandler event : getEvents()) {
            if (event.getAPIEventInstance().isEnabled(bot) && event.isValid()) {
                return true;
            }
        }
        executing = null;
        executingStartTime = 0;
        return false;
    }

    @Override
    public void execute() {
        final GameEventHandler old = executing;
        GameEventHandler current = null;
        int currentPriority = Integer.MIN_VALUE;
        //If we order the events by priority, this shouldn't be needed
        for (final GameEventHandler event : getEvents()) {
            //Checking if it's valid is more cpu intensive than checking priority.
            if ((current == null || event.getPriority() > currentPriority) &&
                event.getAPIEventInstance().isEnabled() && event.isValid()) {
                current = event;
                currentPriority = event.getPriority();
            }
        }
        if (current != null) {
            executing = current;
            if (!Objects.equals(old, current)) {
                executingStartTime = 0;
                final String message = current.getActivationText() + " has been activated!";
                log.debug(message);
                if (current.getAPIEventInstance().areTrayNotificationsEnabled()) {
                    displayTrayNotification(current, message, TrayIcon.MessageType.NONE);
                }
                executing.onStart();
            }
            executingStartTime =
                (executingStartTime != 0 ? executingStartTime : System.currentTimeMillis());
            executing.run();
        }
    }

    public GameEvents.GameEvent getGameEvent(GameEvents.GameEvent eventInstance) {
        for (final GameEventHandler event : getEvents()) {
            if (event.getAPIEventInstance() == eventInstance) {
                return event;
            }
        }
        return null;
    }

    public List<GameEventHandler> getEvents() {
        if (events == null) {
            events = Arrays.asList(
                new OSRSBankPin(),
                new OSRSInterfaceCloser(),
                new OSRSLobbyHandler(),
                new OSRSLoginHandler(),
                new EventNpcDismisser(),
                new OSRSUnexpectedItemHandler(),
                new GenieHandler()
            );
        }
        return events;
    }

    @NonNull
    @Override
    public EventListener getListener() {
        if (listener == null) {
            listener = new OSRSListeners();
        }
        return listener;
    }


    public class OSRSListeners implements InventoryListener, ChatboxListener {

        @Override
        public void onInventoryUpdated(final ItemEvent event) {
            getEvents().stream().filter(handler -> handler.getAPIEventInstance().isEnabled())
                .forEach(handler -> {
                    if (handler instanceof InventoryListener il) {
                        il.onInventoryUpdated(event);
                    }
                });
        }

        @Override
        public void onMessageReceived(MessageEvent event) {
            getEvents().stream().filter(handler -> handler.getAPIEventInstance().isEnabled())
                .forEach(handler -> {
                    if (handler instanceof ChatboxListener cl) {
                        cl.onMessageReceived(event);
                    }
                });
        }
    }
}
