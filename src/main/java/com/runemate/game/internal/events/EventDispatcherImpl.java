package com.runemate.game.internal.events;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.dispatchers.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.rmi.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

/**
 * Processes and dispatches events to the registered listeners.
 */
@Log4j2(topic = "EventDispatcher")
public class EventDispatcherImpl extends EventDispatcher {

    private final HashSet<Class<? extends EventListener>> using = new HashSet<>();
    private final ScheduledExecutorService poller = Executors.newSingleThreadScheduledExecutor();
    private final Map<Integer, List<SpriteItem>> itemState = new ConcurrentHashMap<>();

    private final AbstractBot bot;

    public EventDispatcherImpl(AbstractBot bot) {
        super(bot);
        this.bot = bot;
    }

    @SuppressWarnings("unchecked")
    private <T> void forEach(Class<T> type, Consumer<T> consumer) {
        for (final EventListener listener : getListeners()) {
            if (type.isAssignableFrom(listener.getClass())) {
                consumer.accept((T) listener);
            }
        }
    }

    /**
     * Forwards an event to the registered listeners of the correct type.
     */
    @Override
    public final void process(Event event) {
        if (event == null) {
            return;
        }

        forEach(GlobalListener.class, l -> l.onEvent(event));
        if (event instanceof EngineEvent e) {
            forEach(EngineListener.class, l -> l.onEngineEvent(e));
        } else if (event instanceof CS2ScriptEvent e) {
            forEach(CS2ScriptEventListener.class, l -> {
                if (CS2ScriptEvent.Type.STARTED.equals(e.getType())) {
                    l.onScriptExecutionStarted(e);
                }
            });
        } else if (event instanceof EntityEvent ee) {
            switch (ee.getEntityType()) {
                case PLAYER: {
                    if (event instanceof DeathEvent e) {
                        forEach(PlayerListener.class, l -> l.onPlayerDeath(e));
                    } else if (event instanceof AnimationEvent e) {
                        forEach(PlayerListener.class, l -> l.onPlayerAnimationChanged(e));
                    } else if (event instanceof TargetEvent e) {
                        forEach(PlayerListener.class, l -> l.onPlayerTargetChanged(e));
                    } else if (event instanceof PlayerMovementEvent e) {
                        forEach(PlayerListener.class, l -> l.onPlayerMoved(e));
                    } else if (event instanceof HitsplatEvent e) {
                        forEach(PlayerListener.class, l -> l.onPlayerHitsplat(e));
                    }
                    break;
                }
                case NPC: {
                    if (event instanceof DeathEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcDeath(e));
                    } else if (event instanceof AnimationEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcAnimationChanged(e));
                    } else if (event instanceof TargetEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcTargetChanged(e));
                    } else if (event instanceof NpcSpawnedEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcSpawned(e));
                    } else if (event instanceof HitsplatEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcHitsplat(e));
                    } else if (event instanceof NpcDespawnedEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcDespawned(e));
                    } else if (event instanceof NpcDefinitionChangedEvent e) {
                        forEach(NpcListener.class, l -> l.onNpcDefinitionChanged(e));
                    }
                    break;
                }
                case PROJECTILE: {
                    if (event instanceof ProjectileLaunchEvent e) {
                        forEach(ProjectileListener.class, l -> l.onProjectileLaunched(e));
                    } else if (event instanceof ProjectileMovedEvent e) {
                        forEach(ProjectileListener.class, l -> l.onProjectileMoved(e));
                    }
                    break;
                }
                case GROUNDITEM: {
                    if (event instanceof GroundItemSpawnedEvent e) {
                        forEach(GroundItemListener.class, l -> l.onGroundItemSpawned(e));
                    }
                    break;
                }
                case GAMEOBJECT: {
                    if (event instanceof GameObjectSpawnEvent e) {
                        forEach(GameObjectListener.class, l -> l.onGameObjectSpawned(e));
                    } else if (event instanceof GameObjectDespawnEvent e) {
                        forEach(GameObjectListener.class, l -> l.onGameObjectDespawned(e));
                    }
                    break;
                }
                case SPOTANIMATION: {
                    if (event instanceof SpotAnimationSpawnEvent e) {
                        forEach(SpotAnimationListener.class, l -> l.onSpotAnimationSpawned(e));
                    }
                    break;
                }
                default:
                    //Simply serves as a reminder in case we ever add other EntityType listeners
                    throw new IllegalStateException("Unsupported EntityEvent: " + event.getClass().getSimpleName());
            }
        } else if (event instanceof EngineStateEvent e) {
            forEach(EngineListener.class, l -> l.onEngineStateChanged(e));
        } else if (event instanceof MenuInteractionEvent e) {
            forEach(MenuInteractionListener.class, l -> l.onInteraction(e));
        } else if (event instanceof ItemEvent e) {
            forEach(InventoryListener.class, l -> l.onInventoryUpdated(e));
        } else if (event instanceof SkillEvent e) {
            forEach(SkillListener.class, l -> {
                if (e.getType() == SkillEvent.Type.LEVEL_GAINED) {
                    l.onLevelUp(e);
                } else if (e.getType() == SkillEvent.Type.EXPERIENCE_GAINED) {
                    l.onExperienceGained(e);
                } else if (e.getType() == SkillEvent.Type.CURRENT_LEVEL_CHANGED) {
                    l.onCurrentLevelChanged(e);
                }
            });
        } else if (event instanceof MessageEvent e) {
            forEach(ChatboxListener.class, l -> l.onMessageReceived(e));
        } else if (event instanceof VarpEvent e) {
            forEach(VarpListener.class, l -> l.onValueChanged(e));
        } else if (event instanceof VarcEvent e) {
            forEach(VarcListener.class, l -> {
                if (e.isString()) {
                    l.onStringChanged(e);
                } else {
                    l.onIntChanged(e);
                }
            });
        } else if (event instanceof VarbitEvent e) {
            forEach(VarbitListener.class, l -> l.onValueChanged(e));
        } else if (event instanceof GrandExchangeEvent e) {
            forEach(GrandExchangeListener.class, l -> l.onSlotUpdated(e));
        } else if (event instanceof SceneUpdatedEvent e) {
            forEach(SceneListener.class, l -> l.onSceneUpdated(e));
        } else if (event instanceof HoveredEntitiesEvent e) {
            forEach(SceneListener.class, l -> l.onEntitiesHovered(e));
        } else if (event instanceof SettingChangedEvent e) {
            forEach(SettingsListener.class, l -> l.onSettingChanged(e));
        } else if (event instanceof SettingsConfirmedEvent) {
            forEach(SettingsListener.class, SettingsListener::onSettingsConfirmed);
        } else if (event instanceof TrialEndedEvent) {
            forEach(TrialListener.class, TrialListener::onTrialEnded);
        }
    }

    /**
     * Registers a listener to dispatch in-game events to
     *
     * @param listener a listener (PaintListener, MouseListener, KeyListener, etc)
     */
    public void addListener(final EventListener listener) {
        super.addListener(listener);
        //The GrandExchangeListener is the only listeners that still rely on polling.
        if (listener instanceof GrandExchangeListener && !using.contains(GrandExchangeListener.class)) {
            GrandExchangeDispatcher dispatcher = new GrandExchangeDispatcher();
            poller.scheduleWithFixedDelay(dispatcher, 600, dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS);
            using.add(GrandExchangeListener.class);
        }

        if (listener instanceof GlobalListener) {
            registerCallback(0xFFFFFFFF);
        } else {
            if (listener instanceof ChatboxListener) {
                registerCallback(Callback.CHATBOX);
            }
            if (listener instanceof CS2ScriptEventListener) {
                registerCallback(Callback.CS2);
            }
            if (listener instanceof EngineListener) {
                registerCallback(Callback.ENGINE);
            }
            if (listener instanceof GroundItemListener) {
                registerCallback(Callback.GROUND_ITEM);
            }
            if (listener instanceof GameObjectListener) {
                registerCallback(Callback.OBJECT);
            }
            if (listener instanceof InventoryListener) {
                registerCallback(Callback.INVENTORY);
            }
            if (listener instanceof MenuInteractionListener) {
                registerCallback(Callback.MENU_ACTION);
            }
            if (listener instanceof NpcListener) {
                registerCallback(Callback.NPC);
            }
            if (listener instanceof PlayerListener) {
                registerCallback(Callback.PLAYER);
            }
            if (listener instanceof ProjectileListener) {
                registerCallback(Callback.PROJECTILE);
            }
            if (listener instanceof SceneListener) {
                registerCallback(Callback.REGION);
            }
            if (listener instanceof VarpListener) {
                registerCallback(Callback.VARP);
            }
            if (listener instanceof VarbitListener) {
                registerCallback(Callback.VARBIT);
            }
            if (listener instanceof VarcListener) {
                registerCallback(Callback.VARC);
            }
            if (listener instanceof SpotAnimationListener) {
                registerCallback(Callback.SPOT_ANIMATION);
            }
        }
    }

    @Override
    public void createAnimationEvent(OpenActor actor, int animationId) {
        withActor(actor, (a, t) -> {
            dispatch(new AnimationEvent(t, a, animationId));
        });
    }

    private void withActor(OpenActor actor, BiConsumer<Actor, EntityEvent.EntityType> consumer) {
        EntityEvent.EntityType entityType;
        Actor source;
        if (actor instanceof OpenPlayer p) {
            source = new OSRSPlayer(p);
            entityType = EntityEvent.EntityType.PLAYER;
        } else if (actor instanceof OpenNpc n) {
            source = new OSRSNpc(n);
            entityType = EntityEvent.EntityType.NPC;
        } else {
            throw new IllegalStateException("Unknown actor type: " + actor.getClass().getName());
        }

        consumer.accept(source, entityType);
    }

    @Override
    public void createChatboxEvent(int type, @NonNull String sender, @NonNull String message) {
        dispatch(new MessageEvent(Chatbox.Message.Type.resolve(type, message, sender), sender, message));
    }

    @Override
    public void createConnectionStateEvent(int old, int current) {
        dispatch(new EngineStateEvent(old, current));
    }

    @Override
    public void createDeathEvent(OpenActor actor, int gameCycle) {
        withActor(actor, (a, t) -> {
            Coordinate deathPos = bot.getPlatform().submit(() -> a.getPosition()).join();
            Area.Rectangular deathArea = bot.getPlatform().submit(() -> a.getArea()).join();
            dispatch(new DeathEvent(t, a, deathPos, deathArea, gameCycle));
        });
    }

    @Override
    public void createEngineCycleEvent(int cycle) {
        dispatch(new EngineEvent(cycle, EngineEvent.Type.CLIENT_CYCLE));
    }

    @Override
    public void createHitsplatEvent(OpenActor actor, int typeId, int damage, int specialTypeId, int startCyle, int endCycle) {
        //TODO cleanup parameter types (long endCycle)
        Hitsplat hitsplat = new Hitsplat(typeId, damage, specialTypeId, startCyle, endCycle);
        withActor(actor, (a, t) -> dispatch(new HitsplatEvent(t, a, hitsplat)));
    }

    @Override
    public void createMenuInteractionEvent(
        int arg0, int arg1, int opcode, int identifier, @NonNull String action, @NonNull String target, int mx, int my
    ) {
        dispatch(new MenuInteractionEvent(arg0, arg1, opcode, identifier, JagTags.remove(action), JagTags.remove(target), mx, my));
    }

    @Override
    public void createPlayerMovedEvent(OpenPlayer player) {
        dispatch(new PlayerMovementEvent(new OSRSPlayer(player)));
    }

    @Override
    public void createProjectileLaunchEvent(OpenProjectile projectile) {
        dispatch(new ProjectileLaunchEvent(new OSRSProjectile(projectile)));
    }

    @Override
    public void createRegionLoadedEvent(int x, int y) {
        final Coordinate previous = (Coordinate) bot.getCache().get("RegionBase");
        final Coordinate base = new Coordinate(x, y, 0);
        dispatch(new SceneUpdatedEvent(previous, base));
    }

    @Override
    public void createScriptStartEvent(int scriptId, @Nullable Object[] scriptArgs) {
        dispatch(new CS2ScriptEvent(scriptId, CS2ScriptEvent.Type.STARTED, scriptArgs));
    }

    @Override
    public void createServerTickEvent(int tick) {
        dispatch(new EngineEvent(tick, EngineEvent.Type.SERVER_TICK));

        //Clear the cached base every tick, this should be repopulated by OSRSScene.getBase()
        bot.getCache().remove("RegionBase");
    }

    @Override
    public void createTargetEvent(OpenActor actor, int targetIndex) {
        withActor(actor, (a, t) -> dispatch(new TargetEvent(t, a, bot.getPlatform().invoke(() -> getCharacterTarget(targetIndex)))));
    }

    @Override
    public void createVarbitEvent(final int index, final int old, final int value) {
        //handled by 'createVarpEvent'
    }

    @Override
    public void createVarcIntEvent(final int index, final int old, final int value) {
        if (old != value) {
            dispatch(new VarcEvent(false, index, old, value));
        }
    }

    @Override
    public void createVarcStringEvent(final int index, final String old, final String value) {
        if (!Objects.equals(old, value)) {
            dispatch(new VarcEvent(true, index, old, value));
        }
    }

    @Override
    public void createVarpEvent(final int index, final int old, final int value) {
        bot.getPlatform().submit(() -> {
            Varp varp = Varps.getAt(index);
            dispatch(new VarpEvent(varp, old, value));
            List<Varbit> varbits = Varbits.forVarp(index);
            if (varbits != null) {
                varbits.stream().filter(Objects::nonNull).forEach(varbit -> {
                    int oldValue = varbit.getValue(old);
                    int newValue = varbit.getValue(value);
                    if (oldValue != newValue) {
                        dispatch(new VarbitEvent(varbit, oldValue, newValue));
                    }
                });
            }
        }).join();
    }

    @Override
    public void createNpcSpawnEvent(OpenNpc npc) {
        withActor(npc, (a, t) -> dispatch(new NpcSpawnedEvent((Npc) a)));
    }

    @Nullable
    private static Actor getCharacterTarget(int targetIndex) {
        if (targetIndex == -1) {
            return null;
        }
        if (targetIndex < 0x10000) {
            return OSRSNpcs.getByIndex(targetIndex);
        } else if (targetIndex < (0x10000 + 2048)) {
            return OSRSPlayers.getAt(targetIndex - 0x10000);
        } else {
            return null;
        }
    }

    @Override
    public void createEventObjectSpawnEvent(OpenGameObject node) {
        Coordinate pos = getBase().derive(node.getX(), node.getY());
        dispatch(new GameObjectSpawnEvent(new OSRSGameObject(node, pos), pos));
    }

    @Override
    public void createEventObjectDespawnEvent(OpenGameObject node) {
        Coordinate pos = getBase().derive(node.getX(), node.getY());
        dispatch(new GameObjectDespawnEvent(new OSRSGameObject(node, pos), pos));
    }

    @Override
    public void createProjectileMovedEvent(OpenProjectile projectile, int sceneX, int sceneY) {
        Coordinate pos = getBase().derive(sceneX, sceneY);
        dispatch(new ProjectileMovedEvent(new OSRSProjectile(projectile), pos));
    }

    @Override
    public void createSpotAnimationEvent(OpenSpotAnimation node, int sceneX, int sceneY) {
        Coordinate pos = getBase().derive(sceneX, sceneY);
        dispatch(new SpotAnimationSpawnEvent(new OSRSSpotAnimation(node), pos));
    }

    @Override
    public void processItemEventsFromChange(long invId, int itemIndex, int itemId, int itemQty) {
        for (ItemEvent event : getItemEventsFromChange((int) invId, itemIndex, itemId, itemQty)) {
            bot.getEventDispatcher().dispatchLater(event);
        }
    }

    private synchronized List<ItemEvent> getItemEventsFromChange(int inventoryId, int itemIndex, int itemId, int itemQuantity) {
        List<ItemEvent> events = new ArrayList<>(2);
        List<SpriteItem> inventory = itemState.computeIfAbsent(inventoryId, id -> Inventories.lookup(id).asList());
        List<SpriteItem> itemsInSlot = inventory.stream()
            .filter(item -> item.getIndex() == itemIndex)
            .toList();

        if (itemsInSlot.size() > 1) {
            log.warn("The inventory with id {} cannot have more than one item in index {}", inventoryId, itemIndex);
        }

        SpriteItem itemInSlot = itemsInSlot.stream()
            .findFirst()
            .orElse(null);

        if (itemInSlot == null && itemId >= 0 && itemQuantity > 0) {
            //Add item to empty slot
            SpriteItem item = new SpriteItem(itemId, itemQuantity, itemIndex, Inventories.Documented.getOrigin(inventoryId));
            inventory.add(item);
            events.add(new ItemEvent(item, itemQuantity, inventoryId));
        }
        if (itemInSlot != null) {
            if (itemInSlot.getId() == itemId) {
                //Update quantity of item in slot
                int stackChange = itemQuantity - itemInSlot.getQuantity();
                if (stackChange != 0) {
                    //but only bother sending an event and updating the cache if the quantity actually changed
                    SpriteItem adjustedStackItemInSlot = itemInSlot.derive(stackChange);
                    inventory.remove(itemInSlot);
                    inventory.add(adjustedStackItemInSlot);
                    events.add(new ItemEvent(adjustedStackItemInSlot, stackChange, inventoryId));
                }
            } else {
                //Replace an item in an occupied slot
                //1. Remove from cache and send event negativing it's entire presence
                //2. Build new item in it's slot, cache it, and fire added event.
                inventory.remove(itemInSlot);
                events.add(new ItemEvent(itemInSlot, -itemInSlot.getQuantity(), inventoryId));
                if (itemId != -1) {
                    SpriteItem newItemInSlot = new SpriteItem(itemId,
                        itemQuantity,
                        itemIndex,
                        Inventories.Documented.getOrigin(inventoryId)
                    );
                    inventory.add(newItemInSlot);
                    events.add(new ItemEvent(newItemInSlot, itemQuantity, inventoryId));
                }
            }
        }
        return events;
    }

    @Override
    public void onCurrentLevelUpdated(int index, int old, int value) {
        final var skill = Skills.getByIndex(index);
        if (skill == null || old == -1 || value - old == 0) {
            return;
        }
        final var event = new SkillEvent(skill, SkillEvent.Type.CURRENT_LEVEL_CHANGED, value, old);
        bot.getEventDispatcher().dispatchLater(event);
    }

    @Override
    public void onExperienceUpdated(int index, int previous, int current) {
        final var skill = Skills.getByIndex(index);
        if (skill == null || previous == -1 || current - previous == 0) {
            return;
        }
        bot.getEventDispatcher().dispatchLater(new SkillEvent(skill, SkillEvent.Type.EXPERIENCE_GAINED, current, previous));

        final var previousLevel = Skills.getLevelAtExperience(skill, previous);
        final var currentLevel = Skills.getLevelAtExperience(skill, current);
        if (currentLevel > previousLevel) {
            bot.getEventDispatcher().dispatchLater(new SkillEvent(skill, SkillEvent.Type.LEVEL_GAINED, currentLevel, previousLevel));
        }
    }

    @Override
    @SneakyThrows
    public void createGroundItemEvent(OpenItemNode node) {
        Coordinate pos = getBase().derive(node.getX(), node.getY());
        dispatch(new GroundItemSpawnedEvent(new OSRSGroundItem(node, pos), pos));
    }

    @Override
    public void createNpcDefinitionChangeEvent(OpenNpc open, int previousId, int nextId) {
        if (previousId == nextId) {
            return;
        }

        NpcDefinitionChangedEvent event = bot.getPlatform().submit(() -> {
            Npc npc = new OSRSNpc(open);
            NpcDefinition previous = previousId != -1 ? NpcDefinition.get(previousId) : null;
            NpcDefinition current = nextId != -1 ? NpcDefinition.get(previousId) : null;
            if (Objects.equals(previous, current)) {
                return null;
            }

            return new NpcDefinitionChangedEvent(npc, previous, current);
        }).join();

        if (event != null) {
            dispatch(event);
        }
    }

    @Override
    public void createNpcDespawnEvent(int index, int previousId, int sceneX, int sceneY) {
        NpcDefinition definition = bot.getPlatform().invoke(() -> NpcDefinition.get(previousId));
        dispatch(new NpcDespawnedEvent(index, getBase().derive(sceneX, sceneY), definition));
    }

    @Override
    public void onEntityHovered(long uid) {
        bot.getPlatform().invoke(() -> {
            List<Entity> hovered = new ArrayList<>();
            OSRSScene.resolveEntities(uid, hovered);
            dispatch(new HoveredEntitiesEvent(hovered));
        });
    }

    private Coordinate getBase() {
        return bot.getPlatform().invoke(() -> Scene.getBase(Scene.getCurrentPlane()));
    }

    @Override
    public void shutdown() {
        poller.shutdown();
    }

}
