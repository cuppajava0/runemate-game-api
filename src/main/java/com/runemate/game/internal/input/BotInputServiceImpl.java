package com.runemate.game.internal.input;

import static com.runemate.game.api.hybrid.input.Keyboard.*;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.*;
import java.util.List;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public final class BotInputServiceImpl implements BotInputService {

    private final Point mouse_position = new Point();
    private final List<Integer> pressed_mouse_buttons = Collections.synchronizedList(new ArrayList<>(Mouse.Button.values().length));
    private Interactable mouse_target, previous_mouse_target;

    @Override
    public boolean releaseKeys() {
        boolean success = true;
        for (final int key : KEYCODE_MAPPING) {
            if (key != -1 && Keyboard.isPressed(key)) {
                if (!Keyboard.releaseKey(key)) {
                    success = false;
                }
            }
        }
        return success;
    }


    @NonNull
    public Point getMousePosition() {
        return mouse_position;
    }

    @Override
    public void setMousePosition(@NonNull Point point) {
        mouse_position.setLocation(point);
    }

    public Interactable getMouseTarget() {
        return mouse_target;
    }

    public void setMouseTarget(Interactable target) {
        this.mouse_target = target;
    }

    @Override
    public void setMouseTarget(@Nullable Object o) {
        mouse_target = (Interactable) o;
    }

    public Interactable getPreviousMouseTarget() {
        return previous_mouse_target;
    }

    //TODO This must have been set somewhere previously but doesn't appear to be used now?
    public void setPreviousMouseTarget(Interactable target) {
        this.previous_mouse_target = target;
    }

    @Override
    public void setPreviousMouseTarget(@Nullable Object o) {
        previous_mouse_target = (Interactable) o;
    }

    @NonNull
    public List<Integer> getPressedMouseButtons() {
        return pressed_mouse_buttons;
    }

    public boolean hop(final Point point) {
        if (point == null) {
            return false;
        }
        return hop(point.x, point.y);
    }

    @Override
    //Used by login-handler
    public boolean type(@NonNull String s) {
        return Keyboard.type(s, false);
    }

    @SneakyThrows(RemoteException.class)
    public boolean hop(final int x, final int y) {
        if (mouse_position.x != x || mouse_position.y != y) {
            if (!pressed_mouse_buttons.isEmpty()) {
                int modifiers = 0;
                if (Mouse.isPressed(Mouse.Button.LEFT)) {
                    modifiers |= InputEvent.BUTTON1_DOWN_MASK;
                }
                if (Mouse.isPressed(Mouse.Button.WHEEL)) {
                    modifiers |= InputEvent.BUTTON2_DOWN_MASK;
                }
                if (Mouse.isPressed(Mouse.Button.RIGHT)) {
                    modifiers |= InputEvent.BUTTON3_DOWN_MASK;
                }
                if (Keyboard.isPressed(KeyEvent.VK_ALT)) {
                    modifiers |= InputEvent.ALT_DOWN_MASK;
                }
                if (Keyboard.isPressed(KeyEvent.VK_META)) {
                    modifiers |= InputEvent.META_DOWN_MASK;
                }
                if (Keyboard.isPressed(KeyEvent.VK_CONTROL)) {
                    modifiers |= InputEvent.CTRL_DOWN_MASK;
                }
                if (Keyboard.isPressed(KeyEvent.VK_SHIFT)) {
                    modifiers |= InputEvent.SHIFT_DOWN_MASK;
                }
                if (Keyboard.isPressed(KeyEvent.VK_ALT_GRAPH)) {
                    modifiers |= InputEvent.ALT_GRAPH_DOWN_MASK;
                }
                OpenInput.dragMouse(x, y, modifiers);
            } else {
                OpenInput.moveMouse(x, y);
            }
            mouse_position.setLocation(x, y);
            return true;
        }
        return false;
    }
}
