package com.runemate.game.events;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.logger.*;

public abstract class GameEventHandler implements Runnable, GameEvents.GameEvent {
    public static final int PRIORITY_LOW = 1;
    public static final int PRIORITY_NORMAL = 2;
    public static final int PRIORITY_HIGH = 3;
    private final int priority;

    public GameEventHandler(int priority) {
        this.priority = priority;
    }

    public GameEventHandler() {
        this(PRIORITY_NORMAL);
    }

    public final int getPriority() {
        return priority;
    }

    public abstract GameEvents.GameEvent getAPIEventInstance();

    public abstract boolean isValid();

    @Override
    public String getName() {
        return getActivationText();
    }

    public String getActivationText() {
        return getAPIEventInstance().getName();
    }

    public void onStart() {
    }

    @Override
    public String toString() {
        return getAPIEventInstance().getName() + " (priority=" + priority + ')';
    }
}
