package com.runemate.game.events.osrs;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.logger.*;
import com.runemate.game.events.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public class EventNpcDismisser extends GameEventHandler {
    private static final Pattern EVENT_NPCS = Regex.getPatternForExactStrings(
        "Evil Bob", "Postie Pete", "Frog", "Drunken Dwarf",
        "Dr Jekyll", "Capt' Arnav", "Rick Turpentine",
        "Mysterious Old Man", "Flippa", "Quiz Master",
        "Sandwich lady", "Freaky Forester", "Bee keeper",
        "Miles", "Sergeant Damien", "Dunce", "Niles", "Leo",
        "Pillory Guard", "Giles"
    );
    private Npc dismissable;

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.NPC_DISMISSER;
    }

    @Override
    public boolean isValid() {
        if (!PlayerSense.getAsBoolean(PlayerSense.Key.DISMISS_EVENT_NPCS)) {
        return false;
    }
        Player local = Players.getLocal();
        if (local == null || local.isMoving() || Bank.isOpen() || GrandExchange.isOpen()) {
            return false;
        }
        if (Random.nextInt(1, 101) >= PlayerSense.getAsInteger(PlayerSense.Key.EVENT_NPC_DISMISSAL_PERCENTAGE)) {
            dismissable = Npcs.newQuery()
                .levels(0)
                .names(EVENT_NPCS)
                .actions("Dismiss")
                .reachableFrom(local)
                .visible()
                .targeting(local)
                .results()
                .nearestTo(local);
            return dismissable != null;
        }
        return false;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void run() {
        final SpriteItem selected = Inventory.getSelectedItem();
        if (selected != null) {
            log.debug("Deselecting the selected inventory: " + selected);
            if (selected.click()) {
                Execution.delayUntil(() -> Inventory.getSelectedItem() == null, 1200, 2400);
            }
            return;
        }
        final Magic spell = Magic.getSelected();
        if (spell != null) {
            log.debug("Deselecting the magic spell: " + spell);
            if (spell.deactivate()) {
                Execution.delayUntil(() -> Magic.getSelected() == null, 1200, 2400);
            }
            return;
        }
        log.debug("Dismissing the npc: " + dismissable);
        if (dismissable != null && dismissable.interact("Dismiss")) {
            Execution.delayWhile(dismissable::isValid, 1800, 2400);
        }
    }

    @NonNull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }
}
