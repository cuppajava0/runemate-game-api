package com.runemate.game.events.osrs;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.events.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public class OSRSUnexpectedItemHandler extends GameEventHandler {

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.UNEXPECTED_ITEM_HANDLER;
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public void run() {
        // no-op
    }

    @NonNull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }
}
