package com.runemate.game.events.osrs;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.handler.*;
import com.runemate.game.api.osrs.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.events.*;
import com.runemate.game.internal.events.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;
import lombok.*;
import lombok.extern.log4j.*;

/*
Some error codes:
Code 1 : Could not display video advertising logging in in...
Code 2 : Lets you login normally
Code 3 : Invalid username or password
Code 4 : Your account has been disabled
Code 5 : Your account has not logged out from its last session
Code 6 : Runescape has been updated! Please reload the page.
Code 7 : World is full
Code 8 : Unable to connect, login server offline
Code 9 : Login limit exceeded: too many connections from your address
Code 10 : Unable to connect bad session ID
Code 11 : Your password is too common
Code 12 : You need a members account to login on this world
Code 13 : Could not complete login please try a different world
Code 14 : The server is being updated. Please wait 1 minute and try again.
Code 15 : Unexpected Server Response
Code 16 : Too many login attempts
Code 17 : You are standing in a members only world
Code 18 : Your account has been locked
Code 19 : Fullscreen is currently a member only feature
Code 20 : Invalid login server requested
Code 21 : Stays on logging in forever?
Code 22 : Malformed login packet
Code 23 : No reply from login server
Code 24 : Error loading profile
Code 25 : Unexpected login server response
Code 26 : This computers address was used to break our rules
Code 27 : Service unavailable
Code 28 : Unexpected server response
Code 29 : Stays on logging in forever?
Code 30 : This is a members account
Code 33 : Too many login attempts. Jagex Launcher to continue playing
Code 34 : You need a members' account to use this world.
 */
@Log4j2
public final class OSRSLoginHandler extends GameEventHandler {

    private int fails;

    public OSRSLoginHandler() {
        super(PRIORITY_HIGH);
    }

    public static InteractableRectangle getLoginFieldBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(0, 242, screen.width, 11);
    }

    private static boolean clearEnteredLoginName() {
        final int MAX_TRIES = Random.nextInt(3, 8);
        int tries = 0;
        int deleteKey = KeyEvent.VK_BACK_SPACE;
        while (tries < MAX_TRIES && OpenAccountDetails.getEnteredUsernameLength() > 0) {
            final int length = OpenAccountDetails.getEnteredUsernameLength();
            Keyboard.typeKey(deleteKey);
            if (length == OpenAccountDetails.getEnteredUsernameLength()) {
                if (deleteKey == KeyEvent.VK_BACK_SPACE) {
                    deleteKey = KeyEvent.VK_DELETE;
                } else {
                    deleteKey = KeyEvent.VK_BACK_SPACE;
                }
                ++tries;
            } else {
                tries = 0;
            }
        }
        return OpenAccountDetails.getEnteredUsernameLength() == 0;
    }

    private static boolean clearEnteredPassword() {
        final int MAX_TRIES = Random.nextInt(3, 8);
        int tries = 0;
        int deleteKey = KeyEvent.VK_BACK_SPACE;
        while (tries < MAX_TRIES && OpenAccountDetails.getEnteredPasswordLength() > 0) {
            final int length = OpenAccountDetails.getEnteredPasswordLength();
            Keyboard.typeKey(deleteKey);
            if (length == OpenAccountDetails.getEnteredPasswordLength()) {
                if (deleteKey == KeyEvent.VK_BACK_SPACE) {
                    deleteKey = KeyEvent.VK_DELETE;
                } else {
                    deleteKey = KeyEvent.VK_BACK_SPACE;
                }
                ++tries;
            } else {
                tries = 0;
            }
        }
        return OpenAccountDetails.getEnteredPasswordLength() == 0;
    }

    private static InteractableRectangle getExistingUserBoundsButton() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 389, 271, 144, 40);
    }

    private static InteractableRectangle getLauncherLoginButton() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 276, 232, 210, 55);
    }

    private static InteractableRectangle getDisconnectedFromTheServerButtonBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 315, 287, 138, 32);
    }

    private static InteractableRectangle getTryAgainButtonBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 315, 258, 138, 32);
    }

    private static InteractableRectangle getBackButtonButtonBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 315, 310, 138, 30);
    }

    private static InteractableRectangle getMembersBackButtonButtonBounds() {
        Rectangle screen = Screen.getBounds();
        if (screen == null) {
            screen = new Rectangle(765, 503);
        }
        return new InteractableRectangle(((screen.width - 765) / 2) + 307, 306, 150, 40);
    }

    private void handleLoginResponse() {
        final var handler = GameEvents.LoginManager.getFailedLoginHandler();
        log.debug("Logging in");
        if (Execution.delayUntil(() -> "Connecting to server...".equals(OSRSRunescape.getServerResponseMessage()), 2500)) {
            Execution.delayWhile(() -> "Connecting to server...".equals(OSRSRunescape.getServerResponseMessage())
                || OSRSRunescape.getEngineState() == 20, 10000);
            int state = OSRSRunescape.getEngineState();
            if (state == 10 || state == 11) {
                //banned/invalid credentials/game updated
                //TODO tray notifications
                final String response = OSRSRunescape.getServerResponseMessage().replace("\n", " ");
                if (contains(response, "Invalid username")) {
                    handler.handle(GameEvents.LoginManager.Fail.INVALID_CREDENTIALS, ++fails);
                    log.warn("Unable to log in: " + response);
                    notify(response);
                } else if (contains(response, "Login limit exceeded")) {
                    handler.handle(GameEvents.LoginManager.Fail.LOGIN_LIMIT_EXCEEDED, ++fails);
                    log.warn("Unable to log in: " + response);
                    notify(response);
                } else if (contains(response, "we suspect it has been stolen")) {
                    handler.handle(GameEvents.LoginManager.Fail.SUSPECTED_STOLEN, ++fails);
                    log.warn("Unable to log in: " + response);
                    notify(response);
                } else if (contains(response, "Please reload this page.")) {
                    handler.handle(GameEvents.LoginManager.Fail.RUNESCAPE_UPDATED, ++fails);
                    log.warn("Unable to log in: " + response);
                    notify(response);
                } else if (contains(response, "Please choose yourself a display name via the website before logging in.")) {
                    handler.handle(GameEvents.LoginManager.Fail.MISSING_DISPLAY_NAME, ++fails);
                    log.warn("Unable to log in: " + response);
                    notify(response);
                } else if (contains(response, "digit code generated by")) {
                    log.warn("Unable to log in: " + response);
                    OpenAccountDetails.setAccountAuthenticatorRequired();
                    notify(response);
                    handler.handle(GameEvents.LoginManager.Fail.AUTHENTICATOR, ++fails);
                } else if (contains(response, "need a members' account")) {
                    log.debug("Members account required, selecting non-members world");
                    setNewWorld(true, false, false);
                } else if (contains(response, "in non-member skills")) {
                    log.debug("Selecting a non-members world with a lower skill requirement.");
                    setNewWorld(true, false, false);
                } else if (contains(response, "standing in a members", "log into a members world and move", "need a skill total")) {
                    log.debug("In members area, selecting members world");
                    setNewWorld(false, true, false);
                } else if (contains(response, "must be a Fresh Start Player")) {
                    log.debug("Fresh Start account required, selecting non-fresh start world");
                    setNewWorld(false, true, false);
                } else if (contains(response, "can't play on this world as a Fresh Start Player")) {
                    log.debug("Fresh Start account, selecting fresh start world");
                    setNewWorld(false, true, true);

                } else if (contains(
                    response, "is already logged in", "account has not logged out from its last session", "Too many login attempts.")) {
                    notify(response);
                    handler.handle(GameEvents.LoginManager.Fail.ALREADY_LOGGED_IN, ++fails);
                }
            }
        } else {
            final String response = OSRSRunescape.getServerResponseMessage().replace("\n", " ");
            if (contains(response, "Error connecting to server.")) {
                notify(response);
                handler.handle(GameEvents.LoginManager.Fail.CONNECTION_ERROR, ++fails);
            }
        }
    }

    private void handleServerResponse() {
        final var handler = GameEvents.LoginManager.getFailedLoginHandler();
        final String response = OSRSRunescape.getServerResponseMessage().replace("\n", " ");
        if (contains(response, "PIN", "enter a")) {
            notify(response);
            log.warn("Unable to login: " + response);
            handler.handle(GameEvents.LoginManager.Fail.AUTHENTICATOR, ++fails);
        } else if (contains(response, "need a members' account")) {
            log.debug("Members account required, selecting non-member world");
            setNewWorld(true, false, false);
        } else if (contains(response, "in non-member skills")) {
            log.debug("Selecting a non-members world with a lower skill requirement.");
            setNewWorld(true, false, false);
        } else if (contains(response, "standing in a members", "log into a members world and move", "need a skill total")) {
            log.debug("Standing in members area, selecting members world");
            setNewWorld(false, true, false);
        } else if (contains(response, "must be a Fresh Start Player")) {
            log.debug("Fresh Start account required, selecting non-fresh start world");
            setNewWorld(false, true, false);
        } else if (contains(response, "can't play on this world as a Fresh Start Player")) {
            log.debug("Fresh Start account, selecting fresh start world");
            setNewWorld(false, true, true);
        }
    }

    private static void setNewWorld(boolean free, boolean members, boolean fsw) {
        WorldOverview world = null;
        if (fsw) {
            if (free && members) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).results().random();
            } else if (free) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).free().results().random();
            } else if (members) {
                world = Worlds.newQuery().include(WorldType.FRESH_START_WORLD).member().results().random();
            }
        } else {
            if (free && members) {
                world = Worlds.newQuery().regular().results().random();
            } else if (free) {
                world = Worlds.newQuery().regular().free().results().random();
            } else if (members) {
                world = Worlds.newQuery().regular().member().results().random();
            }
        }
        if (world != null) {
            if (world.getActivity().equals("King of the Skill")) {
                setNewWorld(free, members, fsw);
                return;
            }
            log.debug("Changing the preferred world to " + world.getId());
            Worlds.setPreferred(world.getId());
        }
    }

    @NonNull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Collections.emptyList();
    }

    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return GameEvents.Universal.LOGIN_HANDLER;
    }

    @Override
    public boolean isValid() {
        if (OpenAccountDetails.hasActiveAccount()) {
            var state = RuneScape.getEngineState();
            return state.getValue() < RuneScape.EngineState.LOADING_REGION.getValue();
        }
        return false;
    }

    @Override
    public void onStart() {
        fails = 0;
    }

    @Override
    public void run() {
        FailedLoginHandler handler = GameEvents.LoginManager.getFailedLoginHandler();
        int preferredWorld = Worlds.getPreferred();
        if (preferredWorld != -1 && !WorldSelect.isSelected(preferredWorld) && WorldSelect.isSelectable(preferredWorld)) {
            log.debug("Switching worlds to " + preferredWorld);
            WorldSelect.select(preferredWorld);
            return;
        }

        if (OSRSWorldSelect.isOpen()) {
            OSRSWorldSelect.close();
            return;
        }


        final int loginState = RuneScape.getLoginState();
        if (loginState == RuneScape.LoginState.MAIN_MENU) {
            log.debug("Pressing Existing User");
            if (getExistingUserBoundsButton().click()) {
                Execution.delayUntil(() -> OSRSRunescape.getLoginState() == 2, 1000);
            }
            return;
        }

        if (loginState == RuneScape.LoginState.BETA_WORLD) {
            //High-risk pvp world warning
            final String response = OSRSRunescape.getServerResponseMessage().replace("\n", " ");
            if (contains(response, "Your normal account will not be affected")) {
                log.debug("Beta world detected, switching to a different world.");
                Keyboard.typeKey(KeyEvent.VK_ESCAPE);
                setNewWorld(false, true, false);
            } else {
                log.debug("Dismissing high-risk warning");
                Keyboard.typeKey(KeyEvent.VK_ENTER);
                setNewWorld(false, true, false);
            }
            return;
        }

        if (loginState == RuneScape.LoginState.ENTER_CREDENTIALS) {
            if (!OpenAccountDetails.isUsernameEntered()) {
                if (OpenAccountDetails.getEnteredUsernameLength() == 0) {
                    log.debug("Entering username");
                    OpenAccountDetails.typeUsername();
                    if (OpenAccountDetails.getEnteredUsernameLength() > 0) {
                        Keyboard.typeKey(KeyEvent.VK_TAB);
                    } else {
                        getLoginFieldBounds().click();
                    }
                } else {
                    log.debug("Clearing username");
                    if (!clearEnteredLoginName()) {
                        getLoginFieldBounds().click();
                    }
                }
            } else if (!OpenAccountDetails.isPasswordEntered()) {
                if (OpenAccountDetails.getEnteredPasswordLength() > 0) {
                    log.debug("Clearing password");
                    clearEnteredPassword();
                } else {
                    log.debug("Entering password");
                    OpenAccountDetails.typePassword();
                }
            } else if (Keyboard.typeKey(KeyEvent.VK_ENTER)) {
                handleLoginResponse();
            }
            return;
        }

        if (loginState == RuneScape.LoginState.AUTHENTICATOR) {
            handleServerResponse();
            return;
        }

        if (loginState == 9) {
            log.debug("Clicking Try Again button.");
            handleServerResponse();
            // TODO: check if jagex account and get that try again bounds
            getTryAgainButtonBounds().click();
            return;
        }

        if (loginState == 10) {
            log.debug("Clicking Login Button On Launcher.");
            if (getLauncherLoginButton().click()) {
                handleLoginResponse();
            }
            return;
        }

        if (loginState == RuneScape.LoginState.DISABLED) {
            final String response = OSRSRunescape.getServerResponseMessage().replace("\n", " ");
            if (contains(response, "suspect it has been stolen")) {
                handler.handle(GameEvents.LoginManager.Fail.SUSPECTED_STOLEN, ++fails);
                log.warn("Your account has been disabled due to being suspected stolen");
                notify("Your account has been disabled due to being suspected stolen");
            } else {
                handler.handle(GameEvents.LoginManager.Fail.DISABLED_OR_BANNED, ++fails);
                log.warn("Your account has been disabled or banned");
                notify("Your account has been disabled or banned");
            }
            return;
        }

        if (loginState == RuneScape.LoginState.DISCONNECTED) {
            log.debug("You were disconnected from the server. Clicking ok and logging back in.");
            getDisconnectedFromTheServerButtonBounds().click();
            notify("You were disconnected from the server. Clicking ok and logging back in.");
            return;
        }

        if (loginState == RuneScape.LoginState.TOO_MANY_ATTEMPTS) {
            log.debug("Too many login attempts. Presumably via world hop limit");
            getBackButtonButtonBounds().click();
            handler.handle(GameEvents.LoginManager.Fail.LOGIN_LIMIT_EXCEEDED, ++fails);
        }

        if (loginState == RuneScape.LoginState.NEED_MEMBERS_ACCOUNT) {
            log.debug("Need to click Back and select F2P world");
            getMembersBackButtonButtonBounds().click();
        }
    }

    private void notify(@NonNull String notification) {
        GameEventControllerImpl.displayTrayNotification(this, notification, TrayIcon.MessageType.INFO);
    }

    private boolean contains(String response, String... matchers) {
        if (response == null || response.isEmpty()) {
            return false;
        }
        for (final var matcher : matchers) {
            if (response.contains(matcher)) {
                return true;
            }
        }
        return false;
    }

}
