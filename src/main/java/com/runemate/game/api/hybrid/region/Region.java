package com.runemate.game.api.hybrid.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@UtilityClass
public class Region {

    public final int WIDTH = 64;
    public final int HEIGHT = 64;

    /**
     * Derives the ID of the 64x64 region containing the local player.
     *
     * @see #getRegionId(Locatable)
     */
    public int getCurrentRegionId() {
        return Optional.ofNullable(Players.getLocal())
            .map(Actor::getServerPosition)
            .map(Region::getRegionId)
            .orElse(0);
    }

    /**
     * Derives the ID of the 64x64 region containing the target coordinate using the following formula: {@code y / 64 | ((x / 64) << 8)}.
     */
    public int getRegionId(@NonNull Locatable locatable) {
        Coordinate position = locatable.getPosition();
        if (position == null) {
            return 0;
        }

        Coordinate mapped = Coordinate.uninstance(position);
        if (mapped != null) {
            position = mapped;
        }

        return position.getContainingRegionId();
    }

    @Nullable
    public Coordinate getCurrentRegionBase() {
        return Optional.ofNullable(Players.getLocal())
            .map(Actor::getServerPosition)
            .map(Region::getRegionBase)
            .orElse(null);
    }

    @Nullable
    public Coordinate getRegionBase(@NonNull Locatable locatable) {
        return Optional.ofNullable(locatable.getPosition())
            .map(Region::getRegionId)
            .map(Region::getRegionBase)
            .orElse(null);
    }

    public Coordinate getRegionBase(int regionId) {
        return new Coordinate(64 * (regionId >> 8), 64 * (regionId & 0xff), Scene.getCurrentPlane());
    }

    public boolean isInstanced() {
        return OpenRegion.isInstanced();
    }

    /**
     * @deprecated replaced by {@link Scene#getArea()}.
     */
    @Deprecated
    public static Area.Rectangular getArea() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getArea()");
        final Coordinate base = getBase();
        return new Area.Rectangular(base, base.derive(WIDTH - 1, HEIGHT - 1));
    }

    /**
     * By default, looks up the result of the last region update from {@link com.runemate.game.api.script.framework.listeners.SceneListener}.
     * Otherwise, uses {@link #getCurrentBase()}.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     * @deprecated replaced by {@link Scene#getBase()}.
     */
    @Deprecated
    public static Coordinate getBase() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getBase()");
        return OSRSScene.getBase();
    }

    /**
     * Retrieves the base of the current loaded map directly from the game, updating the cache used by {@link #getBase()}
     * if necessary.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     * @deprecated replaced by {@link Scene#getCurrentBase()}.
     */
    @Deprecated
    public static Coordinate getCurrentBase() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getCurrentBase()");
        var base = OSRSScene.getCurrentBase();

        //Base can get sometimes get desynced on teleport/death/other (cause unknown), causing functionality that relies on it to fail.
        if (!Objects.equals(base, getBase())) {
            log.debug("Current base differs from cached base, updating it.");
            OSRSScene.setStoredBase(base);
        }
        return base;
    }


    /**
     * @deprecated replaced by {@link Scene#getCollisionFlags()}.
     */
    @Nullable
    @Deprecated
    public static int[][][] getCollisionFlags() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getCollisionFlags()");
        return Scene.getCollisionFlags();
    }


    /**
     * @deprecated replaced by {@link Scene#getCollisionFlags(int)}.
     */
    @Nullable
    @Deprecated
    public static int[][] getCollisionFlags(int plane) {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getCollisionFlags()");
        return Scene.getCollisionFlags(plane);
    }

    /**
     * @deprecated replaced by {@link Scene#getCurrentPlane()}.
     */
    @Deprecated
    public static int getCurrentPlane() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getCurrentPlane()");
        return OSRSScene.getCurrentPlane();
    }

    /**
     * Gets the depth (quantity of planes)
     *
     * @see Region#getFloors
     * @deprecated replaced by {@link Scene#FLOORS}
     */
    @Deprecated
    public static int getDepth() {
        return getFloors();
    }

    /**
     * @deprecated replaced by {@link Scene#FLOORS}.
     */
    @Deprecated
    public static int getFloors() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getFloors()");
        return 4;
    }

    /**
     * @deprecated replaced by {@link Scene#getHighPrecisionBase()}.
     */
    @Deprecated
    public static Coordinate.HighPrecision getHighPrecisionBase() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getHighPrecisionBase()");
        final Coordinate base = getBase();
        if (base != null) {
            final int shift = 7;
            return new Coordinate.HighPrecision(base.getX() << shift, base.getY() << shift,
                base.getPlane()
            );
        } else {
            return null;
        }
    }

    /**
     * @deprecated replaced by {@link Scene#getLoadedRegionIds()}.
     */
    @Deprecated
    public static int[] getLoadedRegionIds() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getLoadedRegionIds()");
        return OpenRegion.getLoadedRegionIds();
    }

    /**
     * @deprecated replaced by {@link Scene#HEIGHT}.
     */
    @Deprecated
    public static int getHeight() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getHeight()");
        return 104;
    }

    /**
     * @deprecated replaced by {@link Scene#WIDTH}.
     */
    @Deprecated
    public static int getWidth() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getWidth()");
        return 104;
    }

    /**
     * Contains the raw data (as it is represented in the game client) that defines the currently loaded instance template chunks.
     *
     * @see Template
     * @see #getTemplates()
     * @deprecated replaced by {@link Scene#getLoadedChunkIds()}.
     */
    @Deprecated
    public static int[][][] getInstanceTemplateChunks() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getInstanceTemplateChunks()");
        return Scene.getLoadedChunkIds();
    }

    /**
     * @deprecated replaced by {@link Scene#getRenderFlags()}.
     */
    @Deprecated
    public static byte[][][] getRenderFlags() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getRenderFlags()");
        return Scene.getRenderFlags();
    }

    /**
     * @deprecated replaced by {@link Scene#getTileHeights()}.
     */
    @Deprecated
    public static int[][][] getHeights() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getHeights()");
        return Scene.getTileHeights();
    }

    /**
     * @deprecated replaced by {@link Scene#getBase(int)}.
     */
    @Deprecated
    public static Coordinate getBase(int plane) {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getBase()");
        return OSRSScene.getBase(plane);
    }

    /**
     * @deprecated replaced by {@link Scene#getHoveredEntities()}.
     */
    @Deprecated
    public static List<Entity> getHoveredEntities() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getHoveredEntities()");
        return OSRSScene.getHoveredEntities();
    }

    /**
     * @deprecated replaced by {@link Scene#getLoadedChunks()}
     */
    @Deprecated
    public static List<Template> getTemplates() {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getTemplates()");
        List<Template> templates = new ArrayList<>();
        int[][][] chunks3d = getInstanceTemplateChunks();
        for (int[][] chunks2d : chunks3d) {
            for (int[] chunks : chunks2d) {
                for (int templateId : chunks) {
                    if (templateId > 0) {
                        templates.add(new Template(templateId));
                    }
                }
            }
        }
        return templates;
    }

    /**
     * @deprecated replaced by {@link Chunk#containing(int, int, int)}
     */
    @Deprecated
    public static Template getTemplate(int localX, int localY, int plane) {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getTemplate()");
        if (!isInstanced()) {
            return null;
        }
        var templates = getInstanceTemplateChunks();
        return new Template(templates[plane][localX / 8][localY / 8]);
    }

    /**
     * @deprecated replaced by {@link Chunk#containing(Locatable)}
     */
    @Deprecated
    public static Template getTemplate(Coordinate loaded) {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#getTemplate()");
        if (!isInstanced()) {
            return null;
        }
        Coordinate.SceneOffset offset = loaded.getSceneOffset();
        if (!offset.isValid()) {
            return null;
        }
        return getTemplate(offset.getX(), offset.getY(), loaded.getPlane());
    }

    /**
     * @deprecated does nothing anymore
     */
    @Deprecated
    public static void cacheCollisionFlags(boolean enable) {
        ClientUI.showDeprecationWarning(Environment.getBot(), "Region#cacheCollisionFlags()");
    }

    private static int rotateTemplateX(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkX;
            case 1:
                return chunkY;
            case 2:
                return (8 - 1) - chunkX;
            case 3:
                return (8 - 1) - chunkY;
        }
        throw new IllegalStateException(
            "Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }

    private static int rotateTemplateY(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkY;
            case 1:
                return (8 - 1) - chunkX;
            case 2:
                return (8 - 1) - chunkY;
            case 3:
                return chunkX;
        }
        throw new IllegalStateException(
            "Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }

    public interface CollisionFlags {

        //Legacy
        int NORTH_WEST_BOUNDARY_OBJECT = 0b1;
        int NORTH_BOUNDARY_OBJECT = 0x2;
        int NORTH_EAST_BOUNDARY_OBJECT = 0x4;
        int EAST_BOUNDARY_OBJECT = 0b1000;
        int SOUTH_EAST_BOUNDARY_OBJECT = 0b10000;
        int SOUTH_BOUNDARY_OBJECT = 0x20;
        int SOUTH_WEST_BOUNDARY_OBJECT = 0x40;
        int WEST_BOUNDARY_OBJECT = 0x80;
        int OBJECT_TILE = 0x100;
        int RANGE_BLOCKING_NORTH_WEST_BOUNDARY_OBJECT = 0b1000000000;
        int RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT = 0b10000000000;
        int RANGE_BLOCKING_NORTH_EAST_BOUNDARY_OBJECT = 0b100000000000;
        int RANGE_BLOCKING_EAST_BOUNDARY_OBJECT = 0b1000000000000;
        int RANGE_BLOCKING_SOUTH_EAST_BOUNDARY_OBJECT = 0b10000000000000;
        int RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT = 0b100000000000000;
        int RANGE_BLOCKING_SOUTH_WEST_BOUNDARY_OBJECT = 0b1000000000000000;
        int RANGE_BLOCKING_WEST_BOUNDARY_OBJECT = 0b10000000000000000;
        int UNSTEPPABLE_OBJECT = 0x20000;
        int BLOCKING_FLOOR_OBJECT = 0x40000;
        /**
         * Clipped
         */
        int BLOCKED_TILE = 0b1000000000000000000000;
        int RANGE_ALLOWING_NORTH_WEST_BOUNDARY_OBJECT = 0b10000000000000000000000;
        int RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT = 0b100000000000000000000000;
        int RANGE_ALLOWING_NORTH_EAST_BOUNDARY_OBJECT = 0b1000000000000000000000000;
        int RANGE_ALLOWING_EAST_BOUNDARY_OBJECT = 0b10000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_EAST_BOUNDARY_OBJECT = 0b100000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT = 0b1000000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_WEST_BOUNDARY_OBJECT = 0b10000000000000000000000000000;
        int RANGE_ALLOWING_WEST_BOUNDARY_OBJECT = 0b100000000000000000000000000000;
        int RANGEABLE_OBJECT = 0x40000000;
        int PADDING = 0b11111111111111111111111111111111;

        interface HelperFlags {

            int BLOCKED_OFF = OBJECT_TILE | BLOCKED_TILE | BLOCKING_FLOOR_OBJECT;
        }
    }

    public interface RenderFlags {

        //Such as "air" (ex: area around lumbridge castle, planes 1+) and water
        int CLIPPED = 0x1;
        //Not the proper name but I need it available here and haven't found time to debug it properly
        int LOWER_OBJECTS_TO_OVERRIDE_CLIPPING = 0x2;
        int UNDER_ROOF = 0x4;
        int FORCE_TO_BOTTOM = 0x8;
        int ROOF = 0x10;
        //who knows...
        //int FORCE_RENDER_PLANE_0 = 0x8;
        //int RENDER_ABOVE_PLANE= 0x8 according to runelites instance minimap code....
        //Possibly don't add objects
    }

    /**
     * @deprecated replaced by {@link Chunk}
     */
    @Getter
    @ToString
    @Deprecated
    public static class Template {

        /**
         * For reference, a chunk is an 8x8 map sub-region
         */
        private final int id, plane, x, y, orientation;

        public Template(int templateId) {
            id = templateId;
            orientation = templateId >> 1 & 0x3;
            y = (templateId >> 3 & 0x7FF) * 8;
            x = (templateId >> 14 & 0x3FF) * 8;
            plane = templateId >> 24 & 0x3;
        }

        public Coordinate getBase() {
            return new Coordinate(x, y, plane);
        }

        public Area.Rectangular getArea() {
            Coordinate base = getBase();
            return Area.rectangular(base, base.derive(7, 7));
        }

        public boolean contains(Coordinate coordinate) {
            Area.Rectangular area = getArea();
            return area.contains(coordinate) || area.contains(fromGlobalCoordinate(coordinate));
        }

        public Coordinate fromGlobalCoordinate(Coordinate loaded) {
            Coordinate.SceneOffset offset = loaded.getSceneOffset();
            if (!offset.isValid()) {
                return null;
            }
            int chunkX = offset.getX() & (8 - 1);
            int chunkY = offset.getY() & (8 - 1);
            int rotation = (4 - orientation) % 4;
            return new Coordinate(x + rotateTemplateX(chunkX, chunkY, rotation), y + rotateTemplateY(chunkX, chunkY, rotation), plane);
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Template && ((Template) obj).id == id;
        }
    }
}
