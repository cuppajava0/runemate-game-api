package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.annotations.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import org.apache.commons.lang3.*;
import org.jetbrains.annotations.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
@RequiredArgsConstructor
public abstract class InterfaceComponent implements Validatable, Identifiable, Interactable, Renderable, Onymous {

    private final OpenInterfaceComponent component;
    private InterfaceComponent cachedLayer;
    private Type cachedType;

    protected OpenInterfaceComponent component() {
        return component;
    }

    public List<String> getActions() {
        return component.getActions();
    }

    public String[] getRawActions() {
        return component.getRawActions();
    }

    @Nullable
    public final String getSpellActionName() {
        return component.getSpellActionName();
    }

    @Nullable
    public final InteractableRectangle getBounds() {
        int width = getWidth();
        if (width <= 0) {
            return null;
        }
        int height = getHeight();
        if (height <= 0) {
            return null;
        }
        InterfaceComponent layer = getLayer();
        if (layer == null) {
            return getRootBounds();
        }
        int x = 0;
        int y = 0;
        final InteractableRectangle layerBounds = layer.getBounds();
        if (layerBounds != null) {
            x = layerBounds.x;
            y = layerBounds.y;
        }
        final Point scrollShift = layer.getScrollShift();
        if (scrollShift != null) {
            x -= scrollShift.x;
            y -= scrollShift.y;
        }
        return new InteractableRectangle(
            x + component().getRelativeX(),
            y + component().getRelativeY(),
            width, height
        );
    }


    public final int getChildQuantity() {
        return component.getChildCount();
    }

    @Nullable
    public abstract InterfaceComponent getChild(int index);

    @Nullable
    public abstract InterfaceComponent getChild(Predicate<InterfaceComponent> predicate);

    public abstract List<InterfaceComponent> getChildren();

    public final List<InterfaceComponent> getChildren(final Predicate<InterfaceComponent> predicate) {
        if (predicate == null) {
            return getChildren();
        }
        return newQuery().filter(predicate).results().asList();
    }

    @Nullable
    public final ItemDefinition getContainedItem() {
        int id = getContainedItemId();
        return id != -1 ? ItemDefinition.get(id) : null;
    }


    public final int getContainedItemId() {
        return component.getItemId();
    }

    /**
     * @deprecated {@link InterfaceContainer} API deprecated
     */
    @Deprecated
    public InterfaceContainer getContainer() {
        return InterfaceContainers.getAt(component().getId() >> 16);
    }

    @Nullable
    public InterfaceComponent getParentComponent() {
        if (!isChildComponent()) {
            return null;
        }
        return Interfaces.getAt(component.getId() >> 16, component.getId() & 0xFFFF);
    }

    public final int getSpecializationIndicator() {
        //1337 = "peakhole"/viewport
        //1338 = minimap
        //1354 = magic spell container? 161,66
        //AKA clientcode
        return component.getContentType();
    }


    public final int getFontId() {
        return component.getFontId();
    }

    /*
    public final InterfaceComponent getDragParent() {
        long dragUID = client.rmi().uid("RTComponent.dragParent", uid);
        if (dragUID != 0) {
        //TODO make interface component object from this
        }
        return null;
    }*/


    public final int getHeight() {
        return component.getHeight();
    }

    /**
     * The ID of this component (or the component parent for children) as a product of the indices that locate this component within
     * the underlying component manager:
     * <p>
     * <code>id = containerIndex &lt;&lt; 16 | parentIndex</code>
     * </p>
     * <b>Important:</b> child components have the same ID as their parent component.
     * @see #isChildComponent()
     * @see #getIndex()
     */
    public final int getId() {
        return component.getId();
    }

    /**
     * Returns the index of this component within the containing component array. For grandchildren, this will be the index of this component within the
     * parent components array of children, otherwise, this will be the index within the so-called "container".
     */
    public final int getIndex() {
        if (isChildComponent()) {
            return component.getIndex();
        }
        return component.getId() & 0xFFFF;
    }

    /**
     * @return true if this component is the child of another component
     */
    public boolean isChildComponent() {
        return component.getIndex() != -1;
    }

    public InterfaceComponent getLayer() {
        //Confirmed by line child.layer = (child.id = parent.id);
        InterfaceComponent parent = getParentComponent();
        if (parent != null) {
            return parent;
        }
        if (cachedLayer == null) {
            int layerId = getLayerId();
            if (layerId != -1) {
                cachedLayer = Interfaces.getAt(layerId >>> 16, layerId & 0xFFFF);
            } else {
                cachedLayer = null;
            }
        }
        return cachedLayer;
    }


    public int getLayerId() {
        int layerId = component().getParentId();
        if (layerId != -1) {
            return layerId;
        }
        return component.getLayerId(component.getId() >> 16);
    }

    public int getLayerDepth() {
        int depth = 0;
        InterfaceComponent layer = this;
        while ((layer = layer.getLayer()) != null) {
            depth++;
        }
        return depth;
    }

    /**
     * Gets the name of the interface as shown in the menu
     */
    @Override
    public final String getName() {
        String name = component().getName();
        return name == null || name.isEmpty() ? null : name;
    }

    public final int getProjectedBufferId() {
        final int id = getProjectedEntityId();
        if (id > 0) {
            return id;
        }
        return -1;
    }

    public abstract int getContainedItemQuantity();

    /**
     * Gets the animation being used to animate the projected entity
     */
    public abstract int getProjectedEntityAnimationId();

    /**
     * Gets the information about the entity being projected and returns it (if it's an Item)
     */
    @Nullable
    public final ItemDefinition getProjectedItem() {
        int type = getProjectedEntityType();
        if (type == 4) {
            return ItemDefinition.get(getProjectedEntityId());
        }
        return null;
    }

    /**
     * Gets the information about the entity being projected and returns it (if it's a Player)
     */
    @Nullable
    public final NpcDefinition getProjectedNpc() {
        int type = getProjectedEntityType();
        if (type == 2 || type == 6) {
            return NpcDefinition.get(getProjectedEntityId());
        }
        return null;
    }

    /**
     * Gets the information about the entity being projected and returns it (if it's a Player)
     */
    @Nullable
    public final Player getProjectedPlayer() {
        int type = getProjectedEntityType();
        if (type == 3 || type == 5) {
            final int index = getProjectedEntityId();
            if (index >= 0 && index < 2048) {
                return OSRSPlayers.getAt(index);
            } else if (index == -1) {
                return OSRSPlayers.getLocal();
            }
        } else if (type == 7) {
            return OSRSPlayers.getLocal();
        }
        return null;
    }


    @Nullable
    public Point getScrollShift() {
        Type type = getType();
        if (!Type.CONTAINER.equals(type)) {
            return null;
        }
        int xShift = component().getHorizontalScrollbarPosition();
        int yShift = component().getVerticalScrollbarPosition();
        return new Point(xShift, yShift);
    }


    public final int getSpriteBorderInset() {
        if (!Type.SPRITE.equals(getType())) {
            return 0;
        }
        return component.getBorderThickness();
    }


    @Nullable
    public final Color getSpriteFillColor() {
        if (!Type.SPRITE.equals(getType())) {
            return null;
        }
        return new Color(component().getShadowColor());
    }


    public final int getSpriteId() {
        if (!Type.SPRITE.equals(getType())) {
            return -1;
        }
        return component.getSpriteId();
    }


    public final int getSpriteRotation() {
        if (!Type.SPRITE.equals(getType())) {
            return 0;
        }
        return component.getSpriteRotation();
    }

    /**
     * Gets the text of an InterfaceComponent with type LABEL or TOOLTIP with the "jagtags" (formatting tags) stripped.
     *
     * @return #getRawText() formatted with JagTags.remove()
     */
    @Nullable
    public final String getText() {
        var text = getRawText();
        return text != null ? JagTags.remove(text) : null;
    }

    /**
     * Gets the raw text of an InterfaceComponent with type LABEL or TOOLTIP
     *
     * @return a String if the interface component is one of the specified types and it has text, otherwise null.
     */
    @Nullable
    public final String getRawText() {
        var type = getType();
        if (!Type.LABEL.equals(type) && !Type.TOOLTIP.equals(type)) {
            return null;
        }
        return component.getText();
    }

    public final Color getTextColor() {
        return new Color(component().getColor());
    }

    public final Type getType() {
        if (cachedType == null) {
            int typeId = component.getType();
            cachedType = Type.resolve(typeId);
            if (typeId != -1 && Type.UNDOCUMENTED.equals(cachedType) && Environment.isDevMode()) {
                log.warn("{} has an undocumented type opcode of {}, please alert the RuneMate team!", this, typeId);
            }
        }
        return cachedType;
    }


    public final int getWidth() {
        return component.getWidth();
    }

    @Override
    public int hashCode() {
        int res = component.getId() >> 16;
        res = 31 * res + (component.getId() & 0xFFFF);
        if (component.getIndex() != -1) {
            res = (31 * res) + component.getIndex();
        }
        return res;
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof InterfaceComponent casted) {
            return getId() == casted.getId()
                && component().getIndex() == casted.component().getIndex();
        }
        return false;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(", ", "InterfaceComponent[", "]");
        joiner.add(String.valueOf(getId() >> 16));
        joiner.add(String.valueOf(getId() & 0xFFFF));
        if (isChildComponent()) {
            joiner.add(String.valueOf(getIndex()));
        }
        return joiner.toString();
    }


    @Override
    public final boolean isValid() {
        return OpenClient.validate(component.getUid());
    }


    @Override
    public boolean isVisible() {
        //The code is accurate, the + 20 is a one cycle buffer and could could perhaps be reduce
        //It essentially gives it a 100ms buffer where it'll still return visible even if it's not
        //Due to latency in retrieving the values we can't reasonably expect them to perfectly match up as the game does.
        if (hidden()) {
            return false;
        }
        return component.getRenderCycle() + 20 >= RuneScape.getCurrentCycle();
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    @Nullable
    public final InteractablePoint getInteractionPoint(Point origin) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(origin) : null;
    }

    @Override
    public final boolean contains(Point point) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null && bounds.contains(point);
    }

    @Override
    public final boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return hover() && Menu.click(this, action, target);
    }

    @Override
    public final boolean interact(String action) {
        return interact(action, getName());
    }

    @Override
    public final boolean interact(Pattern action) {
        return interact(action, getName());
    }

    @Override
    public final void render(final Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    public InterfaceComponentQueryBuilder newQuery() {
        //Only container InterfaceComponents have children, and children may not have sub-children
        if (isChildComponent() || !Type.CONTAINER.equals(getType())) {
            return new InterfaceComponentQueryBuilder().provider(Collections::emptyList);
        }
        return new InterfaceComponentQueryBuilder().provider(this::getChildren);
    }


    private int getProjectedEntityType() {
        return component.getEntityType();
    }

    /**
     * @see InterfaceComponent#getProjectedItem()
     * @see InterfaceComponent#getProjectedNpc()
     * @see InterfaceComponent#getProjectedPlayer()
     */

    private int getProjectedEntityId() {
        return component.getEntityId();
    }

    private InteractableRectangle getRootBounds() {
        int root_index = getRootBoundsIndex();
        if (root_index == -1) {
            return null;
        }
        return OSRSInterfaces.getRootBounds(root_index);
    }


    private int getRootBoundsIndex() {
        //If we have a parent the root index can still != -1
        //However, if we have a parent the layer is always equal to the parent
        //The root index is only used by us when the layer is null
        //So if the parent exists, the root index shouldn't ever be needed
        return component.getArrayIndex();
    }

    @SneakyThrows
    private boolean hidden() {
        return component.getHidden();
    }

    @Getter
    public enum Type {
        /**
         * Also known as a layer
         */
        CONTAINER(0),
        /**
         * Also known as a box
         */
        BOX(3),
        /**
         * Also known as "text"
         */
        LABEL(4),
        /**
         * RuneLite calls it graphic
         */
        SPRITE(5),
        MODEL(6),
        /**
         * Maybe an item list?
         */
        TEXT_INVENTORY(7),
        TOOLTIP(8),
        LINE(9),
        UNDOCUMENTED(-1);
        private final int id;

        Type(int id) {
            this.id = id;
        }

        public static Type resolve(int opcode) {
            for (Type type : values()) {
                if (type.id == opcode) {
                    return type;
                }
            }
            return UNDOCUMENTED;
        }

    }
}
