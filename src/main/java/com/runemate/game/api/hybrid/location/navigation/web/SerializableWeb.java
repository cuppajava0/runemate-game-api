package com.runemate.game.api.hybrid.location.navigation.web;

import com.google.common.primitives.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.exceptions.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.osrs.location.navigation.web.requirements.*;
import com.runemate.game.api.osrs.location.navigation.web.vertex_types.teleports.*;
import java.io.*;
import java.util.*;
import java.util.function.*;

public class SerializableWeb extends Web implements Externalizable {

    private static final short LATEST_PROTOCOL = 14;
    private static final int RESERVED_OPCODES = 1024;
    private SerializableWeb.ProgressListener progress_listener;
    private Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>>
        requirement_deserialization_functions;
    private Map<Integer, PentFunction<Coordinate, Collection<WebRequirement>, Collection<WebRequirement>, Integer, ObjectInput, ? extends WebVertex>>
        vertex_deserialization_functions;

    public SerializableWeb(Collection<WebVertex> vertices) {
        super(vertices);
    }

    public SerializableWeb(int estimatedVertexCount) {
        super(estimatedVertexCount);
    }

    public SerializableWeb() {
        super();
    }

    public static SerializableWeb deserialize(byte[] bytes)
        throws IOException, ClassNotFoundException {
        SerializableWeb sw = new SerializableWeb();
        sw.readExternal(new ObjectInputStream(new ByteArrayInputStream(bytes)));
        return sw;
    }

    public static Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>> getPredefinedRequirementDeserializationFunctions() {
        Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>>
            predefined_functions = new HashMap<>(SerializableWeb.RESERVED_OPCODES);
        predefined_functions.put(0, SkillRequirement::new);
        predefined_functions.put(1, VarpRequirement::new);
        predefined_functions.put(2, ItemRequirement::new);
        predefined_functions.put(3, VarpbitsRequirement::new);
        predefined_functions.put(4, MembersRequirement::new);
        predefined_functions.put(5, WildernessRequirement::new);
        predefined_functions.put(6, MoneyRequirement::new);
        predefined_functions.put(7, QuestRequirement::new);
        predefined_functions.put(8, QuestPointRequirement::new);
        predefined_functions.put(9, UnmeetableRequirement::new);
        predefined_functions.put(10, CooldownRequirement::new);
        predefined_functions.put(11, CombatLevelRequirement::new);
        predefined_functions.put(12, AchievementDiaryRequirement::new);
        return predefined_functions;
    }

    public static Map<Integer, PentFunction<Coordinate, Collection<WebRequirement>, Collection<WebRequirement>, Integer, ObjectInput, ? extends WebVertex>> getPredefinedVertexDeserializationFunctions() {
        Map<Integer, PentFunction<Coordinate, Collection<WebRequirement>, Collection<WebRequirement>, Integer, ObjectInput, ? extends WebVertex>>
            predefined_functions = new HashMap<>(RESERVED_OPCODES);
        predefined_functions.put(0, CoordinateVertex::new);
        predefined_functions.put(1, BasicObjectVertex::new);
        predefined_functions.put(3, BasicItemTeleportVertex::new);
        predefined_functions.put(4, ObjectBankVertex::new);
        predefined_functions.put(5, NpcBankVertex::new);
        predefined_functions.put(6, DepositBoxVertex::new);
        predefined_functions.put(7, BasicNpcVertex::new);
        predefined_functions.put(8, DialogNpcVertex::new);
        predefined_functions.put(9, DialogObjectVertex::new);
        predefined_functions.put(10, DialogItemTeleportVertex::new);
        predefined_functions.put(11, TeleportSpellVertex::new);
        predefined_functions.put(12, GrandExchangeVertex::new);
        predefined_functions.put(14, SpiritTreeVertex::new);
        predefined_functions.put(15, UseItemOnObjectVertex::new);
        predefined_functions.put(16, PrayerAltarVertex::new);
        predefined_functions.put(17, InterfaceItemTeleportVertex::new);
        predefined_functions.put(18, MinigameTeleportVertex::new);
        predefined_functions.put(19, FairyRingVertex::new);
        predefined_functions.put(20, GnomeGliderVertex::new);
        return predefined_functions;
    }

    public boolean addRequirementDeserializationProtocol(
        int opcode,
        BiFunction<Integer, ObjectInput, ? extends WebRequirement> function
    ) {
        if (this.getRequirementDeserializationFunctions()
            .put(SerializableWeb.RESERVED_OPCODES + opcode, function) != null) {
            throw new IllegalStateException(
                "There is already a requirement defined for opcode " + opcode);
        }
        return true;
    }

    public boolean addVertexDeserializationProtocol(
        int opcode,
        PentFunction<Coordinate, Collection<WebRequirement>, Collection<WebRequirement>, Integer, ObjectInput, ? extends WebVertex> function
    ) {
        if (this.getVertexDeserializationFunctions()
            .put(SerializableWeb.RESERVED_OPCODES + opcode, function) != null) {
            throw new IllegalStateException(
                "There is already a vertex defined for opcode " + opcode);
        }
        return true;
    }

    public Map<Integer, PentFunction<Coordinate, Collection<WebRequirement>, Collection<WebRequirement>, Integer, ObjectInput, ? extends WebVertex>> getVertexDeserializationFunctions() {
        if (vertex_deserialization_functions == null) {
            vertex_deserialization_functions =
                SerializableWeb.getPredefinedVertexDeserializationFunctions();
        }
        return vertex_deserialization_functions;
    }

    public byte[] serialize() throws IOException {
        return this.serialize(null);
    }

    public byte[] serialize(SerializableWeb.ProgressListener listener) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        progress_listener = listener;
        this.writeExternal(new ObjectOutputStream(baos));
        progress_listener = null;
        return baos.toByteArray();
    }

    public void setProgressListener(SerializableWeb.ProgressListener listener) {
        progress_listener = listener;
    }

    @Override
    public void writeExternal(ObjectOutput stream) throws IOException {
        stream.writeShort(LATEST_PROTOCOL);
        List<WebVertex> vertices = getVertices();
        Map<WebVertex, Integer> vertice_map = new IdentityHashMap<>(vertices.size());
        ListIterator<WebVertex> vertex_iterator = vertices.listIterator();
        while (vertex_iterator.hasNext()) {
            int index = vertex_iterator.nextIndex();
            vertice_map.put(vertex_iterator.next(), index);
        }
        stream.writeInt(vertices.size());
        for (WebVertex vertex : vertices) {
            stream.writeInt(vertex.getPosition().hashCode());
            Map<WebVertex, Double> outputs = vertex.getOutputCosts();
            int outputCount = outputs.size();
            if (outputCount == 0 && vertex.getInputCosts().isEmpty()) {
                throw new MalformedWebException(vertex + " is isolated from all other vertices.");
            }
            stream.writeByte(outputCount);
            for (Map.Entry<WebVertex, Double> output : outputs.entrySet()) {
                Integer output_index = vertice_map.get(output.getKey());
                if (output_index == null) {
                    throw new MalformedWebException(
                        output.getKey() + " is declared as the target of an edge of " + vertex +
                            ", but is not in the web.");
                }
                stream.writeInt(output_index);
                stream.writeDouble(output.getValue());
            }
            Collection<WebRequirement> requirements = vertex.getRequirements();
            if (requirements.size() > Byte.MAX_VALUE) {
                throw new MalformedWebException(
                    "Each vertex can only have " + Byte.MAX_VALUE + " requirements. " + vertex +
                        " has " + requirements.size());
            }
            stream.writeByte(requirements.size());
            for (WebRequirement requirement : requirements) {
                if (requirement instanceof SerializableRequirement) {
                    SerializableRequirement srequirement = (SerializableRequirement) requirement;
                    stream.writeShort(srequirement.getOpcode());
                    stream.writeBoolean(requirement.inverted());
                    if (!srequirement.serialize(stream)) {
                        throw new WebSerializationException("Failed to serialize " + srequirement);
                    }
                    if (progress_listener != null) {
                        progress_listener.onRequirementSerialized(requirement);
                    }
                    Set<WebRequirement> alternatives = new HashSet<>();
                    WebRequirement alternative = requirement;
                    while ((alternative = alternative.getAlternative()) != null) {
                        if (alternative instanceof SerializableRequirement) {
                            alternatives.add(alternative);
                        } else {
                            throw new MalformedWebException(requirement + " isn't serializable.");
                        }
                    }
                    if (alternatives.size() > Byte.MAX_VALUE) {
                        throw new MalformedWebException(
                            "Each vertex can only have " + Byte.MAX_VALUE +
                                " alternative requirements. " + vertex + " has " +
                                alternatives.size());
                    }
                    stream.writeByte(alternatives.size());
                    for (WebRequirement alt : alternatives) {
                        if (alt instanceof SerializableRequirement) {
                            stream.writeShort(((SerializableRequirement) alt).getOpcode());
                            stream.writeBoolean(alt.inverted());
                            if (!((SerializableRequirement) alt).serialize(stream)) {
                                throw new WebSerializationException(
                                    "Failed to serialize alternative " + requirement);
                            }
                            if (progress_listener != null) {
                                progress_listener.onRequirementSerialized(alt);
                            }
                        } else {
                            throw new MalformedWebException(requirement + " isn't serializable.");
                        }
                    }
                } else {
                    throw new MalformedWebException(requirement + " isn't serializable.");
                }
            }
            Collection<WebRequirement> blockingRequirements = vertex.getBlockingConditions();
            if (blockingRequirements.size() > Byte.MAX_VALUE) {
                throw new MalformedWebException(
                    "Each vertex can only have " + Byte.MAX_VALUE + " blocking requirements. " +
                        vertex + " has " + blockingRequirements.size());
            }
            stream.writeByte(blockingRequirements.size());
            for (WebRequirement requirement : blockingRequirements) {
                if (requirement instanceof SerializableRequirement) {
                    SerializableRequirement srequirement = (SerializableRequirement) requirement;
                    stream.writeShort(srequirement.getOpcode());
                    stream.writeBoolean(requirement.inverted());
                    if (!srequirement.serialize(stream)) {
                        throw new WebSerializationException("Failed to serialize " + srequirement);
                    }
                    if (progress_listener != null) {
                        progress_listener.onRequirementSerialized(requirement);
                    }
                    Set<WebRequirement> alternatives = new HashSet<>();
                    WebRequirement alternative = requirement;
                    while ((alternative = alternative.getAlternative()) != null) {
                        if (alternative instanceof SerializableRequirement) {
                            alternatives.add(alternative);
                        } else {
                            throw new MalformedWebException(requirement + " isn't serializable.");
                        }
                    }
                    if (alternatives.size() > Byte.MAX_VALUE) {
                        throw new MalformedWebException(
                            "Each vertex can only have " + Byte.MAX_VALUE +
                                " alternative blocking requirements. " + vertex + " has " +
                                alternatives.size());
                    }
                    stream.writeByte(alternatives.size());
                    for (WebRequirement alt : alternatives) {
                        if (alt instanceof SerializableRequirement) {
                            stream.writeShort(((SerializableRequirement) alt).getOpcode());
                            stream.writeBoolean(alt.inverted());
                            if (!((SerializableRequirement) alt).serialize(stream)) {
                                throw new WebSerializationException(
                                    "Failed to serialize alternative blocking " + requirement);
                            }
                            if (progress_listener != null) {
                                progress_listener.onRequirementSerialized(alt);
                            }
                        } else {
                            throw new MalformedWebException(requirement + " isn't serializable.");
                        }
                    }
                } else {
                    throw new MalformedWebException(requirement + " isn't serializable.");
                }
            }
            stream.writeByte(vertex.getOpcode());
            if (!vertex.serialize(stream)) {
                throw new WebSerializationException("Failed to serialize " + vertex);
            }
            if (progress_listener != null) {
                progress_listener.onVertexSerialized(vertex);
            }
        }
        stream.close();
    }

    @Override
    public void readExternal(ObjectInput stream) throws IOException {
        int protocol = stream.readShort();
        if (protocol > LATEST_PROTOCOL) {
            throw new IOException("The SerializableWeb being loaded is not supported by the parser.");
        }
        int verticeCount = stream.readInt();
        PairList<WebVertex, PairList<Integer, Double>> links = new PairList<>(verticeCount);
        for (int vertexIndex = 0; vertexIndex < verticeCount; ++vertexIndex) {
            Coordinate position;
            if (protocol < 8) {
                position =
                    new Coordinate(stream.readShort(), stream.readShort(), stream.readByte());
            } else {
                position = new Coordinate(stream.readInt());
            }
            int edgeCount = stream.readByte();
            PairList<Integer, Double> edges = new PairList<>(edgeCount);
            for (int index = 0; index < edgeCount; ++index) {
                if (protocol >= 5) {
                    edges.add(stream.readInt(), stream.readDouble());
                } else {
                    edges.add(stream.readInt(), -1D);
                }
            }
            byte requirementCount = stream.readByte();
            List<WebRequirement> requirements =
                requirementCount > 0 ? new ArrayList<>(requirementCount) : Collections.emptyList();
            for (int index = 0; index < requirementCount; ++index) {
                int opcode =
                    protocol >= 8 ? stream.readShort() : UnsignedBytes.toInt(stream.readByte());
                boolean inverted = false;
                if (protocol >= 12) {
                    inverted = stream.readBoolean();
                }
                Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>>
                    deserialization = getRequirementDeserializationFunctions();
                if (deserialization.get(opcode) != null) {
                    WebRequirement requirement =
                        deserialization.get(opcode).apply(protocol, stream);
                    if (inverted) {
                        requirement.invert();
                    }
                    requirements.add(requirement);
                    if (protocol >= 10) {
                        byte alternativeCount = stream.readByte();
                        for (int jndex = 0; jndex < alternativeCount; ++jndex) {
                            opcode = stream.readShort();
                            if (protocol >= 12) {
                                inverted = stream.readBoolean();
                            }
                            WebRequirement alt =
                                deserialization.get(opcode).apply(protocol, stream);
                            if (inverted) {
                                alt.invert();
                            }
                            requirement.or(alt);
                            requirement = requirement.getAlternative();
                        }
                    }
                } else {
                    throw new MalformedWebException(
                        "An unidentified requirement opcode was encountered: " + (opcode));
                }
            }
            final List<WebRequirement> blockingRequirements;
            if (protocol >= 11) {
                byte blockingRequirementCount = stream.readByte();
                blockingRequirements =
                    blockingRequirementCount > 0 ? new ArrayList<>(blockingRequirementCount) :
                        Collections.emptyList();
                for (int index = 0; index < blockingRequirementCount; ++index) {
                    int opcode = stream.readShort();
                    boolean inverted = false;
                    if (protocol >= 12) {
                        inverted = stream.readBoolean();
                    }
                    Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>>
                        deserialization = getRequirementDeserializationFunctions();
                    if (deserialization.get(opcode) != null) {
                        WebRequirement blockingRequirement =
                            deserialization.get(opcode).apply(protocol, stream);
                        if (inverted) {
                            blockingRequirement.invert();
                        }
                        blockingRequirements.add(blockingRequirement);
                        byte alternativeCount = stream.readByte();
                        for (int jndex = 0; jndex < alternativeCount; ++jndex) {
                            opcode = stream.readShort();
                            if (protocol >= 12) {
                                inverted = stream.readBoolean();
                            }
                            WebRequirement alt =
                                deserialization.get(opcode).apply(protocol, stream);
                            if (inverted) {
                                alt.invert();
                            }
                            blockingRequirement.or(alt);
                            blockingRequirement = blockingRequirement.getAlternative();
                        }
                    } else {
                        throw new MalformedWebException(
                            "An unidentified requirement opcode was encountered: " + (opcode));
                    }
                }
            } else {
                blockingRequirements = Collections.emptyList();
            }
            int opcode = UnsignedBytes.toInt(stream.readByte());
            if (getVertexDeserializationFunctions().containsKey(opcode)) {
                links.add(getVertexDeserializationFunctions().get(opcode)
                    .apply(position, requirements, blockingRequirements, protocol, stream), edges);
            } else {
                throw new MalformedWebException(
                    "An unidentified vertex opcode was encountered: " + (opcode));
            }
        }
        List<WebVertex> vertices = links.getLefts();
        List<PairList<Integer, Double>> edges = links.getRights();
        ListIterator<WebVertex> verticeIterator = vertices.listIterator();
        //TODO determine whether the current data structures are optimal for this. There's a lot of calls to get, however it's by index.
        while (verticeIterator.hasNext()) {
            int index = verticeIterator.nextIndex();
            WebVertex vertex = verticeIterator.next();
            for (Pair<Integer, Double> edge : edges.get(index)) {
                if (protocol >= 5) {
                    vertex.addDirectedEdge(vertices.get(edge.getLeft()), edge.getRight());
                } else {
                    vertex.addDirectedEdge(vertices.get(edge.getLeft()));
                }
            }
        }
        this.addVertices(vertices);
        stream.close();
    }

    private Map<Integer, BiFunction<Integer, ObjectInput, ? extends WebRequirement>> getRequirementDeserializationFunctions() {
        if (requirement_deserialization_functions == null) {
            requirement_deserialization_functions =
                SerializableWeb.getPredefinedRequirementDeserializationFunctions();
        }
        return requirement_deserialization_functions;
    }

    public interface ProgressListener {

        default void onRequirementSerialized(WebRequirement requirement) {
        }

        default void onVertexSerialized(WebVertex vertex) {
        }
    }
}
