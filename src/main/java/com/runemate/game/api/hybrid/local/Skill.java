package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;

public enum Skill {
    ATTACK(0),
    DEFENCE(1),
    STRENGTH(2),
    CONSTITUTION(3),
    RANGED(4),
    PRAYER(5),
    MAGIC(6),
    COOKING(7),
    WOODCUTTING(8),
    FLETCHING(9, true),
    FISHING(10),
    FIREMAKING(11),
    CRAFTING(12),
    SMITHING(13),
    MINING(14),
    HERBLORE(15, true),
    AGILITY(16, true),
    THIEVING(17, true),
    SLAYER(18, true),
    FARMING(19, true),
    RUNECRAFTING(20, true),
    HUNTER(21, true),
    CONSTRUCTION(22, true);

    private final int index;
    private final int maxLevel;
    private final boolean members;

    Skill(int index) {
        this(index, 99, false);
    }

    Skill(int index, boolean members) {
        this(index, 99, members);
    }

    Skill(int index, int maxLevel, boolean members) {
        this.index = index;
        this.maxLevel = maxLevel;
        this.members = members;
    }

    public final int getIndex() {
        return index;
    }

    public final int getMaxLevel() {
        return maxLevel;
    }

    public final int getExperience() {
        if (index == -1) {
            return 0;
        }
        return OpenSkills.getCurrentExp(index);
    }

    public final int getBaseLevel() {
        if (index == -1) {
            return 0;
        }
        return OpenSkills.getBaseLevel(index);
    }

    public final int getCurrentLevel() {
        if (index == -1) {
            return 0;
        }
        return OpenSkills.getCurrentLevel(index);
    }

    public final boolean isMembersOnly() {
        return members;
    }

    public final int getExperienceAsPercent() {
        return 100 - getExperienceToNextLevelAsPercent();
    }

    public final int getExperienceToNextLevel() {
        final int nextLevel = getBaseLevel() + 1;
        if (nextLevel <= maxLevel) {
            final int experienceAtNext = Skills.getExperienceAt(nextLevel);
            if (experienceAtNext != -1) {
                return experienceAtNext - getExperience();
            }
        }
        return -1;
    }

    public final int getExperienceToNextLevelAsPercent() {
        final int baseLevel = getBaseLevel();
        if (baseLevel == 0) {
            return 0;
        }
        final int expBetweenLevels = Skills.getExperienceAt(baseLevel + 1) - Skills.getExperienceAt(baseLevel);
        if (expBetweenLevels == 0) {
            return 0;
        }
        int expToNext = getExperienceToNextLevel();
        if (expToNext == -1) {
            return 0;
        }
        return (expToNext * 100) / expBetweenLevels;
    }

    @Override
    public String toString() {
        String name = name();
        return name.charAt(0) + name.substring(1).toLowerCase();
    }
}
