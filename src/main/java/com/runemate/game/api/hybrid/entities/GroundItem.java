package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.details.*;
import lombok.*;

/**
 * An item that has either fallen or been dropped on the ground
 */
public interface GroundItem extends Item, LocatableEntity, Modeled {

    /**
     * Calls {@link #interact(String)} with "Take" as the action.
     */
    boolean take();

    default Ownership getOwnership() {
        return Ownership.NONE;
    }

    @RequiredArgsConstructor
    enum Ownership {
        NONE(0),
        SELF(1),
        OTHER(2),
        GROUP(3);

        private final int id;

        public static Ownership of(int id) {
            for (Ownership o : Ownership.values()) {
                if (o.id == id) {
                    return o;
                }
            }
            return null;
        }
    }

}
