package com.runemate.game.api.hybrid.location.navigation.basic;

import static com.runemate.game.api.hybrid.player_sense.PlayerSense.Key.*;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.google.common.collect.*;
import java.awt.*;
import java.util.List;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.*;

@Log4j2
public abstract class CoordinatePath extends Path {
    private int maxStepDeviation;
    private int maxMinimapStepDistance = 15;
    private int maxViewportStepDistance = 9;

    @Override
    public final Coordinate getNext() {
        return getNext(false);
    }

    public Coordinate getNext(boolean preferViewportTraversal) {
        var reversed = Lists.reverse(getVertices());
        if (!reversed.isEmpty()) {
            if (preferViewportTraversal) { //Gets the last coordinate in the path that is at least 95% visible.
                Shape viewport = Projection.getViewport();
                for (var coordinate : reversed) {
                    if (coordinate.getVisibility(viewport) >= 95 && Distance.to(coordinate) <= maxViewportStepDistance) {
                        return coordinate;
                    }
                }
            }
            if (Minimap.isInteractable()) {
                for (var coordinate : reversed) {
                    if (coordinate.minimap().isVisible() && Distance.to(coordinate) <= maxMinimapStepDistance) {
                        return coordinate;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public final boolean step(@NonNull TraversalOption... options) {
        boolean toggleRunBeforeStepping = PlayerSense.getAsBoolean(TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            return false;
        }

        if (!isEligibleToStep(options)) {
            return triggerStaminaEnhancement(options) && (toggleRunBeforeStepping || triggerRun(options));
        }

        boolean useViewport = ArrayUtils.contains(options, TraversalOption.PREFER_VIEWPORT);
        Coordinate next = getNext(useViewport);
        if (next == null) {
            return false;
        }

        var destination = Traversal.getDestination();
        if (next.equals(destination)) {
            log.trace("Skipping {} since it is our destination", next);
            return true;
        }

        var last = getLast();
        if (destination != null && last != null) {
            //If next is close to our destination we don't want to step unless we're close to the end of the path
            if (Distance.between(destination, next, Distance.Algorithm.CHEBYSHEV) < 5
                && Distance.between(destination, last, Distance.Algorithm.CHEBYSHEV) >= 5) {
                log.trace("Skipping {} as it's too close to our current destination", next);
                return true;
            }
        }

        //Only randomize if the next tile isn't our destination
        if (!next.equals(getLast()) && maxStepDeviation > 0) {
            next = next.randomize(maxStepDeviation, maxStepDeviation);
        }

        if (Environment.isDirectInputEnabled() && ArrayUtils.contains(options, TraversalOption.USE_DIRECT_INPUT)) {
            log.trace("Moving with direct input to {}", next);
            DirectInput.sendMovement(next);
            return true;
        }
        if (useViewport) {
            log.trace("Moving with viewport to {}", next);
            return next.isVisible() && next.interact("Walk here");
        }

        log.trace("Moving with minimap to {}", next);
        return next.minimap().isVisible() && next.minimap().click();
    }

    /**
     * Creates a new CoordinatePath which views the vertices in the opposite order.
     *
     * @return a new, reversed CoordinatePath
     */
    public final CoordinatePath reverse() {
        return PredefinedPath.create(Lists.reverse(getVertices()));
    }

    /**
     * Sets the random stepping offset to between 0 and max.
     * The stepping offset is the amount of deviation from the usual calculated point that will be clicked.
     * Setting this to a high value may result in the bot clicking a minimap button or even off of the minimap.
     *
     * @param max recommended to be around 1, but at most 4
     */
    public void setStepDeviation(int max) {
        this.maxStepDeviation = max;
    }

    /**
     * @see CoordinatePath#getMaxMinimapStepDistance
     */
    @Deprecated
    public int getMaxStepDistance() {
        return this.maxMinimapStepDistance;
    }

    /**
     * @see CoordinatePath#setMaxMinimapStepDistance
     */
    @Deprecated
    public void setMaxStepDistance(int max) {
        this.maxMinimapStepDistance = max;
    }

    public int getMaxMinimapStepDistance() {
        return this.maxMinimapStepDistance;
    }

    public void setMaxMinimapStepDistance(int max) {
        this.maxMinimapStepDistance = max;
    }

    public int getMaxViewportStepDistance() {
        return this.maxViewportStepDistance;
    }

    public void setMaxViewportStepDistance(int max) {
        this.maxViewportStepDistance = max;
    }

    @Override
    public abstract List<Coordinate> getVertices();

    @Override
    public int getLength() {
        return getVertices().size();
    }
}
