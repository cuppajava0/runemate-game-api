package com.runemate.game.api.hybrid.input.direct;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.local.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Builder
@ToString
@Log4j2
@Getter
public class MenuAction {

    @Builder.Default
    private final String option = "Automated";
    @Builder.Default
    private final String target = "";
    private final int identifier;
    private final int param0;
    private final int param1;
    private final int opcode;
    @Builder.Default
    private int itemId = -1;
    @Builder.Default
    private Interactable entity = null;

    @Nullable
    private static MenuAction forInterfaceComponentByIdent(@NonNull InterfaceComponent component, int ident, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(ident)
            .opcode(opcode.getId())
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    @Nullable
    public static MenuAction forInterfaceComponent(@NonNull InterfaceComponent component, int action, MenuOpcode opcode) {
        return forInterfaceComponentByIdent(component, MenuOpcode.getComponentMenuIdentifier(component, action), opcode);
    }

    @Nullable
    public static MenuAction forInterfaceComponent(@NonNull InterfaceComponent component, int action) {
        return forInterfaceComponent(component, action, MenuOpcode.getComponentOpcode(component));
    }

    @Nullable
    public static MenuAction forInterfaceComponent(@NonNull InterfaceComponent component, @NonNull String action) {
        return forInterfaceComponentByIdent(
            component,
            MenuOpcode.getComponentMenuIdentifier(component, action),
            MenuOpcode.getComponentOpcode(component)
        );
    }

    @Nullable
    public static MenuAction forInterfaceComponent(@NonNull InterfaceComponent component, @NonNull Pattern action) {
        return forInterfaceComponentByIdent(
            component,
            MenuOpcode.getComponentMenuIdentifier(component, action),
            MenuOpcode.getComponentOpcode(component)
        );
    }

    @Nullable
    public static MenuAction forPlayer(@NonNull Player player, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final int index = OpenPlayer.indexOf(((Entity) player).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(player)
            .build();
    }

    @Nullable
    public static MenuAction forPlayer(@NonNull Player player, int action) {
        return forPlayer(player, MenuOpcode.getPlayerOpcode(action));
    }

    @Nullable
    public static MenuAction forPlayer(@NonNull Player player, @NonNull String action) {
        return forPlayer(player, MenuOpcode.getPlayerOpcode(action));
    }

    @Nullable
    public static MenuAction forPlayer(@NonNull Player player, @NonNull Pattern action) {
        return forPlayer(player, MenuOpcode.getPlayerOpcode(action));
    }

    @Nullable
    public static MenuAction forNpc(@NonNull Npc npc, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final int index = OpenNpc.indexOf(((Entity) npc).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(npc)
            .build();
    }

    @Nullable
    public static MenuAction forNpc(@NonNull Npc npc, int action) {
        return forNpc(npc, MenuOpcode.getNpcOpcode(action));
    }

    @Nullable
    public static MenuAction forNpc(@NonNull Npc npc, @NonNull String action) {
        return forNpc(npc, MenuOpcode.getNpcOpcode(npc, action));
    }

    @Nullable
    public static MenuAction forNpc(@NonNull Npc npc, @NonNull Pattern action) {
        return forNpc(npc, MenuOpcode.getNpcOpcode(npc, action));
    }

    @Nullable
    public static MenuAction forGameObject(@NonNull GameObject obj, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final Area.Rectangular area = obj.getArea();
        if (area == null) {
            return null;
        }
        final Coordinate position = area.getBottomLeft();
        final Coordinate.SceneOffset offset = position.getSceneOffset();
        return MenuAction.builder()
            .identifier(obj.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(obj)
            .build();
    }

    @Nullable
    public static MenuAction forGameObject(@NonNull GameObject obj, int action) {
        return forGameObject(obj, MenuOpcode.getGameObjectOpcode(action));
    }

    @Nullable
    public static MenuAction forGameObject(@NonNull GameObject obj, @NonNull String action) {
        return forGameObject(obj, MenuOpcode.getGameObjectOpcode(obj, action));
    }

    @Nullable
    public static MenuAction forGameObject(@NonNull GameObject obj, @NonNull Pattern action) {
        return forGameObject(obj, MenuOpcode.getGameObjectOpcode(obj, action));
    }

    @Nullable
    public static MenuAction forGroundItem(@NonNull GroundItem item, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final Coordinate position = item.getPosition();
        if (position == null) {
            return null;
        }
        final Coordinate.SceneOffset offset = position.getSceneOffset();
        return MenuAction.builder()
            .identifier(item.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(item)
            .build();
    }

    @Nullable
    public static MenuAction forGroundItem(@NonNull GroundItem item, int action) {
        return forGroundItem(item, MenuOpcode.getGroundItemOpcode(action));
    }

    @Nullable
    public static MenuAction forGroundItem(@NonNull GroundItem item, @NonNull String action) {
        return forGroundItem(item, MenuOpcode.getGroundItemOpcode(item, action));
    }

    @Nullable
    public static MenuAction forGroundItem(@NonNull GroundItem item, @NonNull Pattern action) {
        return forGroundItem(item, MenuOpcode.getGroundItemOpcode(item, action));
    }

    /**
     * Builds a MenuAction to withdraw an item from the bank.
     * <p>
     * Default quantities: -1 (All-but-1), 0 (All), 1, 5, 10
     * <p>
     * When using any quantity other than those listed above, the default withdraw quantity must be set before this action is built.
     */
    @Nullable
    public static MenuAction forBankWithdrawal(@NonNull SpriteItem item, int quantity) {
        int defaultQuantity = Bank.getExactDefaultQuantity();
        int customQuantity = VarbitID.BANK_CUSTOM_WITHDRAW_QUANTITY.getValue(0);

        if (defaultQuantity != 1 && customQuantity != quantity) {
            log.warn("Cannot build bank withdraw action for quantity {} when custom quantity is {}", quantity, customQuantity);
            return null;
        }

        int action;
        switch (quantity) {
            case -1:
                if (customQuantity == 0) {
                    action = 7;
                } else {
                    action = 8;
                }
                break;
            case 0: //All
                if (customQuantity == 0) {
                    action = 6;
                } else {
                    action = 7;
                }
                break;
            case 1:
                action = 1;
                break;
            case 5:
                action = 3;
                break;
            case 10:
                action = 4;
                break;
            default:
                action = 5;
        }

        if (defaultQuantity != 1) {
            if (defaultQuantity == quantity) {
                action = 1;
            } else {
                action++;
            }
        }

        final InterfaceComponent component = getSpriteItemComponent(item);
        if (component == null) {
            return null;
        }

        return MenuAction.builder()
            .identifier(action)
            .opcode(MenuOpcode.CC_OP.getId())
            .param0(item.getIndex())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item)
            .build();
    }

    @Nullable
    public static MenuAction forSpriteItem(@NonNull InterfaceComponent component, @NonNull SpriteItem item, int action, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        MenuActionBuilder ma = MenuAction.builder()
            .identifier(action == 0 ? 0 : action + 1)
            .opcode(opcode.getId())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item);

        if (item.getOrigin() == SpriteItem.Origin.EQUIPMENT) {
            ma.param0(-1);
        } else {
            ma.param0(item.getIndex());
        }

        return ma.build();
    }

    @Nullable
    public static MenuAction forSpriteItem(@NonNull SpriteItem item, int action, MenuOpcode opcode) {
        final InterfaceComponent component = getSpriteItemComponent(item);
        if (component == null) {
            return null;
        }

        if (item.getOrigin() == SpriteItem.Origin.QUIVER) {
            return forInterfaceComponent(component, action);
        }

        return forSpriteItem(component, item, action, opcode);
    }

    @Nullable
    public static MenuAction forSpriteItem(@NonNull SpriteItem item, int action) {
        final InterfaceComponent component = getSpriteItemComponent(item);
        if (component == null) {
            return null;
        }

        if (item.getOrigin() == SpriteItem.Origin.QUIVER) {
            return forInterfaceComponent(component, action);
        }

        return forSpriteItem(component, item, action, MenuOpcode.getItemOpcode(component, action));
    }

    @Nullable
    public static MenuAction forSpriteItem(@NonNull SpriteItem item, @NonNull String action) {
        final InterfaceComponent component = getSpriteItemComponent(item);
        if (component == null) {
            return null;
        }

        if (item.getOrigin() == SpriteItem.Origin.QUIVER) {
            return forInterfaceComponent(component, action);
        }

        final int index = MenuOpcode.getItemActionIndex(component, action);
        if (index == -1) {
            return null;
        }

        return forSpriteItem(component, item, index, MenuOpcode.getItemOpcode(component, index));
    }

    @Nullable
    public static MenuAction forSpriteItem(@NonNull SpriteItem item, @NonNull Pattern action) {
        final InterfaceComponent component = getSpriteItemComponent(item);
        if (component == null) {
            return null;
        }

        if (item.getOrigin() == SpriteItem.Origin.QUIVER) {
            return forInterfaceComponent(component, action);
        }

        final int index = MenuOpcode.getItemActionIndex(component, action);
        if (index == -1) {
            return null;
        }

        return forSpriteItem(component, item, index, MenuOpcode.getItemOpcode(component, index));
    }

    @Nullable
    public static InterfaceComponent getSpriteItemComponent(@NonNull SpriteItem item) {
        //TODO other origin implementations
        switch (item.getOrigin()) {
            case INVENTORY:
                if (Bank.isOpen()) {
                    return Interfaces.getAt(15, 3, item.getIndex());
                }
                return Interfaces.getAt(149, 0, item.getIndex());
            case BANK:
                return Interfaces.getAt(12, 13, item.getIndex());
            case EQUIPMENT:
                return Equipment.Slot.resolve(item.getIndex()).getComponent();
            case QUIVER:
                return Quiver.getEquipmentSlotComponent();
            default:
                log.warn("MenuAction currently does not support item origin {}", item.getOrigin());
                return null;
        }
    }

}
