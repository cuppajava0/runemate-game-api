package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import org.jetbrains.annotations.*;

/**
 * An entity moving over the world-graph that is targeting an Actor
 */
public interface Projectile extends Identifiable, LocatableEntity, Rotatable, Modeled, Animable {

    /**
     * Delegates to {@link #getSpotAnimationId()}
     */
    default int getId() {
        return getSpotAnimationId();
    }

    /**
     * The character that this projectile is targeting/attacking
     *
     * @return The character if available, otherwise null
     */
    @Nullable
    Actor getTarget();

    int getSpotAnimationId();

    @Nullable
    SpotAnimationDefinition getDefinition();

    int getLaunchCycle();

    int getImpactCycle();

    Coordinate getLaunchPosition();

    boolean hasLaunched();
}
