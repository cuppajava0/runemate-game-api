package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class BankQueryBuilder extends LocatableEntityQueryBuilder<LocatableEntity, BankQueryBuilder> {
    @Override
    public BankQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends LocatableEntity>> getDefaultProvider() {
        return () -> {
            if (on != null) {
                return Banks.getLoadedOn(on.toArray(new Coordinate[0])).asList();
            }
            if (within != null) {
                if (within.size() == 1) {
                    return Banks.getLoadedWithin(within.iterator().next()).asList();
                }
                List<LocatableEntity> objects = new ArrayList<>();
                for (Area area : within) {
                    objects.addAll(Banks.getLoadedWithin(area));
                }
                return objects;
            }
            return Banks.getLoaded().asList();
        };
    }
}
