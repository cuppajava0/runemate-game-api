package com.runemate.game.api.hybrid.location.navigation.cognizant;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.location.navigation.basic.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.google.common.collect.*;
import java.util.*;
import javax.annotation.*;
import lombok.*;
import lombok.extern.log4j.*;

/**
 * A predefined list of coordinates to use as waypoints to traverse through while doing long-distance navigation.
 * <p>
 * Similar in nature to PredefinedPath but is intended to have significantly fewer coordinates defined and for each Coordinate to be spaced out much further.
 */
@Log4j2
public class WaypointPath extends Path {

    private final List<Locatable> path;

    private WaypointPath(final Locatable... path) {
        this(Arrays.asList(path));
    }

    private WaypointPath(final List<Locatable> path) {
        this.path = Collections.unmodifiableList(path);
    }

    public static WaypointPath create(@NonNull final Locatable... coordinates) {
        return new WaypointPath(coordinates);
    }

    public static WaypointPath create(@NonNull final List<Locatable> coordinates) {
        return new WaypointPath(coordinates);
    }

    @Override
    public final List<Locatable> getVertices() {
        return path;
    }

    @Override
    public boolean step(@NonNull TraversalOption... options) {
        boolean toggleRunBeforeStepping = PlayerSense.getAsBoolean(PlayerSense.Key.TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            return false;
        }
        if (isEligibleToStep(options)) {
            boolean useViewport = Arrays.asList(options).contains(TraversalOption.PREFER_VIEWPORT);
            Coordinate next = (Coordinate) getNext(useViewport);
            if (next == null) {
                return false;
            }
            if (useViewport ? !next.isVisible() : !next.minimap().isVisible()) {
                return false;
            }
            if (useViewport ? !next.interact("Walk here") : !next.minimap().click()) {
                return false;
            }
        }
        return triggerStaminaEnhancement(options) &&
            (toggleRunBeforeStepping || triggerRun(options));
    }

    @Nullable
    @Override
    public Locatable getNext() {
        return getNext(false);
    }

    @Override
    public final Locatable getNext(boolean preferViewportTraversal) {
        List<Locatable> reversed = Lists.reverse(getVertices());
        if (!reversed.isEmpty()) {
            Area.Rectangular region = Scene.getArea();
            List<Locatable> furthest = new ArrayList<>();
            for (final Locatable waypoint : reversed) {
                if (region.contains(waypoint)) {
                    furthest.add(waypoint);
                }
            }
            for (final Locatable waypoint : furthest) {
                CoordinatePath path = null;
                if (waypoint instanceof Coordinate) {
                    path = ScenePath.buildTo(waypoint);
                } else if (waypoint instanceof Area) {
                    path = ScenePath.buildTo(((Area) waypoint).getCoordinates().toArray(new Coordinate[0]));
                } else if (waypoint instanceof PredefinedEntity) {
                    LocatableEntity resolved = ((PredefinedEntity) waypoint).resolve();
                    Area waypoint_entity =
                        resolved != null ? resolved.getArea() : waypoint.getArea();
                    if (waypoint_entity != null) {
                        path = ScenePath.buildTo(waypoint_entity.getCoordinates());
                    }
                } else {
                    log.warn("WaypointPath doesn't know how to handle the waypoint {}", waypoint);
                }
                if (path != null) {
                    return path.getNext(preferViewportTraversal);
                }
            }
        }
        return null;
    }
}
