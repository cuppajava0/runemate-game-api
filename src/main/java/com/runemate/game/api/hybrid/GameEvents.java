package com.runemate.game.api.hybrid;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.handler.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.events.*;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;
import org.jetbrains.annotations.*;

public final class GameEvents {

    private GameEvents() {
    }

    @Nullable
    public static GameEvent get(String s) {
        for (GameEvent event : Universal.values()) {
            if (event.getName().equals(s)) {
                return event;
            }
        }
        return null;
    }

    public enum Universal implements GameEvent {
        BANK_PIN,
        INTERFACE_CLOSER,
        LOBBY_HANDLER,
        UNEXPECTED_ITEM_HANDLER,
        LOGIN_HANDLER,
        GENIE_HANDLER,
        NPC_DISMISSER;
        private String name;

        Universal() {
            this(null);
        }

        Universal(String name) {
            this.name = name;
        }

        @NonNull
        @Override
        public List<GameEvent> getChildren(AbstractBot bot) {
            final GameEvent parent = ((GameEventControllerImpl) bot.getGameEventController()).getGameEvent(this);
            return parent == null ? Collections.emptyList() : parent.getChildren(bot);
        }

        @Override
        public String getName() {
            if (name == null) {
                return name = StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.CAMEL_CASE);
            }
            return name;
        }
    }

    public interface GameEvent {

        String getName();

        @NonNull
        default List<GameEvent> getChildren() {
            return getChildren(Environment.getBot());
        }

        @NonNull
        default List<GameEvent> getChildren(AbstractBot bot) {
            return Collections.emptyList();
        }

        @Nullable
        default GameEvent getChild(AbstractBot bot, String name) {
            final List<GameEvent> events = getChildren(bot);
            for (final GameEvent event : events) {
                if (event.getName().equals(name)) {
                    return event;
                }
            }
            return null;
        }

        default GameEvent getChild(String name) {
            return getChild(Environment.getBot(), name);
        }

        default boolean isEnabled() {
            return isEnabled(Environment.getBot());
        }

        default boolean isEnabled(AbstractBot active) {
            if (active == null) {
                return false;
            }
            final Boolean value = (Boolean) active.getConfiguration().get("random." + getName().toLowerCase() + ".enabled");
            if (value == null) {
                return true;
            }
            return value;
        }

        default void disable(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".enabled", false);
            }
        }

        default void disable() {
            disable(Environment.getBot());
        }

        default void enable(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".enabled", true);
            }
        }

        default void enable() {
            enable(Environment.getBot());
        }

        default boolean areTrayNotificationsEnabled() {
            return areTrayNotificationsEnabled(Environment.getBot());
        }

        default boolean areTrayNotificationsEnabled(AbstractBot active) {
            if (active == null) {
                return true;
            }
            final Boolean value = (Boolean) active.getConfiguration().get("random." + getName().toLowerCase() + ".tray_notifications");
            if (value == null) {
                return true;
            }
            return value;
        }

        default void disableTrayNotifications() {
            disableTrayNotifications(Environment.getBot());
        }

        default void disableTrayNotifications(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".tray_notifications", false);
            }
        }

        default void enableTrayNotifications() {
            enableTrayNotifications(Environment.getBot());
        }

        default void enableTrayNotifications(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".tray_notifications", true);
            }
        }
    }

    public static class LoginManager {

        static final FailedLoginHandler DEFAULT_FAILED_LOGIN_HANDLER = new FailedLoginHandler() {
            @Override
            public void handle(Fail fail, int occurrence) {
                AbstractBot bot = Environment.getBot();
                if (bot == null) {
                    return;
                }

                if (fail == null) {
                    ClientUI.showAlert("A login handler failure occurred but was classified as 'null'");
                    bot.stop("Login failed");
                    return;
                }

                switch (fail) {
                    case AUTHENTICATOR -> {
                        ClientUI.showAlert("Please enter your 6 digit authenticator code to continue the login process. The bot has been paused, and will stop after 5 minutes unless unpaused.");
                        bot.pause("You need to enter your 6 digit authenticator code to continue the login process.");
                        if (!Execution.delayWhile(bot::isPaused, (int) TimeUnit.MINUTES.toMillis(5))) {
                            bot.stop("Login failed: Authenticator required");
                        }
                    }
                    case SESSION_ENDED, RUNESCAPE_UPDATED -> {
                        ClientUI.showAlert("Login failed: " + fail.description);
                        bot.stop("Login failed: " + fail.description);
                    }
                    default -> {
                        if (occurrence < fail.getAllowedRetries()) {
                            int secondsUntilRetry = (int) Random.nextGaussian(15, 90, 40);
                            ClientUI.sendTrayNotification(fail.getDescription() + " - trying again in " + secondsUntilRetry + " seconds.");
                            Execution.delay(TimeUnit.SECONDS.toMillis(secondsUntilRetry));
                        } else {
                            ClientUI.showAlert(fail.getDescription());
                            bot.stop(fail.getDescription());
                        }
                    }
                }
            }
        };

        @Deprecated
        public static void setCredentials(String username, String password, String pin) {
            OpenAccountDetails.setCredentials(username, password, pin);
        }

        public static FailedLoginHandler getFailedLoginHandler() {
            final AbstractBot active = Environment.getBot();
            FailedLoginHandler handler = null;
            if (active != null) {
                handler = (FailedLoginHandler) active.getConfiguration().get("random.failed_login_handler");
            }
            return handler == null ? DEFAULT_FAILED_LOGIN_HANDLER : handler;
        }

        public static void setFailedLoginHandler(FailedLoginHandler failedLoginHandler) {
            setFailedLoginHandler(Environment.getBot(), failedLoginHandler);
        }

        public static void setFailedLoginHandler(AbstractBot active, FailedLoginHandler failedLoginHandler) {
            if (active != null) {
                active.getConfiguration().put("random.failed_login_handler", failedLoginHandler);
            }
        }

        @Getter
        public enum Fail {
            JAG_BLOCKED("Login blocked: Blocked by JAG"),
            AUTHENTICATOR("Login blocked: Authenticator required"),
            /**
             * @deprecated we don't currently distinguish between Disabled or Banned
             */
            @Deprecated
            BAN("Login blocked: Account temporary or permanent ban"),
            DISABLED_OR_BANNED("Login blocked: Account disabled or banned"),
            GENERIC("Login failed: Too many failed attempts", 3),
            SESSION_ENDED("Session Ended"),
            CONNECTION_ERROR("Login blocked: Couldn't connect to server", 4),
            SUSPECTED_STOLEN("Login blocked: Account suspected stolen"),
            LOGIN_LIMIT_EXCEEDED("Login blocked: Login limit exceeded", 4),
            ALREADY_LOGGED_IN("Login blocked: Already logged in", 3),
            RUNESCAPE_UPDATED("Login blocked: RuneScape has updated"),
            INVALID_CREDENTIALS("Login blocked: Invalid credentials", 2),
            MISSING_DISPLAY_NAME("Login blocked: Missing display name"),
            /**
             * @deprecated not really a login failure, so no longer triggered
             */
            @Deprecated
            DISCONNECTED_FROM_SERVER("Disconnected from server", 5);

            private final String description;
            private final int allowedRetries;

            Fail(String description, int allowedRetries) {
                this.description = description;
                this.allowedRetries = allowedRetries;
            }

            Fail(String description) {
                this(description, 0);
            }

        }
    }
}
