package com.runemate.game.api.hybrid.local;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import com.runemate.game.cache.file.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

@Log4j2
public class Varbits {
    private static final VarbitLoader LOADER = new VarbitLoader();
    private static final Cache<Integer, Varbit> CACHE = CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build();
    private static final Cache<Integer, List<Varbit>> VARP_CACHE = CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build();

    /**
     * Loads the Varbit with the specified id
     */
    @Nullable
    public static Varbit load(int id) {
        if (id >= 0) {
            Varbit varbits = CACHE.getIfPresent(id);
            if (varbits != null) {
                return varbits;
            }
            try {
                CacheVarbit loaded = LOADER.load(ConfigType.VARBIT, id);
                if (loaded != null) {
                    Varbit vb = new Varbit(id, loaded.typeId, loaded.varIndex, loaded.msb, loaded.lsb);
                    CACHE.put(id, vb);
                    return vb;
                }
            } catch (final IOException ioe) {
                log.warn("Unable to load Varbit at {}", id, ioe);
            }
        }
        return null;
    }

    /**
     * Loads all the Varbits into a list.
     */
    public static List<Varbit> loadAll(Predicate<Varbit> filter) {
        Js5IndexFile archive = JS5CacheController.getLargestJS5CacheController().getIndexFile(CacheIndex.CONFIGS.getId());
        if (archive != null) {
            Map<Integer, byte[]> entries = archive.getReferenceTable().load(ConfigType.VARBIT.getId());
            if (entries != null) {
                ArrayList<Varbit> varbits = new ArrayList<>(entries.size());
                for (Map.Entry<Integer, byte[]> entry : entries.entrySet()) {
                    try {
                        CacheVarbit parsed = LOADER.form(ConfigType.VARBIT.getId(), entry.getKey(), entry.getValue());
                        if (parsed != null) {
                            final var varbit = new Varbit(entry.getKey(), parsed.typeId, parsed.varIndex, parsed.msb, parsed.lsb);
                            if (filter == null || filter.test(varbit)) {
                                varbits.add(varbit);
                            }
                        }
                    } catch (final IOException ioe) {
                        log.warn("Unable to load Varbit at {}", entry.getKey(), ioe);
                    }
                }
                return varbits;
            }
        }
        return Collections.emptyList();
    }

    public static List<Varbit> loadAll() {
        return loadAll(null);
    }

    /**
     * Gets all Varbits packed into the Varp at the passed index.
     *
     * @param index varp index
     */
    public static List<Varbit> forVarp(int index) {
        try {
            return VARP_CACHE.get(index, () -> loadAll(varbit -> varbit.getVarId() == index));
        } catch (ExecutionException e) {
            return Collections.emptyList();
        }
    }
}
