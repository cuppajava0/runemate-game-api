package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.structures.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.List;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import javafx.scene.canvas.*;
import javax.annotation.*;
import lombok.*;
import lombok.extern.log4j.*;

/**
 * A 3D in-game entity model
 */
@Log4j2
public abstract class Model implements LocatableEntity, Rotatable, Animable {

    public static final int DEFAULT_HEIGHT_OFFSET = 0;
    protected final int floorHeight;
    protected final LocatableEntity owner;

    public Model(final LocatableEntity owner) {
        this(owner, DEFAULT_HEIGHT_OFFSET);
    }

    public Model(final LocatableEntity owner, final int floorHeight) {
        this.owner = owner;
        this.floorHeight = floorHeight;
    }

    @Nullable
    @Deprecated
    protected static InteractableRectangle getBoundingRectangle(final Collection<Triangle> triangles) {
        if (!triangles.isEmpty()) {
            int minX = Integer.MAX_VALUE;
            int minY = Integer.MAX_VALUE;
            int maxX = Integer.MIN_VALUE;
            int maxY = Integer.MIN_VALUE;
            for (final Triangle triangle : triangles) {
                Point a = triangle.getA();
                if (a.x > maxX) {
                    maxX = a.x;
                }
                if (a.x < minX) {
                    minX = a.x;
                }
                if (a.y > maxY) {
                    maxY = a.y;
                }
                if (a.y < minY) {
                    minY = a.y;
                }
                Point b = triangle.getB();
                if (b.x > maxX) {
                    maxX = b.x;
                }
                if (b.x < minX) {
                    minX = b.x;
                }
                if (b.y > maxY) {
                    maxY = b.y;
                }
                if (b.y < minY) {
                    minY = b.y;
                }
                Point c = triangle.getC();
                if (c.x > maxX) {
                    maxX = c.x;
                }
                if (c.x < minX) {
                    minX = c.x;
                }
                if (c.y > maxY) {
                    maxY = c.y;
                }
                if (c.y < minY) {
                    minY = c.y;
                }
            }
            int width = maxX - minX;
            int height = maxY - minY;
            if (width > 0 && height > 0) {
                return new InteractableRectangle(minX, minY, width, height);
            }
        }
        return null;
    }

    public LocatableEntity getOwner() {
        return owner;
    }

    @Nullable
    private InteractablePoint getInteractionPoint(@NonNull List<Triangle> triangles) {
        if (triangles.isEmpty()) {
            log.debug("No triangles were provided for point selection on {}", owner);
            return null;
        }
        Triangle selected = null;
        triangles = triangles.stream().sorted(Comparator.comparingDouble(Triangle::getArea)).collect(Collectors.toList());
        if (triangles.get(triangles.size() - 1).getArea() == 0D) {
            //Remove the sides that have an invalid length
            triangles = triangles.stream().filter(t -> t.getLongestSidesLength() > 0).collect(Collectors.toList());
            //If all lines are of a length 0 we can't do anything
            if (triangles.isEmpty()) {
                log.warn("No lines are available for point selection after filtering by longest side length on {}", owner);
                return null;
            }
            //Each triangle is a line, sort by length of the longest line.
            if (triangles.size() == 1) {
                selected = triangles.get(0);
                Line selectedSide = selected.getLongestSide();
                List<InteractablePoint> sidePoints = selectedSide.getPoints();
                //TODO Confirm that this will never return the exact value sidePoints.size()
                int index = (int) Random.nextGaussian(0, sidePoints.size());
                if (index == sidePoints.size()) {
                    log.debug("The index can indeed equal sidePoints.size()");
                }
                Point sidePoint = sidePoints.get((int) Random.nextGaussian(0, sidePoints.size()));
                return new InteractablePoint(sidePoint);
            } else {
                triangles.sort(Comparator.comparingDouble(Triangle::getLongestSidesLength));
                //Pick a line based on it's length and a bellcurve.
                double desiredLength = Random.nextGaussian(
                    triangles.get(0).getLongestSidesLength(),
                    triangles.get(triangles.size() - 1).getLongestSidesLength()
                );
                PairList<Triangle, Double> closest = new PairList<>(8);
                double closestLength = Double.POSITIVE_INFINITY;
                for (Triangle triangle : triangles) {
                    double difference = Math.abs(desiredLength - triangle.getLongestSidesLength());
                    if (difference <= closestLength) {
                        closest.removeIf(td -> td.getRight() > difference);
                        closest.add(triangle, difference);
                        closestLength = difference;
                    }
                }
                //If multiple lines have the same distance to the desired length, this ensures it doesn't pick the same line each time.
                Collections.shuffle(closest, Random.getRandom());
                selected = closest.get(0).getLeft();
                if (selected != null) {
                    //Pick a point along the line of the triangle with the longest side length
                    Line selectedSide = selected.getLongestSide();
                    List<InteractablePoint> sidePoints = selectedSide.getPoints();
                    //TODO Confirm that this will never return the exact value sidePoints.size()
                    Point sidePoint = sidePoints.get((int) Random.nextGaussian(0, sidePoints.size()));
                    return new InteractablePoint(sidePoint);
                }
                return null;
            }
        }
        triangles = triangles.stream().filter(triangle -> triangle.getArea() > 0).collect(Collectors.toList());
        if (triangles.isEmpty()) {
            return null;
        }
        //If we have a single triangle, pick a point that's uniformly distributed within it, unless no integer points fall within it, then allow the selection of an edge point.
        if (triangles.size() == 1) {
            selected = triangles.get(0);
        } else {
            //Pick a triangle by using a bellcurve where the min is the smallest area and max is the largest area, then based on the value chosen find the triangle with the closest area to it.
            double desiredArea = Random.nextGaussian(triangles.get(0).getArea(), triangles.get(triangles.size() - 1).getArea());
            PairList<Triangle, Double> closest = new PairList<>(8);
            double closestArea = Double.POSITIVE_INFINITY;
            for (Triangle triangle : triangles) {
                double difference = Math.abs(desiredArea - triangle.getArea());
                if (difference <= closestArea) {
                    closest.removeIf(td -> td.getRight() > difference);
                    closest.add(triangle, difference);
                    closestArea = difference;
                }
            }
            //If multiple triangles have the same distance to the desired area, this ensures it doesn't pick the same line each time.
            Collections.shuffle(closest, Random.getRandom());
            selected = closest.get(0).getLeft();
        }
        if (selected != null) {
            Set<InteractablePoint> unique_points = selected.getPoints();
            if (unique_points.isEmpty()) {
                for (Line side : selected.getSides()) {
                    unique_points.addAll(side.getPoints());
                }
                if (unique_points.isEmpty()) {
                    return null;
                }
            }
            List<InteractablePoint> points = new ArrayList<>(unique_points);
            InteractablePoint centroid = new InteractablePoint(
                (selected.getA().x + selected.getB().x + selected.getC().x) / 3,
                (selected.getA().y + selected.getB().y + selected.getC().y) / 3
            );
            points.sort(Comparator.comparingDouble(o -> Distance.Algorithm.MANHATTAN.calculate(o.x, o.y, centroid.x, centroid.y)));
            int pointIndex = (int) Random.nextGaussian(0, points.size());
            InteractablePoint destination = points.get(pointIndex);
            if (destination == null) {
                return null;
            }
            return destination;
        }
        return null;
    }

    public void animate(int animationId, int animationFrame, int stanceId, int stanceFrame) {
        //Not all model types can be animated such as bounding models. The animation should occur before the
        //bounding model is built.
    }

    public abstract List<Triangle> projectTriangles();

    public abstract List<Triangle> projectTrianglesWithin(Shape viewport);

    protected abstract int getTriangleCount();

    public abstract int getHeight();

    @Nullable
    public Polygon projectConvexHull() {
        return projectConvexHull(projectTriangles());
    }

    @Nullable
    private Polygon projectConvexHull(List<Triangle> projectedFaces) {
        return ConvexHulls.jarvisMarchOnFaces(projectedFaces);
    }

    @Override
    public void render(Graphics2D g2d) {
        Polygon convexHull = projectConvexHull();
        if (convexHull != null) {
            g2d.drawPolygon(convexHull);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        Polygon convexHull = projectConvexHull();
        if (convexHull != null) {
            gc.strokePolygon(Renderable.convert(convexHull.xpoints), Renderable.convert(convexHull.ypoints), convexHull.npoints);
        }
    }

    @Override
    public boolean isVisible() {
        return !projectTriangles().isEmpty();
    }

    @Override
    public final double getVisibility() {
        int mpc = getTriangleCount();
        if (mpc == 0) {
            return 0;
        }
        return (projectTriangles().size() * 100.0d) / mpc;
    }

    @Override
    public final InteractablePoint getInteractionPoint(Point origin) {
        return getInteractionPoint(projectTriangles());
    }

    @Override
    public final boolean contains(final Point point) {
        return contains(projectTriangles(), point);
    }

    private boolean contains(List<Triangle> triangles, final Point point) {
        Polygon hull = projectConvexHull(triangles);
        if (hull == null || !hull.contains(point)) {
            return false;
        }
        Area bounds = new Area();
        for (final Triangle triangle : triangles) {
            Polygon polygon = triangle.toPolygon();
            //TODO is it faster and/or more accurate to use polygon.contains(Point) or our Triangle.contains(Point)
            //if (triangle.contains(point)){
            if (polygon.contains(point)) {
                return true;
            }
            bounds.add(new Area(polygon));
        }
        return bounds.contains(point);
    }

    @Override
    public final boolean click() {
        return Mouse.click(owner, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        //remove maybe
        return Menu.click(owner, action, target);
    }

    @Nullable
    @Override
    public final Coordinate getPosition(Coordinate regionBase) {
        return owner != null ? owner.getPosition(regionBase) : null;
    }

    @Nullable
    @Override
    public final Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        return owner != null ? owner.getHighPrecisionPosition(regionBase) : null;
    }

    @Nullable
    @Override
    public com.runemate.game.api.hybrid.location.Area.Rectangular getArea(Coordinate regionBase) {
        return owner != null ? owner.getArea(regionBase) : null;
    }

    @Override
    public final int getHighPrecisionOrientation() {
        final int orientation;
        return owner instanceof Rotatable && (orientation = ((Rotatable) owner).getHighPrecisionOrientation()) != -1 ? orientation : -1;
    }

    @Override
    public int getOrientationAsAngle() {
        return owner instanceof Rotatable ? ((Rotatable) owner).getOrientationAsAngle() : -1;
    }

    @Override
    public boolean isFacing(Locatable locatable) {
        return RotatableCommons.isFacing(this, locatable);
    }

    @Override
    public final int getAnimationId() {
        return owner instanceof Animable ? ((Animable) owner).getAnimationId() : -1;
    }

    /**
     * Gets the BoundingModel of this model.
     */
    @Nullable
    public abstract BoundingModel getBoundingModel();

    /**
     * Gets a bounding rectangle of the model.
     */
    @Deprecated
    public final InteractableRectangle projectBoundingRectangle() {
        return getBoundingRectangle(getBoundingModel().projectTriangles());
    }

    @Override
    public String toString() {
        return "Model(origin: " + owner + ")";
    }

    /**
     * Gets a list of colors that are used by default on the models polygons.
     * These are sometimes replaced before being rendered and these substitutions can be retrieved from
     * GameObjectDefinition#getColorSubstitutions and NpcDefinition#getColorSubstitutions.
     */
    public abstract Set<Color> getDefaultColors();

    @Override
    public Model getModel() {
        return this;
    }

    @Override
    public void setBackupModel(int[] frontBottomLeft, int[] backTopRight) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBackupModel(Pair<int[], int[]> values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setBackupModel(Model backup) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(int[] frontBottomLeft, int[] backTopRight) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(Pair<int[], int[]> values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForcedModel(Model forced) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Model model = (Model) o;
        return floorHeight == model.floorHeight && Objects.equals(owner, model.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(floorHeight, owner.hashCode());
    }
}
