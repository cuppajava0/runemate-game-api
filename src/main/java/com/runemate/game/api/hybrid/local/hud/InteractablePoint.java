package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import java.awt.*;
import java.util.regex.*;
import lombok.*;

/**
 * A java.awt.Point wrapped to provide Interactable functionality, use of this class is HIGHLY discouraged.
 */
public final class InteractablePoint extends Point implements Interactable {
    public InteractablePoint(@NonNull final Point point) {
        this(point.x, point.y);
    }

    public InteractablePoint(final int x, final int y) {
        super(x, y);
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public double getVisibility() {
        return 100;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    public InteractablePoint getInteractionPoint() {
        return this;
    }

    @NonNull
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        return this;
    }

    @Override
    public boolean contains(Point point) {
        return x == point.x && y == point.y;
    }

    @Override
    public boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(action, target);
    }

    @Override
    public String toString() {
        return "InteractablePoint(" + x + ", " + y + ')';
    }
}
