package com.runemate.game.api.hybrid.location.navigation.cognizant;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.basic.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;

import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Getter
@Log4j2
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ScenePath extends CoordinatePath {

    private interface BlockedFlags {
        int EAST = 19136776;
        int WEST = 19136896;
        int NORTH = 19136770;
        int SOUTH = 19136800;
        int NORTH_EAST = 19136782;
        int SOUTH_EAST = 19136824;
        int NORTH_WEST = 19136899;
        int SOUTH_WEST = 19136992;
    }

    private final List<Coordinate> vertices;

    private static class Builder implements BlockedFlags {
        private final Coordinate start;
        private final Collection<Coordinate> destinations;
        private final int plane;
        private final int[][] collisionFlags;
        private final Coordinate sceneBase;
        private int[] frontier;
        private int[] parents;
        private int width;
        private int height;
        private int fHead = 1;
        private int fTail = 0;

        private static final int[][] MOVEMENTS = new int[][] {
            {-1, 0, BlockedFlags.EAST },
            {1, 0, BlockedFlags.WEST },
            {0, -1, BlockedFlags.NORTH },
            {0, 1, BlockedFlags.SOUTH },
            {-1, -1, BlockedFlags.NORTH_EAST },
            {-1, 1, BlockedFlags.SOUTH_EAST },
            {1, -1, BlockedFlags.NORTH_WEST },
            {1, 1, BlockedFlags.SOUTH_WEST }
        };

        private static final int W = 0, E = 1, S = 2, N = 3, SW = 4, NW = 5, SE = 6, NE = 7;

        public Builder(Coordinate start, Collection<Coordinate> destinations) {
            this.start = start;
            this.destinations = destinations;
            this.plane = start == null ? 0 : start.getPlane();
            this.collisionFlags = Scene.getCollisionFlags(plane);
            this.sceneBase = Scene.getBase(plane);
        }

        public Builder(Coordinate start, Collection<Coordinate> destinations, int[][] collisionFlags, Coordinate sceneBase) {
            this.start = start;
            this.destinations = destinations;
            this.plane = start == null ? 0 : start.getPlane();
            this.collisionFlags = collisionFlags;
            this.sceneBase = sceneBase;
        }

        @Nullable
        public ScenePath build() {
            if (start == null || collisionFlags == null || sceneBase == null) {
                return null;
            }
            this.width = collisionFlags.length;
            this.height = collisionFlags[0].length;
            this.parents = new int[width*height];
            this.frontier = new int[4096];
            Area scene = Area.rectangular(sceneBase, sceneBase.derive(width, height));
            if (!scene.contains(start)) {
                return null;
            }
            Coordinate.SceneOffset offset = start.getSceneOffset(sceneBase);
            if (!offset.isValid()) {
                log.debug("ScenePath operating with non-standard scene size d={}x{} b={}", width, height, sceneBase);
            }

            int sceneStart = index(offset.getX(), offset.getY());
            if (sceneStart > parents.length) {
                log.warn("Index of start ([{}]{}) exceeds scene size ({})", sceneStart, offset, parents.length);
                return null;
            }

            Set<Integer> sceneDestinations = destinations.stream().filter(c -> c != null && c.getPlane() == plane && scene.contains(c))
                .map(this::index)
                .map(this::toScene).collect(Collectors.toCollection(LinkedHashSet::new));
            if (sceneDestinations.isEmpty()) {
                return null;
            }

            Arrays.fill(parents, -1);
            parents[sceneStart] = -2;
            frontier[0] = sceneStart;
            while (fHead != fTail) {
                int next = frontier[fTail];
                fTail = (fTail + 1) & 4095;
                if (sceneDestinations.contains(next)) {
                    List<Coordinate> path = new ArrayList<>();
                    path.add(sceneBase.derive(x(next), y(next)));
                    int parent;
                    while ((parent = parents[next]) != -2) {
                        next = parent;
                        path.add(sceneBase.derive(x(next), y(next)));
                    }
                    Collections.reverse(path);
                    return new ScenePath(path);
                }
                processMovements(next);
            }
            return null;
        }

        private Coordinate toScene(Coordinate c) {
            return c.derive(-sceneBase.getX(), -sceneBase.getY());
        }

        private int index(Coordinate c) {
            return index(c.getX(), c.getY());
        }

        private int index(int x, int y) {
            return x * height + y;
        }

        private int toScene(int index) {
            return index - index(sceneBase);
        }

        private int x(int index) {
            return index / height;
        }

        private int y(int index) {
            return index % height;
        }

        private void processMovements(int index) {
            int x = x(index);
            int y = y(index);
            for (int i = 0; i < MOVEMENTS.length; i++) {
                int[] move = MOVEMENTS[i];
                int newX = x + move[0];
                int newY = y + move[1];
                int flag = move[2];
                if (newX >= 0 && newX < width && newY >= 0 && newY < height
                    && parents[index(newX, newY)] == -1
                    && (collisionFlags[newX][newY] & flag) == 0) {
                    Coordinate newCoord = new Coordinate(newX, newY, plane);
                    if (i >= 4) {
                        switch (i) {
                            case SW -> {
                                move = MOVEMENTS[S];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                                move = MOVEMENTS[W];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                            }
                            case NW -> {
                                move = MOVEMENTS[N];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                                move = MOVEMENTS[W];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                            }
                            case SE -> {
                                move = MOVEMENTS[S];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                                move = MOVEMENTS[E];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                            }
                            case NE -> {
                                move = MOVEMENTS[N];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                                move = MOVEMENTS[E];
                                if ((collisionFlags[x + move[0]][y + move[1]] & move[2]) != 0) {
                                    continue;
                                }
                            }
                        }
                    }
                    int newIndex = index(newCoord);
                    frontier[fHead] = newIndex;
                    fHead = (fHead + 1) & 4095;
                    parents[newIndex] = index;
                }
            }
        }

    }

    @Nullable
    public static ScenePath buildBetween(Locatable start, Collection<? extends Locatable> destinations) {
        Coordinate startCoord = Optional.ofNullable(start).map(Locatable::getPosition).orElse(null);
        Collection<Coordinate> destCoords = destinations.stream()
            .map(Locatable::getPosition)
            .filter(Objects::nonNull)
            .toList();
        return buildBetween(startCoord, destCoords);
    }

    @Nullable
    public static ScenePath buildBetween(Coordinate start, Collection<Coordinate> destinations) {
        return new Builder(start, destinations).build();
    }

    @Nullable
    public static ScenePath buildBetween(Coordinate start, Collection<Coordinate> destinations, int[][] collisionFlags, Coordinate sceneBase) {
        return new Builder(start, destinations, collisionFlags, sceneBase).build();
    }

    @Nullable
    public static ScenePath buildBetween(Locatable start, Locatable destination) {
        return buildBetween(start, List.of(destination));
    }
    
    @Nullable
    public static ScenePath buildTo(Locatable... destinations) {
        return buildBetween(Players.getLocal(), List.of(destinations));
    }

    @Nullable
    public static ScenePath buildTo(Collection<? extends Locatable> destinations) {
        return buildTo(destinations.toArray(new Locatable[0]));
    }
}