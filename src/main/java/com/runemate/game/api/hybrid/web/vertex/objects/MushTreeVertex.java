package com.runemate.game.api.hybrid.web.vertex.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
@ToString
public class MushTreeVertex extends ObjectVertex {

    private final Pattern destination;

    public MushTreeVertex(final @NonNull Coordinate position, final @NonNull String destination) {
        super(position, Pattern.compile("Use"), GameObjects.newQuery().names("Magic Mushtree").actions("Use"));
        this.destination = Pattern.compile(destination, Pattern.CASE_INSENSITIVE);
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        final var component = getInterfaceComponent();
        if (component != null) {
            return component.interact("Continue")
                && Execution.delayWhile(() -> localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 2400);
        }

        final var object = getObject();
        if (object == null) {
            log.warn("Failed to resolve target object for {}", this);
            return false;
        }

        if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
            var ma = MenuAction.forGameObject(object, action);
            if (ma != null) {
                DirectInput.send(ma);
                return Execution.delayUntil(() -> getInterfaceComponent() != null, () -> local.getAnimationId() != -1, 3000);
            }
        }

        if (object.getVisibility() <= 40) {
            Camera.concurrentlyTurnTo(object);
        }

        return object.interact(action)
            && Execution.delayUntil(() -> getInterfaceComponent() != null, local::isMoving, 2400);
    }

    private InterfaceComponent getInterfaceComponent() {
        return Interfaces.newQuery()
            .containers(608)
            .types(InterfaceComponent.Type.LABEL)
            .texts(destination)
            .visible()
            .results()
            .first();
    }
}
