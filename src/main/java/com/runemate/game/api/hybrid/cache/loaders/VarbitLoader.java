package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class VarbitLoader extends SerializedFileLoader<CacheVarbit> {

    public VarbitLoader() {
        super(CacheIndex.CONFIGS.getId());
    }

    @Override
    protected CacheVarbit construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheVarbit();
    }
}
