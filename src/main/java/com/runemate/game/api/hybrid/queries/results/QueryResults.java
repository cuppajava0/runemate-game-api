package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.util.calculations.Random;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;
import javax.annotation.Nullable;

public abstract class QueryResults<T, QR extends QueryResults> implements Collection<T> {
    protected final List<T> backingList;
    protected final Set<T> backingSet;
    protected final ConcurrentMap<String, Object> cache;

    public QueryResults(final Collection<? extends T> results) {
        this(results, null);
    }

    public QueryResults(final Collection<? extends T> results, ConcurrentMap<String, Object> cache) {
        this.cache = cache;
        if (results instanceof List) {
            this.backingList = (List<T>) results;
            this.backingSet = new HashSet<>(results);
        } else if (results instanceof Set) {
            this.backingList = new ArrayList<>(results);
            this.backingSet = (Set<T>) results;
        } else {
            throw new IllegalArgumentException("The results should be either a List or a Set, not a " + results.getClass().getName());
        }
    }

    public final QR sort(final Comparator<? super T> comparator) {
        backingList.sort(comparator);
        return get();
    }

    protected abstract QR get();

    public final QR shuffle() {
        Collections.shuffle(backingList, Random.getRandom());
        return get();
    }

    public final QR reverse() {
        Collections.reverse(backingList);
        return get();
    }

    /**
     * Limits the results to the amount specified starting at index 0
     *
     * @param entries the amount of entries to allow
     */
    public final QR limit(final int entries) {
        return limit(0, entries);
    }

    /**
     * Limits the elements in the list to the amount specified starting at the start index
     */
    public final QR limit(final int startIndex, final int amount) {
        List<T> keep = new ArrayList<>(amount);
        for (int index = startIndex; index < size() && index - startIndex < amount; ++index) {
            keep.add(get(index));
        }
        backingList.retainAll(keep);
        backingSet.retainAll(keep);
        return get();
    }

    @Override
    public int size() {
        return Math.min(backingSet.size(), backingList.size());
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        return backingSet.contains(o);
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return backingList.iterator();
    }

    @NonNull
    @Override
    public T[] toArray() {
        return (T[]) backingList.toArray();
    }

    @NonNull
    @Override
    public T[] toArray(@NonNull Object[] array) {
        return backingList.toArray((T[]) array);
    }

    @Override
    public boolean add(T t) {
        return backingSet.add(t) && backingList.add(t);
    }

    @Override
    public boolean remove(Object o) {
        return backingSet.remove(o) && backingList.remove(o);
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> c) {
        return backingSet.containsAll(c);
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends T> c) {
        return backingSet.addAll(c) && backingList.addAll(c);
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) {
        return backingSet.removeAll(c) && backingList.removeAll(c);
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> c) {
        return backingSet.retainAll(c) && backingList.retainAll(c);
    }

    @Override
    public void clear() {
        backingSet.clear();
        backingList.clear();
    }

    public T get(final int index) {
        return backingList.get(index);
    }

    @Nullable
    public final T first() {
        int size = size();
        if (size == 0) {
            return null;
        }
        return get(0);
    }

    @Nullable
    public final T last() {
        int size = size();
        if (size == 0) {
            return null;
        }
        return get(size - 1);
    }

    @Nullable
    public final T random() {
        int size = size();
        if (size == 0) {
            return null;
        }
        return get(Random.nextInt(size));
    }

    public int indexOf(T o) {
        return backingList.indexOf(o);
    }

    public int lastIndexOf(T o) {
        return backingList.lastIndexOf(o);
    }

    @Deprecated
    public ListIterator<T> listIterator() {
        return backingList.listIterator();
    }

    @Deprecated
    public ListIterator<T> listIterator(int index) {
        return backingList.listIterator(index);
    }

    /**
     * Gets a list containing the same elements as the queries results.
     */
    public List<T> asList() {
        return backingList;
    }

    /**
     * Gets a set containing the same elements as the queries results.
     */
    public Set<T> asSet() {
        return backingSet;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + backingList;
    }
}
