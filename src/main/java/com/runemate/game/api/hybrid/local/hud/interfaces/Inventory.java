package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.experimental.*;
import org.apache.commons.lang3.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

@Log4j2
@UtilityClass
public final class Inventory {

    public static final Pattern EQUIP_ACTIONS = Pattern.compile("^Equip|Wear|Wield|Carry$");

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<SpriteItem> filter) {
        return Items.contains(getItems(), filter);
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return Items.contains(getItems(), id);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
        return Items.containsAllOf(getItems(), predicate);
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAllOf(getItems(), filters);
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return Items.containsAllOf(getItems(), ids);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
        return Items.containsAnyExcept(getItems(), filter);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyExcept(getItems(), filters);
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return Items.containsAnyExcept(getItems(), ids);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
        return Items.containsAnyOf(getItems(), filter);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyOf(getItems(), filters);
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return Items.containsAnyOf(getItems(), ids);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    public static boolean containsAnyOf(Collection<Pattern> names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<SpriteItem> filter) {
        return Items.containsOnly(getItems(), filter);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
        return Items.containsOnly(getItems(), filters);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return Items.getQuantity(getItems());
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<SpriteItem> filter) {
        return Items.getQuantity(getItems(), filter);
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filters) {
        return Items.getQuantity(getItems(), filters);
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return Items.getQuantity(getItems(), ids);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return Items.getQuantity(getItems(), names);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return Items.getQuantity(getItems(), names);
    }

    public static boolean equip(SpriteItem item) {
        log.info("Equipping {}", item);
        return item.interact(EQUIP_ACTIONS) && Execution.delayWhile(item::isValid, 1800, 2400);
    }

    public static boolean equip(SpriteItem... items) {
        log.info("Equipping {}", Arrays.toString(items));
        for (SpriteItem item : items) {
            item.interact(EQUIP_ACTIONS);
        }
        return Execution.delayUntil(() -> {
            boolean equipped = true;
            for (SpriteItem item : items) {
                equipped = equipped && !item.isValid();
            }
            return equipped;
        }, 1800, 3000);
    }

    public static InteractableRectangle getBoundsOf(final int slot) {
        final List<InteractableRectangle> bounds = getSlotBounds();
        return slot >= 0 && slot < bounds.size() ? bounds.get(slot) : null;
    }

    public static int getEmptySlots() {
        return 28 - getUsedSlots();
    }

    @Nullable
    public static SpriteItem getItemIn(final int slot) {
        return newQuery().indices(slot).results().first();
    }

    /**
     * Returns a list of all the items in your inventory (Does not require opening the tab)
     *
     * @return a list of all the items in your inventory
     */
    public static SpriteItemQueryResults getItems() {
        return Inventories.lookup(Inventories.Documented.INVENTORY);
    }

    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return Inventories.lookup(Inventories.Documented.INVENTORY, filter);
    }

    public static SpriteItemQueryResults getItems(final int... ids) {
        return getItems(Items.getIdPredicate(ids));
    }

    public static SpriteItemQueryResults getItems(final String... names) {
        return getItems(Items.getNamePredicate(names));
    }

    public static SpriteItemQueryResults getItems(final Pattern... names) {
        return getItems(Items.getNamePredicate(names));
    }

    /**
     * Technically relates to a selected item OR spell.
     */
    public static boolean isItemSelected() {
        if (!OpenClient.isInterfaceSelected()) {
            return false;
        }
        return OSRSInventory.isInventoryComponent(OpenClient.getLastSelectedInterfaceUID());
    }

    @Nullable
    public static SpriteItem getSelectedItem() {
        return OSRSInventory.getSelectedItem();
    }

    public static List<InteractableRectangle> getSlotBounds() {
        return OSRSInventory.getSlotBounds();
    }

    public static int getUsedSlots() {
        return getItems().size();
    }

    public static boolean isEmpty() {
        return getUsedSlots() == 0;
    }

    public static boolean isFull() {
        return getUsedSlots() == 28;
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.INVENTORY);
    }

}