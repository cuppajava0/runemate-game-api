package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.cache.item.*;
import java.lang.reflect.*;
import java.util.*;

public class RegionFileCacheLoader<T extends DecodedItem> extends SerializedFileLoader<T> {
    private final Class<T> clss;
    private final CacheRegion region;
    private final boolean rs3;


    public RegionFileCacheLoader(Class<T> clss, CacheRegion region, boolean rs3) {
        super(CacheIndex.MAPS.getId());
        this.clss = clss;
        this.region = region;
        this.rs3 = rs3;
    }

    @Override
    protected T construct(int entry, int file, Map<String, Object> arguments) {
        try {
            return clss.getConstructor(Coordinate.class, boolean.class)
                .newInstance(new Coordinate(region.getBaseX(), region.getBaseY(), 0), rs3);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(
                "Failed to initialize a cache region element with reflection", e);
        }
    }
}
