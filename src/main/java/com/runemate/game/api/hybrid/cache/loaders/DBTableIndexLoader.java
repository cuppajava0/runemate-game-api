package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.db.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class DBTableIndexLoader extends SerializedFileLoader<DBTableIndex> {

    public DBTableIndexLoader() {
        super(CacheIndex.DBTABLEINDEX.getId());
    }

    @Override
    protected DBTableIndex construct(final int entry, final int file, final Map<String, Object> arguments) {
        return new DBTableIndex(entry, file - 1);
    }

}
