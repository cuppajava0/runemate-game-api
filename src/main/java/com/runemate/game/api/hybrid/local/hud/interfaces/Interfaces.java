package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;

public final class Interfaces {

    private Interfaces() {
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the cachedComponents with the specified powers
     *
     * @param acceptedActions the powers that are valid (case-sensitive)
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getActionPredicate(final String... acceptedActions) {
        return interfaceComponent -> {
            for (final String action : interfaceComponent.getActions()) {
                for (final String acceptedAction : acceptedActions) {
                    if (action.equals(acceptedAction)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets the component at the specified index. This is not recommended and queries should be used instead in almost every situation.
     */
    @Nullable
    public static InterfaceComponent getAt(final int containerIndex, final int componentIndex) {
        return Optional.ofNullable(OpenInterfaceComponentManager.getLoadedComponent(containerIndex, componentIndex))
            .map(OSRSInterfaceComponent::new)
            .orElse(null);
    }

    /**
     * Gets the component at the specified index. This is not recommended and queries should be used instead in almost every situation.
     */
    @Nullable
    public static InterfaceComponent getAt(int containerIndex, int componentIndex, int subComponentIndex) {
        return Optional.ofNullable(OpenInterfaceComponentManager.getLoadedComponent(containerIndex, componentIndex, subComponentIndex))
            .map(OSRSInterfaceComponent::new)
            .orElse(null);
    }


    public static Predicate<InterfaceComponent> getHeightPredicate(final int height) {
        return interfaceComponent -> interfaceComponent.getHeight() == height;
    }

    public static InterfaceComponentQueryResults getLoaded(Predicate<InterfaceComponent> predicate) {
        final List<InterfaceComponent> components = OpenInterfaceComponentManager.getLoadedComponents()
            .stream()
            .map(ic -> (InterfaceComponent) new OSRSInterfaceComponent(ic))
            .collect(Collectors.toList());
        return Interfaces.newQuery().provider(() -> components).filter(predicate).results();
    }

    public static InterfaceComponentQueryResults getLoaded() {
        return getLoaded(null);
    }

    public static Predicate<InterfaceComponent> getNamePredicate(final String... acceptedNames) {
        return interfaceComponent -> {
            String name = interfaceComponent.getName();
            if (name != null) {
                for (String accepted : acceptedNames) {
                    if (name.equals(accepted)) {
                        return true;
                    }
                }
            } else {
                for (final String accepted : acceptedNames) {
                    if (accepted == null) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    public static Predicate<InterfaceComponent> getProjectedBufferPredicate(final int... acceptedBufferIds) {
        return interfaceComponent -> {
            final int bufferId = interfaceComponent.getProjectedBufferId();
            for (final int accepted : acceptedBufferIds) {
                if (bufferId == accepted) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<InterfaceComponent> getProjectedItemPredicate(final Predicate<ItemDefinition> itemPredicate) {
        return interfaceComponent -> {
            final ItemDefinition def = interfaceComponent.getProjectedItem();
            return def != null && itemPredicate.test(def);
        };
    }

    public static Predicate<InterfaceComponent> getProjectedNpcPredicate(final Predicate<NpcDefinition> npcPredicate) {
        return interfaceComponent -> {
            final NpcDefinition def = interfaceComponent.getProjectedNpc();
            return def != null && npcPredicate.test(def);
        };
    }

    public static Predicate<InterfaceComponent> getProjectedPlayerPredicate(final Predicate<Player> playerPredicate) {
        return interfaceComponent -> {
            final Player player = interfaceComponent.getProjectedPlayer();
            return player != null && playerPredicate.test(player);
        };
    }


    @Nullable
    public static InterfaceComponent getSelected() {
        if (isInterfaceSelected()) {
            int uid = OpenClient.getLastSelectedInterfaceUID();
            InterfaceComponent component = Interfaces.getAt(uid >>> 16, uid & 0xFFFF);
            if (component != null) {
                int subComponentIndex = OpenClient.getLastSelectedComponentId();
                if (subComponentIndex != -1) {
                    component = component.getChild(subComponentIndex);
                }
            }
            return component;
        }
        return null;
    }

    private static boolean isInterfaceSelected() {
        return OpenClient.isInterfaceSelected();
    }

    public static Predicate<InterfaceComponent> getSizePredicate(final int width, final int height) {
        return interfaceComponent -> interfaceComponent.getWidth() == width && interfaceComponent.getHeight() == height;
    }

    public static Predicate<InterfaceComponent> getTextColorPredicate(final Color... acceptedColors) {
        return interfaceComponent -> {
            final Color color = interfaceComponent.getTextColor();
            for (final Color accepted : acceptedColors) {
                if (color.equals(accepted)) {
                    return true;
                }
            }
            return false;
        };
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the cachedComponents with the specified text (.contains)
     *
     * @param acceptedText the text that is accepted by the Predicate (case-sensitive)
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getTextContainsPredicate(final String... acceptedText) {
        return getTextContainsPredicate(true, acceptedText);
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the cachedComponents with the specified text (.contains)
     *
     * @param caseSensitive whether or not the comparison is case sensitive
     * @param acceptedText  the text that is valid
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getTextContainsPredicate(final boolean caseSensitive, final String... acceptedText) {
        return component -> {
            String text = component.getText();
            if (text == null) {
                for (String accepted : acceptedText) {
                    if (accepted == null) {
                        return true;
                    }
                }
                return false;
            }
            if (!caseSensitive) {
                text = text.toLowerCase();
            }
            for (String accepted : acceptedText) {
                if (!caseSensitive) {
                    accepted = accepted.toLowerCase();
                }
                if (text.contains(accepted)) {
                    return true;
                }
            }
            return false;
        };
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the cachedComponents with the specified text (.equals)
     *
     * @param acceptedText the text that is valid (case-sensitive)
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getTextEqualsPredicate(final String... acceptedText) {
        return getTextEqualsPredicate(true, acceptedText);
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the cachedComponents with the specified text (.equals)
     *
     * @param caseSensitive whether or not the comparison is case sensitive
     * @param acceptedText  the text that is accepted by the Predicate
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getTextEqualsPredicate(final boolean caseSensitive, final String... acceptedText) {
        return interfaceComponent -> {
            String text = interfaceComponent.getText();
            if (text != null) {
                if (!caseSensitive) {
                    text = text.toLowerCase();
                }
                for (String accepted : acceptedText) {
                    if (!caseSensitive) {
                        accepted = accepted.toLowerCase();
                    }
                    if (text.equals(accepted)) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the components with the specified sprite
     *
     * @param acceptedSpriteIds the powers that are valid (case-sensitive)
     * @return a Predicate
     */
    public static Predicate<InterfaceComponent> getSpritePredicate(final int... acceptedSpriteIds) {
        return interfaceComponent -> {
            final int id = interfaceComponent.getSpriteId();
            for (final int accepted : acceptedSpriteIds) {
                if (id == accepted) {
                    return true;
                }
            }
            return false;
        };
    }

    /**
     * Gets a InterfaceComponent Predicate that can be used to get the ccmponents with the specified texture
     *
     * @param acceptedTextureIds the powers that are valid (case-sensitive)
     * @return a Predicate
     */
    @Deprecated
    public static Predicate<InterfaceComponent> getTexturePredicate(final int... acceptedTextureIds) {
        return interfaceComponent -> {
            final int id = interfaceComponent.getSpriteId();
            for (final int accepted : acceptedTextureIds) {
                if (id == accepted) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<InterfaceComponent> getVisiblePredicate() {
        return InterfaceComponent::isVisible;
    }

    public static Predicate<InterfaceComponent> getWidthPredicate(final int width) {
        return interfaceComponent -> interfaceComponent.getWidth() == width;
    }

    public static InterfaceComponentQueryBuilder newQuery() {
        return new InterfaceComponentQueryBuilder();
    }

    /**
     * Scrolls until the component is visible within the viewport
     *
     * @param component the component you want to see
     * @param viewport  the viewport restricting visibility to the component
     * @return true if the component is visible, else false
     */
    public static boolean scrollTo(InterfaceComponent component, InterfaceComponent viewport) {
        return component != null && viewport != null && scrollTo(component::getBounds, viewport.getBounds());
    }

    /**
     * Gets the percentage y-intersection of the child in the viewport
     *
     * @return 0-100% y-intersection
     */
    private static double getVisibility(final Rectangle child, final Rectangle viewport) {
        if (child == null || viewport == null) {
            return 0;
        }
        final double isect_h = child.intersection(viewport).getHeight();
        final double child_h = child.getHeight();
        return isect_h < 0 ? 0 : (isect_h / child_h) * 100;
    }

    /**
     * Distance that we need to scroll for the item to be 100% visible
     */
    private static double distanceToScroll(final Rectangle child, final Rectangle viewport) {
        return child == null || viewport == null
            ? -1
            : child.y < viewport.y ? viewport.y - child.y : child.getMaxY() > viewport.getMaxY() ? viewport.getMaxY() - child.getMaxY() : 0;
    }

    /**
     * Checks if y-intersection of the child to viewport is more than a given value
     *
     * @param child    Child component
     * @param viewport Viewport that should contain the child
     * @return true if the child-viewport y-insertion is more than a PlayerSensed value
     */
    //Open to naming suggestions
    public static boolean isVisibleInScrollpane(final Rectangle child, final Rectangle viewport) {
        return getVisibility(child, viewport) > PlayerSense.getAsDouble(PlayerSense.Key.ACCEPTABLE_INTERFACE_VISIBILITY);
    }

    public static boolean scrollTo(final Callable<Rectangle> viewableCall, final InteractableRectangle viewport) {
        final double acceptable_visibility = PlayerSense.getAsDouble(PlayerSense.Key.ACCEPTABLE_INTERFACE_VISIBILITY);
        return Execution.delayUntil(() -> {
            Rectangle child;

            if (viewableCall == null || viewport == null || (child = viewableCall.call()) == null) {
                return false;
            }

            if (getVisibility(child, viewport) > acceptable_visibility) {
                return true;
            }

            final double dist = distanceToScroll(child, viewport);
            if (dist == 0) { //We don't want DIV/0 exceptions
                return false;
            }
            //Delay between scroll ticks, increases as we get closer for rapid "bursts"
            final double baseDelay = PlayerSense.getAsDouble(PlayerSense.Key.SCROLL_DELAY_NUMERATOR) / Math.pow(Math.abs(dist), 0.4);
            //Delay after each bursts, gets shorter and smooth the closer we get to similar the player "scanning"
            final double postScrollDelay = Math.pow(Math.abs(dist), 0.4) * Math.sqrt(baseDelay);
            //Random number of scroll ticks each iteration
            final int scrollCount = Random.nextInt(6, 12);
            //Approx number of scroll ticks it will take to reach the given child
            final double totalScrolls = Math.abs(dist) / Random.nextInt(40, 50);

            //Mouse position prediction
            //This is rough but gives should simulate the player knowing the rough location of the item in the bank.
            //If scrolling, but not in the bank, this is still good as it should stop the mouse hovering the
            //absolute center of the viewport as it currently does.
            final int y_middle = viewport.y + (viewport.height / 2);

            int x, y;
            if (viewport.height == 0) {
                y = y_middle;
            } else if (dist < 0) {
                y = Random.nextInt(y_middle, viewport.y + viewport.height);
            } else {
                y = Random.nextInt(viewport.y, y_middle);
            }
            if (child.width == 0) {
                //In case viewport bounds are fucked as well
                if (viewport.width == 0) {
                    x = viewport.getInteractionPoint().x;
                } else {
                    x = Random.nextInt(viewport.x, viewport.x + viewport.width);
                }
            } else {
                x = Random.nextInt(child.x, child.x + child.width);
            }

            if (Mouse.move(new InteractablePoint(x, y))) {
                for (int i = 0; i < (scrollCount < totalScrolls ? scrollCount : totalScrolls); i++) {
                    Mouse.scroll(dist < 0, (int) Random.nextGaussian(baseDelay * 0.8, baseDelay * 1.2));
                }
                Execution.delay((int) Random.nextGaussian(postScrollDelay * 0.8D, postScrollDelay * 1.2D));
            }
            return getVisibility(child, viewport) > acceptable_visibility;
        }, 3000, 5000);
    }

    public static boolean scrollTo(SpriteItem item, InteractableRectangle viewport) {
        return item != null && viewport != null && scrollTo(item::getBounds, viewport);
    }

    public static boolean scrollTo(SpriteItem item, InterfaceComponent viewport) {
        return item != null && viewport != null && scrollTo(item::getBounds, viewport.getBounds());
    }
}
