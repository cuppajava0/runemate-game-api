package com.runemate.game.api.hybrid.location.navigation.web.vertex_types;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import lombok.*;

public class CoordinateVertex extends WebVertex implements SerializableVertex {
    public CoordinateVertex(int x, int y, int plane, Collection<WebRequirement> requirements) {
        super(new Coordinate(x, y, plane), requirements);
    }

    public CoordinateVertex(final Coordinate position, Collection<WebRequirement> requirements) {
        super(position, requirements);
    }

    public CoordinateVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> restrictions, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, restrictions, protocol, stream);
    }

    @Override
    public boolean step() {
        return step(false);
    }

    @Override
    public boolean step(boolean prefersViewport) {
        return prefersViewport ? getPosition().interact("Walk here") :
            getPosition().minimap().click();
    }

    @NonNull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final Coordinate pos = getPosition();
        final boolean prefersViewport = (boolean) args.get(PREFERS_VIEWPORT);
        final Set<Coordinate> reachable = (Set<Coordinate>) args.get(REACHABLE);
        final Area.Rectangular region = (Area.Rectangular) args.get(SCENE);
        if (pos != null
            && Distance.between(pos, (Coordinate) args.get(WebVertex.AVATAR_POS)) > 0
            && region.contains(pos)
            && reachable.contains(pos)
            &&
            (prefersViewport ? pos.getVisibility() >= 80 : pos.minimap().isVisible(region, args))) {
            return new Pair<>(this, WebPath.VertexSearchAction.CONTINUE);
        }
        return new Pair<>(null, WebPath.VertexSearchAction.CONTINUE);
    }

    @Override
    public int hashCode() {
        if (cachedHashcode == -1) {
            return cachedHashcode = position.hashCode();
        }
        return cachedHashcode;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "CoordinateVertex(x=" + position.getX() + ", y=" + position.getY() + ", plane=" +
            position.getPlane() + ')';
    }

    @Override
    public int getOpcode() {
        return 0;
    }

    @Override
    public boolean serialize(ObjectOutput stream) {
        return true;
    }

    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        return true;
    }
}
