package com.runemate.game.api.hybrid.local;

import com.runemate.client.bind.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.Timer;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.*;
import com.google.common.util.concurrent.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.*;
import lombok.extern.log4j.*;

@Log4j2
public final class Camera {

    public static final ConcurrentMap<AbstractBot, ExecutorService> executorMap =
        new ConcurrentHashMap<>(5);
    public static final ConcurrentMap<AbstractBot, FutureTask<Boolean>> lastTaskMap =
        new ConcurrentHashMap<>(5);
    private static final Color NORMAL_CHAT_TEXT_COLOR = new Color(150, 150, 150);
    private final static int FIXED_MAX_JAG_ZOOM = 1448;
    private final static int FIXED_MIN_JAG_ZOOM = 181;
    private final static int RESIZABLE_MAX_JAG_ZOOM = 3190;
    private final static int RESIZABLE_MIN_JAG_ZOOM = 398;
    //Varc values were taken from CS2-Script #42 (camera_do_zoom) and confirmed in-game
    private final static int MIN_VARC_ZOOM = 128;
    private final static int MAX_VARC_ZOOM = 896;

    private Camera() {
    }

    /**
     * Gets the current camera yaw as an angle
     *
     * @return The camera yaw as an angle (0-359), or -1 if unavailable
     */
    public static int getYaw() {
        return (int) (OpenCamera.getJagYaw() / 2048.0 * 360.0);
    }

    /**
     * Gets the normalized camera pitch
     *
     * @return The camera pitch, normalized between 0 and 1, or -1 if unavailable
     */
    public static double getPitch() {
        return Math.max(
            0,
            CommonMath.round(CommonMath.normalize(OpenCamera.getJagPitch(), 128, 383), 3)
        );
    }

    /**
     * An OSRS-only method to get the camera x
     *
     * @return a value representing an axis of the camera's position.
     */
    public static int getX() {
        if (Environment.getClientType() == ClientType.RUNELITE) {
            return OpenCamera.getLegacyCameraX();
        }
        return OpenWorldView.getTopLevelWorldView().getScene().getCameraX();
    }

    /**
     * An OSRS-only method to get the camera y
     *
     * @return a value representing an axis of the camera's position.
     */
    public static int getY() {
        if (Environment.getClientType() == ClientType.RUNELITE) {
            return OpenCamera.getLegacyCameraY();
        }
        return OpenWorldView.getTopLevelWorldView().getScene().getCameraY();
    }

    /**
     * An OSRS-only method to get the camera z
     *
     * @return a value representing an axis of the camera's position.
     */
    public static int getZ() {
        if (Environment.getClientType() == ClientType.RUNELITE) {
            return OpenCamera.getLegacyCameraZ();
        }
        return OpenWorldView.getTopLevelWorldView().getScene().getCameraZ();
    }

    public static int getJagYaw() {
        return OpenCamera.getJagYaw();
    }

    public static int getJagPitch() {
        return OpenCamera.getJagPitch();
    }

    /**
     * Gets the zoom level on OSRS in the pure form used by the OSRS game engine.
     * Not normalized at all.
     * In resizable mode, the maximum and minimum values seem to depend on the screen height.
     * Used for projection.
     *
     * @return Zoom level
     */
    public static int getJagZoom() {
        return OpenCamera.getJagZoom();
    }

    /**
     * Gets the zoom level as seen from the in-game zoom setting.
     * Not normalized at all.
     * Unlike JagZoom, the minimum and maximum values do not vary with the display mode.
     *
     * @return Zoom level. From 128 (fully zoomed out) to 896 (fully zoomed in).
     */
    public static int getZoomSetting() {
        //Varc74 could also be used, the two values seem to be always identical
        return Varcs.getInt(73);
    }

    /**
     * Gets the normalized camera zoom between 0 and 1, osrs only
     *
     * @return Normalized zoom level
     */
    public static double getZoom() {
        return CommonMath.normalize(getZoomSetting(), MIN_VARC_ZOOM, MAX_VARC_ZOOM);
    }


    /**
     * @param zoom      Normalized zoom between 0.0 and 1.0
     * @param tolerance Tolerance of the provided zoom value
     */
    public static void setZoom(double zoom, double tolerance) {
        int zoomRange = MAX_VARC_ZOOM - MIN_VARC_ZOOM;
        setZoomSetting(MIN_VARC_ZOOM + (int) (zoom * zoomRange), (int) (tolerance * zoomRange));
    }

    /**
     * Tries to set the zoom value to the value provided.
     *
     * @param desiredZoom From 128 to 896
     * @param tolerance   Absolute tolerance
     * @return True if the final zoom was within provided tolerance, false otherwise.
     */
    public static boolean setZoomSetting(int desiredZoom, int tolerance) {
        if (desiredZoom > MAX_VARC_ZOOM || desiredZoom < MIN_VARC_ZOOM) {
            throw new IllegalArgumentException(
                "The desired VarcZoom of " + desiredZoom + " is not valid"
            );
        }
        int initial = getZoomSetting();
        int offset = desiredZoom - initial;
        long startTime = System.currentTimeMillis();
        Shape viewport = Projection.getViewport();
        if (viewport != null) {
            Mouse.move(new InteractableShape(viewport));
        }
        int max_scroll_time = Random.nextInt(3000, 5000);
        if (offset > 0) {
            while (desiredZoom > getZoomSetting() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(false);
            }
        } else {
            while (desiredZoom < getZoomSetting() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(true);
            }
        }
        return Math.abs(getZoomSetting() - desiredZoom) <= tolerance;
    }

    public static boolean setJagZoom(int desiredJagZoom, int tolerance) {
        boolean resizable = OSRSInterfaceOptions.isViewportResizable();
        if (desiredJagZoom > (resizable ? RESIZABLE_MAX_JAG_ZOOM : FIXED_MAX_JAG_ZOOM) ||
            desiredJagZoom < (resizable ? RESIZABLE_MIN_JAG_ZOOM : FIXED_MIN_JAG_ZOOM)) {
            throw new IllegalArgumentException(
                "The desiredJagZoom of " + desiredJagZoom + " is not valid in " +
                    (resizable ? "resizable" : "fixed") + " mode.");
        }
        int initial = getJagZoom();
        int offset = desiredJagZoom - initial;
        long startTime = System.currentTimeMillis();
        Shape viewport = Projection.getViewport();
        if (viewport != null) {
            Mouse.move(new InteractableShape(viewport));
        }
        int max_scroll_time = Random.nextInt(3000, 5000);
        if (offset > 0) {
            while (desiredJagZoom > getJagZoom() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(false);
            }
        } else {
            while (desiredJagZoom < getJagZoom() &&
                (System.currentTimeMillis() - startTime) < max_scroll_time) {
                Mouse.scroll(true);
            }
        }
        return Math.abs(getJagZoom() - desiredJagZoom) <= tolerance;
    }

    /**
     * Turns the camera to the specified yaw using a reasonable tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw) {
        return turnTo(yaw, getPitch());
    }

    /**
     * Turns the camera to the specified yaw and pitch using a reasonable tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw, double pitch) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(yaw, pitch);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Starts turning the camera to the specified yaw and pitch using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(int yaw, double pitch) {
        return concurrentlyTurnTo(yaw, pitch,
            PlayerSense.getAsDouble(PlayerSense.Key.CAMERA_TOLERANCE)
        );
    }

    /**
     * Starts turning the camera to the specified yaw and pitch(uses PlayerSense and is asynchronized)
     * <p>
     * Bugfix by @Guru - Camera sometimes turns wrong direction
     *
     * @param tolerance - How precise does our camera movement need to be?
     *                  0 = 100% precise (must be exact), 0.05 = 95% precise, 1 = 0% precise
     */
    public static Future<Boolean> concurrentlyTurnTo(
        final int desiredYaw,
        final double desiredPitch,
        final double tolerance
    ) {
        if (desiredYaw == -1 || desiredPitch == -1) {
            return null;
        }
        if (desiredYaw < 0 || desiredYaw > 359) {
            log.warn("The desired yaw ({}) must be value between 0 and 359", desiredYaw);
            return null;
        }
        if (desiredPitch < 0 || desiredPitch > 1) {
            log.warn("The desired yaw ({}) must be value between 0 and 1", desiredPitch);
            return null;
        }
        final AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        FutureTask<Boolean> new_task = new FutureTask<>(() -> {
            Thread thread = Thread.currentThread();
            Timer timer = new Timer(2500, 3750);
            int yawOffset = RotatableCommons.getYawOffset(desiredYaw);
            double currentPitch = getPitch();
            boolean left = yawOffset < PlayerSense.getAsInteger(PlayerSense.Key.CAMERA_DIRECTION_BIAS);
            boolean down = currentPitch > desiredPitch;
            final int xKey = left ? KeyEvent.VK_LEFT : KeyEvent.VK_RIGHT;
            final int yKey = down ? KeyEvent.VK_DOWN : KeyEvent.VK_UP;
            final var xStr = left ? "LEFT" : "RIGHT";
            final var yStr = down ? "DOWN" : "UP";
            try {
                boolean moveX = Math.abs(yawOffset) > (360 * tolerance);
                boolean moveY = Math.abs(currentPitch - desiredPitch) > tolerance;
                if (moveX && !Keyboard.isPressed(xKey)) {
                    if (log.isDebugEnabled()) {
                        log.debug("Pressing {} to change yaw to {} from {} [{}]", KeyEvent.getKeyText(xKey), desiredYaw, getYaw(), xStr);
                    }
                    Keyboard.pressKey(xKey);
                }
                if (moveY && !Keyboard.isPressed(yKey)) {
                    if (log.isDebugEnabled()) {
                        log.debug(
                            "Pressing {} to change pitch to {} from {} [{}]",
                            KeyEvent.getKeyText(yKey),
                            desiredPitch,
                            getPitch(),
                            yStr
                        );
                    }
                    Keyboard.pressKey(yKey);
                }

                timer.start();
                while (bot.isRunning() && !thread.isInterrupted() && timer.isRunning() &&
                    (moveX || moveY)) {
                    if (moveX) {
                        moveX = left
                            ? RotatableCommons.getYawOffset(desiredYaw) < (360 * tolerance)
                            : RotatableCommons.getYawOffset(desiredYaw) > (360 * tolerance);

                        if (!moveX && Keyboard.isPressed(xKey)) {
                            log.debug("Releasing {} key ", xStr);
                            Keyboard.releaseKey(xKey);
                        }
                        if (moveX && !Keyboard.isPressed(xKey)) {
                            log.debug("Pressing the {} key again because we released it too early", xStr);
                            Keyboard.pressKey(xKey);
                        }
                    }
                    if (moveY) {
                        moveY = down ? getPitch() - desiredPitch > tolerance : getPitch() - desiredPitch < tolerance;
                        if (!moveY && Keyboard.isPressed(yKey)) {
                            log.debug("Releasing {} key", yStr);
                            Keyboard.releaseKey(yKey);
                        }
                        if (moveY && !Keyboard.isPressed(yKey)) {
                            log.debug("Pressing the {} key again because we released it too early", yStr);
                            Keyboard.pressKey(yKey);
                        }
                    }
                    Execution.delay(10, 40, 20);
                }
                return Math.abs(RotatableCommons.getYawOffset(desiredYaw)) <= (360 * tolerance)
                    && Math.abs(getPitch() - desiredPitch) <= tolerance;
            } catch (Exception e) {
                log.warn("Error while moving camera", e);
                return false;
            } finally {
                if (!bot.isRunning() || thread.isInterrupted() || !timer.isRunning()) {
                    if (Keyboard.isPressed(xKey)) {
                        log.debug("Releasing the yaw controlling key {}", KeyEvent.getKeyText(xKey));
                        Keyboard.releaseKey(xKey);
                    }
                    if (Keyboard.isPressed(yKey)) {
                        log.debug("Releasing the pitch controlling key {}", KeyEvent.getKeyText(yKey));
                        Keyboard.releaseKey(yKey);
                    }
                }
            }
        });
        FutureTask<Boolean> last = lastTaskMap.get(bot);
        if (last != null && !last.isDone()) {
            try {
                last.get(3, TimeUnit.SECONDS);
            } catch (Exception ignored) {
            }
            last.cancel(true);
        }
        synchronized (lastTaskMap) {
            lastTaskMap.put(bot, new_task);
        }
        synchronized (executorMap) {
            executorMap.computeIfAbsent(
                bot,
                as -> Executors.newSingleThreadExecutor(getCameraMovementThreadFactory(bot))
            ).submit(new_task);
        }
        return new_task;
    }

    private static ThreadFactory getCameraMovementThreadFactory(AbstractBot bot) {
        return new ThreadFactoryBuilder().setNameFormat(
                "camera-thread-"
                    + Environment.getRuneScapeProcessId()
                    + '-'
                    + bot.getMetadata().getName().replace("%", ""))
            .build();
    }

    /**
     * Turns the camera to the specified pitch (uses PlayerSense)
     */
    public static boolean turnTo(double pitch) {
        return turnTo(getYaw(), pitch);
    }

    /**
     * Turns the camera to the specified yaw and pitch using a specified tolerance (uses PlayerSense)
     */
    public static boolean turnTo(int yaw, double pitch, double tolerance) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(yaw, pitch, tolerance);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Turns the camera to the specified target using a reasonable tolerance (uses PlayerSense and adjusts yaw and pitch)
     */
    public static boolean turnTo(Locatable target) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    /**
     * Starts turning the camera to the specified target using a reasonable tolerance (uses PlayerSense, is asynchronized, and adjusts yaw and pitch)
     *
     * @param target the entity to be in focus
     */
    public static Future<Boolean> concurrentlyTurnTo(Locatable target) {
        if (target == null) {
            return null;
        }
        Player center = Players.getLocal();
        int angle = getAngleYawRelativeTo(center, target);
        if (angle == -1) {
            return null;
        }
        double pitch = getPitchRelativeTo(center, target);
        if (pitch < 0 || pitch > 1) {
            return null;
        }
        return concurrentlyTurnTo(angle, pitch);
    }

    public static boolean turnTo(Locatable target, int yaw) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target, yaw);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    public static Future<Boolean> concurrentlyTurnTo(Locatable target, int yaw) {
        if (target == null) {
            return null;
        }
        Player center = Players.getLocal();
        if (center == null) {
            return null;
        }
        return concurrentlyTurnTo(yaw, getPitchRelativeTo(center, target));
    }

    public static boolean turnTo(Locatable target, double pitch) {
        try {
            Future<Boolean> future = concurrentlyTurnTo(target, pitch);
            return future != null && future.get();
        } catch (InterruptedException | ExecutionException | CancellationException ee) {
            return false;
        }
    }

    public static Future<Boolean> concurrentlyTurnTo(Locatable target, double pitch) {
        if (target == null) {
            return null;
        }
        int angle = getAngleYawRelativeTo(Players.getLocal(), target);
        if (angle == -1) {
            return null;
        }
        return concurrentlyTurnTo(angle, pitch);
    }

    /**
     * Starts turning the camera to the specified yaw using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(int yaw) {
        return concurrentlyTurnTo(yaw, getPitch());
    }

    /**
     * Starts turning the camera to the specified pitch using a reasonable tolerance (uses PlayerSense and is asynchronized)
     */
    public static Future<Boolean> concurrentlyTurnTo(double pitch) {
        return concurrentlyTurnTo(getYaw(), pitch);
    }

    /**
     * Reports whether the zoom is locked and cannot be changed.
     */
    public static boolean isZoomLocked() {
        if (Environment.isOSRS()) {
            Varbit varbit = Varbits.load(6357);
            return varbit != null && varbit.getValue() == 1;
        } else {
            Varbit varbit = Varbits.load(19926);
            return varbit != null && varbit.getValue() == 1;
        }
    }

    public static boolean isTurning() {
        FutureTask<Boolean> last = lastTaskMap.get(Environment.getBot());
        return last != null && !last.isDone();
    }

    private static String getEnteredChatText() {
        for (InterfaceComponent ic : Interfaces.newQuery()
            .containers(137, 228, 464, 1467, 1470, 1471, 1529)
            .grandchildren(false).types(InterfaceComponent.Type.LABEL).visible().heights(17)
            .actions("Enter message")
            .results()) {
            if (!NORMAL_CHAT_TEXT_COLOR.equals(ic.getTextColor())) {
                return ic.getText();
            }
        }
        return null;
    }

    private static int getAngleYawRelativeTo(Locatable center, Locatable target) {
        return CommonMath.getAngleOf(center, target);
    }

    private static double getPitchRelativeTo(Locatable center, Locatable target) {
        //Max I've seen is .849 (seems common
        return Math.max(.1, -0.0179 * Distance.between(center, target) + 0.688);
    }

    @InternalAPI
    public static boolean isSceneTileDrawn(int floor, int tileX, int tileZ, Map<String, Object> cache) {
        if (Environment.getClientType() == ClientType.OFFICIAL) {
            return isSceneTileDrawnVanilla(floor, tileX, tileZ);
        }

        return isSceneTileDrawnRuneLite(floor, tileX, tileZ, cache);
    }

    public static boolean isSceneTileDrawn(int floor, int tileX, int tileZ) {
        return isSceneTileDrawn(floor, tileX, tileZ, new HashMap<>());
    }

    private static boolean isSceneTileDrawnVanilla(int floor, int tileX, int tileZ) {
        int cameraTileX = getCameraTileX();
        int cameraTileZ = getCameraTileY();

        int drawDistance = getDrawDistance(false);
        int minTileX = Math.max(0, cameraTileX - drawDistance);
        int minTileZ = Math.max(0, cameraTileZ - drawDistance);
        int maxTileX = Math.min(103, cameraTileX + drawDistance);
        int maxTileZ = Math.min(103, cameraTileZ + drawDistance);

        if (tileX >= minTileX && tileX < maxTileX && tileZ >= minTileZ && tileZ < maxTileZ) {
            //TODO temporarily return true while we fix the rendering logic
//            int[][] heightmap = getHeightmap(floor);
//            return heightmap[tileX][tileZ] - getY() >= 2000;
            return true;
        }
        return false;
    }

    private static boolean isSceneTileDrawnRuneLite(int floor, int tileX, int tileZ, Map<String, Object> cache) {
        Coordinate base = (Coordinate) cache.computeIfAbsent("region_base", t -> Scene.getBase());
        if (base.getPlane() != floor) {
            return false;
        }

        boolean gpu = (boolean) cache.computeIfAbsent("RuneLite.gpu", t -> isGpu());
        int offset = getDrawDistance(gpu);
        int cameraTileX = (int) cache.computeIfAbsent("Camera.cameraTileX", t -> OpenCamera.getLegacyCameraX() >> 7);
        int cameraTileZ = (int) cache.computeIfAbsent("Camera.cameraTileY", t -> OpenCamera.getLegacyCameraY() >> 7); //intentional inversion

//        tileX += 40;
//        tileZ += 40;

        int minTileX = cameraTileX - offset;
        if (minTileX < 0) {
            minTileX = 0;
        }

        int minTileZ = cameraTileZ - offset;
        if (minTileZ < 0) {
            minTileZ = 0;
        }

        int maxTileX = cameraTileX + offset;
        int maxTileZ = cameraTileZ + offset;

        return tileX >= minTileX && tileX < maxTileX && tileZ >= minTileZ && tileZ < maxTileZ;
    }

    private static boolean isGpu() {
        List<String> plugins = Environment.getActiveRuneLitePlugins();
        return plugins.contains("GPU") || plugins.contains("117 HD");
    }

    private static int getDrawDistance(boolean gpu) {
        return gpu ? OpenRegion.getDrawDistanceRuneLite() : OpenWorldView.getTopLevelWorldView().getScene().getDrawDistance();
    }

    public static int getSceneOffset() {
        return Environment.getClientType() == ClientType.OFFICIAL ? 25 : 40;
    }

    private static int[][] getHeightmap(int floor) {
        return Scene.getTileHeights()[floor];
    }

    private static int getCameraTileX() {
        return OpenWorldView.getTopLevelWorldView().getScene().getCameraTileX();
    }

    private static int getCameraTileY() {
        return OpenWorldView.getTopLevelWorldView().getScene().getCameraTileY();
    }
}