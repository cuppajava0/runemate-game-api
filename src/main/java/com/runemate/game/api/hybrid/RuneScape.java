package com.runemate.game.api.hybrid;

import com.runemate.client.game.open.*;
import com.runemate.game.api.osrs.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;

public final class RuneScape {

    private RuneScape() {
    }

    /**
     * Gets the current fps of the game's canvas
     */

    public static int getFps() {
        return OpenClient.getFps();
    }

    /**
     * The amount of cycles since the applet began executing. Each cycle is 20ms in duration.
     */

    public static int getCurrentCycle() {
        return OpenClient.getGlobalCycle();
    }

    /**
     * Gets the current game version (such as 790.1)
     *
     * @return The current game version as a double
     */

    public static double getVersion() {
        return OpenClient.getVersion();
    }

    /**
     * Gets whether or not you are logged into the game (this includes when the map is loading)
     */
    public static boolean isLoggedIn() {
        return OSRSRunescape.isLoggedIn();
    }

    /**
     * Gets whether or not the region is loaded on OSRS
     */
    public static boolean isMapLoaded() {
        return isState(EngineState.SESSION_LOADED);
    }

    /**
     * Gets whether or not a new region is loading on OSRS
     */
    public static boolean isMapLoading() {
        return isState(EngineState.LOADING_REGION);
    }

    public static boolean isLoggingIn() {
        return isState(EngineState.SESSION_CREATION);
    }

    public static boolean isReconnecting() {
        return isState(EngineState.SESSION_RECONNECTING);
    }

    public static boolean isSwitchingWorlds() {
        return isState(EngineState.SESSION_SWITCHING_SERVER);
    }

    public static boolean isStarting() {
        return isState(EngineState.INITIALIZING);
    }

    public static boolean hasCrashed() {
        return isState(EngineState.CRASHED);
    }

    public static EngineState getEngineState() {
        return EngineState.of(OpenClient.getEngineState());
    }

    private static boolean isState(EngineState engineState) {
        return Objects.equals(engineState, getEngineState());
    }

    public static boolean isCutscenePlaying() {
        return OpenCamera.isLocked();
    }

    /**
     * Logs the player completely out of the game.
     *
     * @return true if logged out
     */
    public static boolean logout() {
        return logout(false);
    }

    /**
     * Logs the player out of the game. Lobby can be specified but it's for RS3 only.
     *
     * @param lobby whether to go to the lobby on RS3, otherwise it's ignored.
     * @return true if logged out
     */
    public static boolean logout(final boolean lobby) {
        return OSRSRunescape.logout();
    }

    public static int getLoginState() {
        return OSRSRunescape.getLoginState();
    }

    public static boolean isDisconnected() {
        return getLoginState() == LoginState.DISCONNECTED;
    }

    /**
     * Gets the current server tick according to the game client.
     */
    public static int getServerTick() {
        return OpenClient.getServerTick();
    }

    /**
     * Known Engine States.
     */
    public enum EngineState {
        UNKNOWN(-1),
        UNINITIALIZED(0),
        INITIALIZING(5), //Loading client
        SESSION_SETUP(10), //Title
        AWAITING_TOTP(11), //Authenticator
        SESSION_CREATION(20), //Logging in
        LOADING_REGION(25), //Loading region
        SESSION_LOADED(30), //Logged in
        SESSION_RECONNECTING(40),
        SESSION_SWITCHING_SERVER(45), //Switching worlds
        CRASHED(1000);

        private final int value;

        EngineState(int value) {
            this.value = value;
        }

        /**
         * Returns an EngineState corresponding to the id, or null if id does not match any.
         *
         * @return EngineState corresponding to the identifier `id`
         */
        public static EngineState of(int id) {
            return Arrays.stream(EngineState.values())
                .filter(i -> i.getValue() == id)
                .findFirst()
                .orElse(null);
        }

        public int getValue() {
            return value;
        }
    }

    public interface LoginState {
        int MAIN_MENU = 0;
        int BETA_WORLD = 1;
        int ENTER_CREDENTIALS = 2;
        int INVALID_CREDENTIALS = 3;
        int AUTHENTICATOR = 4;
        int DISABLED = 14;
        int DISCONNECTED = 24;
        int TOO_MANY_ATTEMPTS = 33;
        int NEED_MEMBERS_ACCOUNT = 34;
    }
}
