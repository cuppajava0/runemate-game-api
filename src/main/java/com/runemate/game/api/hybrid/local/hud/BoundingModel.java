package com.runemate.game.api.hybrid.local.hud;

import com.google.common.hash.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.game.api.osrs.projection.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.stream.*;

/**
 * A 3d cube-shaped model that can be used for accurate backup models.
 */
public final class BoundingModel extends Model {
    public static final Pair<int[], int[]> HUMANOID =
        new Pair<>(new int[] { -35, -196, -20 }, new int[] { 35, 6, 13 });
    private final Pair<int[], int[]> offsets;

    public BoundingModel(final LocatableEntity entity, final Pair<int[], int[]> values) {
        this(entity, values.getLeft(), values.getRight());
    }

    public BoundingModel(
        final LocatableEntity entity, final int[] frontBottomLeft,
        final int[] backTopRight
    ) {
        this(entity, DEFAULT_HEIGHT_OFFSET, frontBottomLeft, backTopRight);
    }

    public BoundingModel(
        final LocatableEntity entity, final int heightOffset,
        final int[] frontBottomLeft, final int[] backTopRight
    ) {
        super(entity, heightOffset);
        if (frontBottomLeft.length != 3 || backTopRight.length != 3) {
            throw new IllegalArgumentException(
                "frontBottomLeft and backTopRight need to be size 3");
        }
        this.offsets = new Pair<>(frontBottomLeft, backTopRight);
    }

    public Pair<int[], int[]> getSourceValues() {
        return offsets;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public List<Triangle> projectTriangles() {
        return projectTrianglesWithin(Projection.getViewport());
    }

    @Override
    public List<Triangle> projectTrianglesWithin(Shape viewport) {
        final int[] frontBottomLeftOffsets = this.offsets.getLeft().clone();
        final int[] backTopRightOffsets = this.offsets.getRight().clone();
        int shift = 16;
        int storage = getHighPrecisionOrientation();
        if (storage > 0 && storage < OSRSProjection.SINE.length) {
            final int sin = OSRSProjection.SINE[storage];
            final int cos = OSRSProjection.COSINE[storage];
            storage = frontBottomLeftOffsets[2] * sin + frontBottomLeftOffsets[0] * cos >> shift;
            frontBottomLeftOffsets[2] =
                frontBottomLeftOffsets[2] * cos - frontBottomLeftOffsets[0] * sin >> shift;
            frontBottomLeftOffsets[0] = storage;
            storage = backTopRightOffsets[2] * sin + backTopRightOffsets[0] * cos >> shift;
            backTopRightOffsets[2] =
                backTopRightOffsets[2] * cos - backTopRightOffsets[0] * sin >> shift;
            backTopRightOffsets[0] = storage;
        }
        shift = 7;
        final Coordinate.HighPrecision highPrecision = getHighPrecisionPosition();
        final Coordinate base = Scene.getBase();
        List<Triangle> triangles = new ArrayList<>(10);
        if (highPrecision != null && base != null) {
            int x = highPrecision.getX() - (base.getX() << shift);
            int y = highPrecision.getY() - (base.getY() << shift);
            int height = floorHeight;
            Map<String, Object> cache = new HashMap<>();
            //We already have it, so may as well put it in the cache here.
            cache.put("RegionBase", base);
            Point[] points = Projection.multiAbsoluteToScreen(height,
                new int[] {
                    x + frontBottomLeftOffsets[0],
                    x + backTopRightOffsets[0],
                    x + frontBottomLeftOffsets[0],
                    x + backTopRightOffsets[0],
                    x + backTopRightOffsets[0],
                    x + backTopRightOffsets[0],
                    x + frontBottomLeftOffsets[0],
                    x + frontBottomLeftOffsets[0]
                }, new int[] {
                    frontBottomLeftOffsets[1],
                    frontBottomLeftOffsets[1],
                    backTopRightOffsets[1],
                    backTopRightOffsets[1],
                    backTopRightOffsets[1],
                    frontBottomLeftOffsets[1],
                    backTopRightOffsets[1],
                    frontBottomLeftOffsets[1],
                }, new int[] {
                    y + frontBottomLeftOffsets[2],
                    y + frontBottomLeftOffsets[2],
                    y + frontBottomLeftOffsets[2],
                    y + frontBottomLeftOffsets[2],
                    y + backTopRightOffsets[2],
                    y + backTopRightOffsets[2],
                    y + backTopRightOffsets[2],
                    y + backTopRightOffsets[2]
                }, cache
            );
            if (points == null) {
                return Collections.emptyList();
            }
            final Point frontBottomLeft = points[0];
            final Point frontBottomRight = points[1];
            final Point frontTopLeft = points[2];
            final Point frontTopRight = points[3];
            final Point backTopRight = points[4];
            final Point backBottomRight = points[5];
            final Point backTopLeft = points[6];
            final Point backBottomLeft = points[7];
            if (frontBottomLeft != null && frontBottomRight != null && frontTopLeft != null) {
                triangles.add(new Triangle(frontBottomLeft, frontBottomRight, frontTopLeft));
            }
            if (frontTopLeft != null && frontTopRight != null && frontBottomRight != null) {
                triangles.add(new Triangle(frontTopLeft, frontTopRight, frontBottomRight));
            }
            if (frontTopRight != null && frontBottomRight != null && backTopRight != null) {
                triangles.add(new Triangle(frontTopRight, frontBottomRight, backTopRight));
            }
            if (backBottomRight != null && backTopRight != null && frontBottomRight != null) {
                triangles.add(new Triangle(backBottomRight, backTopRight, frontBottomRight));
            }
            if (backTopLeft != null && backTopRight != null && backBottomRight != null) {
                triangles.add(new Triangle(backTopLeft, backTopRight, backBottomRight));
            }
            if (backBottomLeft != null && backTopLeft != null && backBottomRight != null) {
                triangles.add(new Triangle(backBottomLeft, backTopLeft, backBottomRight));
            }
            if (frontBottomLeft != null && backBottomLeft != null && backTopLeft != null) {
                triangles.add(new Triangle(frontBottomLeft, backBottomLeft, backTopLeft));
            }
            if (frontTopLeft != null && frontBottomLeft != null && backTopLeft != null) {
                triangles.add(new Triangle(frontTopLeft, frontBottomLeft, backTopLeft));
            }
            if (frontTopLeft != null && frontTopRight != null && backTopLeft != null) {
                triangles.add(new Triangle(frontTopLeft, frontTopRight, backTopLeft));
            }
            if (backTopLeft != null && backTopRight != null && frontTopRight != null) {
                triangles.add(new Triangle(backTopLeft, backTopRight, frontTopRight));
            }
        }
        if (viewport != null) {
            List<Triangle> ts = triangles;
            triangles = Environment.getBot().getPlatform()
                .submit(() -> ts.stream().filter(triangle -> viewport.contains(triangle.getA())
                        && viewport.contains(triangle.getB())
                        && viewport.contains(triangle.getC())).collect(Collectors.toList()))
                .join();
        }
        return triangles;
    }

    @Override
    protected int getTriangleCount() {
        return 10;
    }

    @Override
    public int getHeight() {
        return offsets.getRight()[1] - offsets.getLeft()[1];
    }

    @Override
    public BoundingModel getBoundingModel() {
        return this;
    }

    @Override
    public int hashCode() {
        final Hasher hasher = Hashing.md5().newHasher();
        for (final int a : offsets.getLeft()) {
            hasher.putInt(a);
        }
        for (final int b : offsets.getRight()) {
            hasher.putInt(b);
        }
        return hasher.hash().asInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BoundingModel)) {
            return false;
        }
        BoundingModel that = (BoundingModel) o;
        return Arrays.equals(offsets.getLeft(), that.offsets.getLeft()) &&
            Arrays.equals(offsets.getRight(), that.offsets.getRight());
    }

    @Override
    public String toString() {
        if (Objects.equals(offsets, HUMANOID)) {
            return "BoundingModel.HUMANOID";
        }
        return "BoundingModel{fbl:" + Arrays.toString(offsets.getLeft()) + ", btr:" +
            Arrays.toString(offsets.getRight()) + '}';
    }

    @Override
    public Set<Color> getDefaultColors() {
        return Collections.emptySet();
    }
}
