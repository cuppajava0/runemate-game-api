package com.runemate.game.api.hybrid.region;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.function.*;

public final class Banks {
    private static final Predicate<Npc> BANKER_FILTER = Npcs.getActionPredicate("Bank");
    private static final Predicate<GameObject> BANK_CHEST_FILTER = GameObjects.getNamePredicate("Bank chest", "Shantay chest", "Bank Chest-wreck");
    private static final Predicate<GameObject> BANK_BOOTH_FILTER = GameObjects.getActionPredicate("Bank");

    private static final Coordinate[] OSRS_BANK_BLACKLIST = {
        new Coordinate(2097, 3920, 0),
        new Coordinate(2097, 3921, 0),
        new Coordinate(2098, 3920, 0),
        new Coordinate(2098, 3921, 0),
        new Coordinate(2726, 3496, 0),
        new Coordinate(2729, 3496, 0),
        new Coordinate(3088, 3242, 0),
        new Coordinate(3090, 3245, 0), // Draynor banker
        new Coordinate(3096, 3492, 0),
        new Coordinate(3147, 3448, 0),
        new Coordinate(3148, 3448, 0),
        new Coordinate(3148, 3449, 0),
        new Coordinate(3180, 3433, 0),
        new Coordinate(3187, 3446, 0),
        new Coordinate(3191, 3445, 0),
    };

    private Banks() {
    }

    public static BankQueryBuilder newQuery() {
        return new BankQueryBuilder();
    }

    /**
     * Gets all loaded bankers, bank booths, and bank chests (deposit boxes are not included)
     */
    public static LocatableEntityQueryResults<LocatableEntity> getLoaded() {
        return getLoaded(null);
    }

    public static LocatableEntityQueryResults<LocatableEntity> getLoaded(final Predicate<? super LocatableEntity> filter) {
        final List<LocatableEntity> banks = new ArrayList<>(getLoadedBankBooths());
        banks.addAll(getLoadedBankChests());
        banks.addAll(getLoadedBankers());
        if (filter != null) {
            banks.removeIf(filter.negate());
        }
        return new LocatableEntityQueryResults<>(banks);
    }

    public static LocatableEntityQueryResults<LocatableEntity> getLoadedOn(Coordinate... coordinates) {
        final List<LocatableEntity> banks = new ArrayList<>(getLoadedBankBoothsOn(coordinates));
        banks.addAll(getLoadedBankChestsOn(coordinates));
        banks.addAll(getLoadedBankersOn(coordinates));
        return new LocatableEntityQueryResults<>(banks);
    }

    public static LocatableEntityQueryResults<LocatableEntity> getLoadedWithin(Area area) {
        final List<LocatableEntity> banks = new ArrayList<>(getLoadedBankBoothsWithin(area));
        banks.addAll(getLoadedBankChestsWithin(area));
        banks.addAll(getLoadedBankersWithin(area));
        return new LocatableEntityQueryResults<>(banks);
    }

    private static LocatableEntityQueryBuilder<Npc, NpcQueryBuilder> bankersQueryBuilder() {
        return Npcs.newQuery().actions("Bank").off(OSRS_BANK_BLACKLIST);
    }

    private static LocatableEntityQueryBuilder<GameObject, GameObjectQueryBuilder> bankChestsQueryBuilder() {
        return GameObjects.newQuery()
            .types(GameObject.Type.PRIMARY)
            .names("Bank chest", "Bank Chest", "Shantay chest", "Bank boat", "Bank Chest-wreck").actions("Use", "Open")
            .off(OSRS_BANK_BLACKLIST);
    }

    private static LocatableEntityQueryBuilder<GameObject, GameObjectQueryBuilder> bankBoothsQueryBuilder() {
        return GameObjects.newQuery().types(GameObject.Type.PRIMARY, GameObject.Type.BOUNDARY).actions("Bank").off(OSRS_BANK_BLACKLIST);
    }

    public static LocatableEntityQueryResults<Npc> getLoadedBankers() {
        return bankersQueryBuilder().results();
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedBankBooths() {
        return bankBoothsQueryBuilder().results();
    }

    public static LocatableEntityQueryResults<GameObject> getLoadedBankChests() {
        return bankChestsQueryBuilder().results();
    }

    private static LocatableEntityQueryResults<Npc> getLoadedBankersOn(Coordinate... positions) {
        return bankersQueryBuilder().on(positions).results();
    }

    private static LocatableEntityQueryResults<GameObject> getLoadedBankBoothsOn(Coordinate... positions) {
        return bankBoothsQueryBuilder().on(positions).results();
    }

    private static LocatableEntityQueryResults<GameObject> getLoadedBankChestsOn(Coordinate... positions) {
        return bankChestsQueryBuilder().on(positions).results();
    }

    private static LocatableEntityQueryResults<Npc> getLoadedBankersWithin(Area area) {
        return bankersQueryBuilder().within(area).results();
    }

    private static LocatableEntityQueryResults<GameObject> getLoadedBankBoothsWithin(Area area) {
        return bankBoothsQueryBuilder().within(area).results();
    }

    private static LocatableEntityQueryResults<GameObject> getLoadedBankChestsWithin(Area area) {
        return bankChestsQueryBuilder().within(area).results();
    }

    @Deprecated
    public static Predicate<Npc> getBankerPredicate() {
        return BANKER_FILTER;
    }

    @Deprecated
    public static Predicate<GameObject> getBankBoothPredicate() {
        return BANK_BOOTH_FILTER;
    }

    @Deprecated
    public static Predicate<GameObject> getBankChestPredicate() {
        return BANK_CHEST_FILTER;
    }
}
