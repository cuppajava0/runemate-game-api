package com.runemate.game.api.hybrid.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.rmi.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javafx.scene.canvas.*;

public abstract class Entity implements Modeled, LocatableEntity {
    public final long uid;
    protected Model backupModel, forcedModel;
    private boolean releaseUidOnGarbageCollection = true;

    public Entity(long uid) {
        this.uid = uid;
    }

    @Override
    public void setBackupModel(int[] frontBottomLeft, int[] backTopRight) {
        this.backupModel = new BoundingModel(this, frontBottomLeft, backTopRight);
    }

    @Override
    public void setBackupModel(final Pair<int[], int[]> values) {
        setBackupModel(values.getLeft(), values.getRight());
    }

    @Override
    public void setBackupModel(Model backup) {
        this.backupModel = backup;
    }

    @Override
    public void setForcedModel(int[] frontBottomLeft, int[] backTopRight) {
        this.forcedModel = new BoundingModel(this, frontBottomLeft, backTopRight);
    }

    @Override
    public void setForcedModel(Pair<int[], int[]> values) {
        setForcedModel(values.getLeft(), values.getRight());
    }

    @Override
    public void setForcedModel(Model forced) {
        this.forcedModel = forced;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public void render(Graphics2D g2d) {
        Renderable renderable = getModel();
        if (renderable != null || (renderable = getArea()) != null) {
            renderable.render(g2d);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        Renderable renderable = getModel();
        if (renderable != null) {
            java.awt.geom.Area bounds = new java.awt.geom.Area();
            for (final Triangle triangle : ((Model) renderable).projectTriangles()) {
                bounds.add(new java.awt.geom.Area(triangle.toPolygon()));
            }
            PathIterator iterator = bounds.getPathIterator(null);
            float[] floats = new float[6];
            Point previous = null, current = null;
            Point lineStart = null;
            while (!iterator.isDone()) {
                int type = iterator.currentSegment(floats);
                if (type == PathIterator.SEG_MOVETO) {
                    previous = null;
                    current = lineStart = new Point((int) floats[0], (int) floats[1]);
                } else if (type == PathIterator.SEG_LINETO) {
                    int x = (int) floats[0];
                    int y = (int) floats[1];
                    if (previous == null || previous.x != x || previous.y != y) {
                        previous = current;
                        current = new Point(x, y);
                        if (previous != null) {
                            gc.strokeLine(previous.x, previous.y, current.x, current.y);
                        }
                    }
                } else if (type == PathIterator.SEG_CLOSE) {
                    int x = (int) floats[0];
                    int y = (int) floats[1];
                    if (lineStart != null && (lineStart.x != x || lineStart.y != y)) {
                        gc.strokeLine(lineStart.x, lineStart.y, x, y);
                    }
                }
                iterator.next();
            }
        } else if ((renderable = getArea()) != null) {
            renderable.render(gc);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Entity) {
            if (BridgeUtil.getFieldInstance(uid) == BridgeUtil.getFieldInstance(((Entity) object).uid)) {
                return true;
            }
            //The purpose of this is to allow matching the entity to menu item targets with a much higher accuracy
            if (getClass().equals(object.getClass())) {
                Area.Rectangular area = getArea();
                Area.Rectangular otherArea = ((Entity) object).getArea();
                if (Objects.equals(area, otherArea)) {
                    String name = null;
                    String objectName = null;
                    if (this instanceof Onymous) {
                        name = ((Onymous) this).getName();
                    }
                    if (object instanceof Onymous) {
                        objectName = ((Onymous) object).getName();
                    }
                    return Objects.equals(name, objectName);
                }
            }
        }
        return false;
    }

    public void releaseUidOnGarbageCollection(boolean release) {
        this.releaseUidOnGarbageCollection = release;
    }

    @Override
    public int hashCode() {
        return (int) BridgeUtil.getFieldInstance(uid);
    }

    @Override

    public boolean isValid() {
        return OpenClient.validate(uid);
    }

    /*@Override
    protected final void finalize() throws Throwable {
        super.finalize();
        if (releaseUidOnGarbageCollection) {
            client.rmi().free(uid);
        }
    }*/
}
