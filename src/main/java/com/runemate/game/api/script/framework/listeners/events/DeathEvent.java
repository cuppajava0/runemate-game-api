package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class DeathEvent implements EntityEvent {
    EntityType entityType;
    Locatable source;
    Coordinate position;
    Area.Rectangular area;
    int cycle;

    /**
     * @deprecated use #getEntityType()
     */
    @Deprecated
    public Type getType() {
        return Type.valueOf(entityType.name());
    }

    @Deprecated
    public enum Type {
        NPC,
        PLAYER
    }
}
