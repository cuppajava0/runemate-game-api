package com.runemate.game.api.script.annotations;

import java.lang.annotation.*;

/**
 * An annotation marking a method as only being compatible with OSRS.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface OSRSOnly {
}
