package com.runemate.game.api.script;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.concurrent.*;
import lombok.*;

public final class Execution {

    private Execution() {
    }

    /**
     * Delays the current thread's execution for the specified length of time in milliseconds
     *
     * @param length The length of time to delay in milliseconds
     * @return Whether we slept for the correct amount of time
     */
    public static boolean delay(long length) {
        if (length < 0) {
            throw new IllegalArgumentException("The specified delay length of " + length + " can not be a negative value.");
        }
        if (length == 0) {
            return true;
        }
        try {
            Thread.sleep(length);
            return true;
        } catch (InterruptedException ignored) {
            //Set the interrupted flag which gets cleared by the exception.
            Thread.currentThread().interrupt();
            return false;
        }
    }

    /**
     * Delays the current thread's execution for a length of time given in milliseconds
     * between the minimum and maximum length
     *
     * @param minLength The minimum length in milliseconds (Inclusive)
     * @param maxLength The maximum length in milliseconds (Inclusive)
     */
    public static boolean delay(int minLength, int maxLength) {
        return delay((long) minLength, maxLength);
    }

    /**
     * Delays the current thread's execution for a length of time
     * between the minimum and maximum length, averaging around the averageLength
     *
     * @param minLength     The minimum length in milliseconds (Inclusive)
     * @param maxLength     The maximum length in milliseconds (Inclusive)
     * @param averageLength The average length of time in milliseconds to delay execution
     */
    public static boolean delay(int minLength, int maxLength, int averageLength) {
        return delay((long) minLength, maxLength, averageLength);
    }

    /**
     * Delays the current thread's execution for a length of time given in milliseconds
     * between the minimum and maximum length
     *
     * @param minLength The minimum length in milliseconds (Inclusive)
     * @param maxLength The maximum length in milliseconds (Inclusive)
     */
    public static boolean delay(long minLength, long maxLength) {
        return delay(minLength, maxLength, (minLength + maxLength) >> 1);
    }

    /**
     * Delays the current thread's execution for a length of time
     * between the minimum and maximum length, averaging around the averageLength
     *
     * @param minLength     The minimum length in milliseconds (Inclusive)
     * @param maxLength     The maximum length in milliseconds (Inclusive)
     * @param averageLength The average length of time in milliseconds to delay execution
     */
    public static boolean delay(long minLength, long maxLength, long averageLength) {
        return delay((long) Random.nextGaussian(minLength, maxLength, averageLength));
    }

    /**
     * Delays until condition returns true. Usage of this method is
     * discouraged to the same extent as while loops.
     * The advantage of this over a while loop is that it takes small pauses between calls
     * to reduce cpu load.
     *
     * @param condition
     */
    public static void delayUntil(Callable<Boolean> condition) {
        delayUntil(condition, null, 50, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Delays execution of the current thread until the condition is met (returns true)
     * The current result will be checked every Math.min(100, timeout / 20) milliseconds
     * (frequency) until the condition is met or the timeout is reached
     *
     * @param condition The condition that the thread will wait on
     * @param timeout   The maximum amount of time to delay in milliseconds
     */
    public static boolean delayUntil(Callable<Boolean> condition, int timeout) {
        return delayUntil(condition, null, timeout);
    }

    public static boolean delayUntil(Callable<Boolean> condition, int minTimeout, int maxTimeout) {
        return delayUntil(condition, null, minTimeout, maxTimeout);
    }

    public static boolean delayUntil(Callable<Boolean> condition, Callable<Boolean> reset, int timeout) {
        return delayUntil(condition, reset, timeout, timeout);
    }

    public static boolean delayUntil(Callable<Boolean> condition, Callable<Boolean> reset, int minTimeout, int maxTimeout) {
        return delayUntil(condition, reset, calculateOptimalFrequency(minTimeout, maxTimeout), minTimeout, maxTimeout);
    }

    /**
     * Delays execution of the current thread until the condition is met (returns true)
     * The current result will be checked every X milliseconds (frequency) until the condition
     * is met or the timeout is reached.
     *
     * @param condition  The condition that the thread will wait on
     * @param reset      A condition that will be used to reset the current time to 0
     * @param frequency  The frequency that the condition is checked in milliseconds
     * @param minTimeout The minimum length of time in milliseconds that can pass before
     *                   the method times out
     * @param maxTimeout The maximum length of time in milliseconds that can pass before
     *                   the method times out
     */
    @SneakyThrows(Exception.class)
    public static boolean delayUntil(Callable<Boolean> condition, Callable<Boolean> reset, int frequency, int minTimeout, int maxTimeout) {
        final StopWatch watch = new StopWatch();
        watch.start();
        int timeout = (minTimeout == maxTimeout) ? minTimeout : (int) Random.nextGaussian(minTimeout, maxTimeout);
        while (!condition.call() && watch.getRuntime() < timeout) {
            delay(frequency);
            if (reset == null) {
                continue;
            }
            int state = RuneScape.getEngineState().getValue();
            if (state < RuneScape.EngineState.LOADING_REGION.getValue()) {
                continue;
            }
            if (reset.call()) {
                watch.reset();
            }
        }
        return condition.call();
    }

    public static void delayUntilCycle(int targetEngineCycle) {
        //noinspection StatementWithEmptyBody
        while (RuneScape.getCurrentCycle() < targetEngineCycle) {
        }
    }

    /**
     * Delays while condition returns true. Usage of this method is discouraged to the
     * same extent as while loops.
     * The advantage of this over a while loop is that it takes small pauses
     * between calls to reduce cpu load.
     *
     * @param condition
     */
    public static void delayWhile(Callable<Boolean> condition) {
        delayWhile(condition, null, 50, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public static boolean delayWhile(final Callable<Boolean> condition, final int timeout) {
        return delayWhile(condition, null, timeout);
    }

    public static boolean delayWhile(final Callable<Boolean> condition, final int minTimeout, final int maxTimeout) {
        return delayWhile(condition, null, minTimeout, maxTimeout);
    }

    public static boolean delayWhile(Callable<Boolean> condition, Callable<Boolean> reset, int timeout) {
        return delayWhile(condition, reset, calculateOptimalFrequency(timeout), timeout, timeout);
    }

    public static boolean delayWhile(Callable<Boolean> condition, Callable<Boolean> reset, int minTimeout, int maxTimeout) {
        return delayWhile(condition, reset, calculateOptimalFrequency(minTimeout, maxTimeout), minTimeout, maxTimeout);
    }

    /**
     * Delays execution of the current thread while the condition is met (returns true)
     * The current result will be checked every X milliseconds (frequency) until the
     * condition is met or the timeout is reached.
     *
     * @param condition  The condition that the thread will wait on
     * @param reset      A condition that will be used to reset the current time to 0
     * @param frequency  The frequency that the condition is checked in milliseconds
     * @param minTimeout The minimum length of time in milliseconds that can pass before the
     *                   method times out
     * @param maxTimeout The maximum length of time in milliseconds that can pass before the
     *                   method times out
     */
    @SneakyThrows(Exception.class)
    public static boolean delayWhile(Callable<Boolean> condition, Callable<Boolean> reset, int frequency, int minTimeout, int maxTimeout) {
        final StopWatch watch = new StopWatch();
        watch.start();
        int timeout = (minTimeout == maxTimeout) ? minTimeout : (int) Random.nextGaussian(minTimeout, maxTimeout);
        while (condition.call() && watch.getRuntime() < timeout) {
            delay(frequency);
            if (reset == null) {
                continue;
            }
            int state = RuneScape.getEngineState().getValue();
            if (state < RuneScape.EngineState.LOADING_REGION.getValue()) {
                continue;
            }
            if (reset.call()) {
                watch.reset();
            }
        }
        return !condition.call();
    }

    private static int calculateOptimalFrequency(int minTimeout, int maxTimeout) {
        return calculateOptimalFrequency((minTimeout + maxTimeout) >> 1);
    }

    private static int calculateOptimalFrequency(int timeout) {
        return Math.min(100, timeout / 20);
    }

}
