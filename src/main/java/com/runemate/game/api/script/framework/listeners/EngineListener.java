package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/**
 * A Listener for receiving events related directly to the game's engine.
 * It has methods that are fired whenever a new client side logic cycle begins and whenever a new server side logic tick begins.
 */
public interface EngineListener extends EventListener {

    @Deprecated
    default void onCycleStart() {
    }

    @Deprecated
    default void onTickStart() {
    }

    default void onEngineEvent(EngineEvent event) {
        switch (event.getType()) {
            case SERVER_TICK -> onTickStart();
            case CLIENT_CYCLE -> onCycleStart();
        }
    }

    default void onEngineStateChanged(EngineStateEvent event) {
    }

}
