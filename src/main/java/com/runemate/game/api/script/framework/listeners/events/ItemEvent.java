package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import lombok.*;
import org.jetbrains.annotations.*;

@Value
public class ItemEvent implements Event {

    SpriteItem item;
    int quantityChange;

    @ToString.Exclude
    int inventoryId;

    @Getter(lazy = true)
    @ToString.Exclude
    Type type = resolveType();

    @Getter(lazy = true)
    @Nullable
    Inventories.Documented inventory = Inventories.Documented.resolve(inventoryId);

    private Type resolveType() {
        if (quantityChange > 0) {
            return Type.ADDITION;
        }
        if (quantityChange < 0) {
            return Type.REMOVAL;
        }
        return Type.UNKNOWN;
    }

    public int getQuantityChange() {
        return Math.abs(quantityChange);
    }

    public enum Type {
        ADDITION,
        REMOVAL,
        UNKNOWN
    }
}
