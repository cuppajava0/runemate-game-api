package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.net.*;
import lombok.*;

@Value
public class GrandExchangeEvent implements Event {

    GrandExchange.Slot changedSlot;
    Type type;

    public enum Type {
        OFFER_CREATED,
        OFFER_REMOVED,
        OFFER_STATE_CHANGED,
        OFFER_COMPLETION_CHANGED
    }
}
