package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSGameObject extends OSRSEntity implements GameObject {

    private boolean modelLoaded;
    private Type type;

    private final OpenGameObject object;
    private final Coordinate position;

    public OSRSGameObject(OpenGameObject object, Coordinate position) {
        super(object.getUid());
        this.object = object;
        this.position = position;
    }

    @Override
    public int getAnimationId() {
        return (int) object.getAnimationSequence();
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        var real = super.getModel();
        if (real instanceof RemoteModel) {
            return real;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        if (!modelLoaded) {
            modelLoaded = true;
            CacheObjectDefinition.Extended definition = (CacheObjectDefinition.Extended) getDefinition();
            if (definition != null) {
                final CacheObjectDefinition.Extended local_state = (CacheObjectDefinition.Extended) definition.getLocalState();
                if (local_state != null) {
                    definition = local_state;
                }
                int[][] raw_appearance = definition.getModelIds();
                if (raw_appearance != null && raw_appearance.length == 1) {
                    int[] appearance = raw_appearance[0];
                    if (appearance.length > 0) {
                        List<CacheModel> components = new ArrayList<>();
                        int specializedType = getSpecializedTypeIndicator();
                        int[] modelTypes = definition.getModelTypes();
                        if (modelTypes == null) {
                            if (specializedType == 10 || specializedType == 11) {
                                for (int modelID : appearance) {
                                    components.add(CacheModel.load(modelID));
                                }
                            } else {
                                //For compatibility's sake. Client code simply returns null at this point
                                components.add(CacheModel.load(appearance[0]));
                            }
                        } else {
                            int index = -1;
                            for (int i = 0; i < modelTypes.length; ++i) {
                                if (modelTypes[i] == specializedType) {
                                    index = i;
                                    break;
                                }
                            }
                            if (index != -1 && index < appearance.length) {
                                components.add(CacheModel.load(appearance[index]));
                            } else if (backupModel == null && appearance.length == 1) {
                                components.add(CacheModel.load(appearance[0]));
                            }
                        }

                        //Null-check the models
                        components.removeIf(Objects::isNull);

                        if (!components.isEmpty()) {
                            int heightOffset = object.getHeight();
                            if (heightOffset == -1) {
                                heightOffset = 0;
                            }
                            CompositeCacheModel model = new CompositeCacheModel(this, /*heightOffset*/0, components);
                            model.setScale(definition.getModelXScale(), definition.getModelYScale(), definition.getModelZScale());
                            model.setTranslation(definition.getModelXTranslation(),
                                definition.getModelYTranslation(),
                                definition.getModelZTranslation()
                            );
                            return cacheModel = model;
                        }
                    }
                }
            }
        }
        return backupModel;
    }


    @Override
    public byte getSpecializedTypeIndicator() {
        //TODO It appears placement is always -1 when the object isn't loaded. This could result in a faster isValid method. Analyze the data to confirm.
        return (byte) (object.getPlacement() & 0x3f);
    }

    @Override
    public int getId() {
        return object.getId();
    }

    @Override
    public Type getType() {
        if (type == null) {
            type = switch (object.getType()) {
                case "BoundaryObject" -> Type.BOUNDARY;
                case "EventObject" -> Type.PRIMARY;
                case "FloorObject" -> Type.GROUND_DECORATION;
                case "WallObject" -> Type.WALL_DECORATION;
                default -> {
                    log.warn("Unknown object type {}", object.getType());
                    yield Type.UNKNOWN;
                }
            };
        }
        return type;
    }

    @Override
    public GameObjectDefinition getDefinition() {
        int id = getId();
        if (id < 0) {
            return null;
        }
        return GameObjectDefinition.get(id);
    }


    private long getInnerHash() {
        return object.getHash();
    }

    @Override
    @NonNull
    public Coordinate getPosition(Coordinate regionBase) {
        return position;
    }


    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        if (getType() != Type.UNKNOWN) {
            int hpRegionX = object.getLocalX();
            if (hpRegionX == -1) {
                hpRegionX = 0;
            }
            int hpRegionY = object.getLocalY();
            if (hpRegionY == -1) {
                hpRegionY = 0;
            }
            if (regionBase == null) {
                regionBase = OSRSScene.getBase(0);
            }
            int xInset = 0;
            int yInset = 0;
            byte type = getSpecializedTypeIndicator();
            if (type == 4) {
                //A basic wall object, no insets
            } else if (type == 5) {
                int inset = 16;
                int orientation = getDirection().ordinal() & 0x3;
                xInset = (new int[] { 1, 0, -1, 0 }[orientation] * inset);
                yInset = (new int[] { 0, -1, 0, 1 }[orientation] * inset);
            } else if (type == 6) {
                int inset = 8;
                int orientation = getDirection().ordinal() & 0x3;
                xInset = (new int[] { 1, -1, -1, 1 }[orientation] * inset);
                yInset = (new int[] { -1, -1, 1, 1 }[orientation] * inset);
            } else if (type == 7) {
                //Something to do with orientation
            } else if (type == 8) {
                //TODO I give up for now
                /*int inset = 32;
                int orientation = getDirection().ordinal() + 2 & 0x3;
                xInset = (new int[]{1, -1, -1, 1}[orientation] * inset);
                yInset = (new int[]{-1, -1, 1, 1}[orientation] * inset);*/
            }
            final Coordinate.HighPrecision hp = regionBase.getHighPrecisionPosition();
            return new Coordinate.HighPrecision(hp.getX() + hpRegionX + xInset, hp.getY() + hpRegionY + yInset, position.getPlane());
        }
        int x = position.getX() << 7;
        int y = position.getY() << 7;
        final Area.Rectangular area = getArea(regionBase);
        if (area != null) {
            x += (area.getWidth() << 6);
            y += (area.getHeight() << 6);
        }
        return new Coordinate.HighPrecision(x, y, position.getPlane());
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        GameObjectDefinition def = getDefinition();
        if (def != null) {
            GameObjectDefinition transformation = def.getLocalState();
            if (transformation != null) {
                def = transformation;
            }
            int width = def.getWidth();
            int height = def.getHeight();
            /*if (width > 1 || height > 1) {
                byte specializedTypeIndicator = getSpecializedTypeIndicator();
                //only types 10 and 11 appear to be having larger areas? type 4 does in basement of wizards tower on 2 objects but they're not within the area it seems....
                if (specializedTypeIndicator != 10 && specializedTypeIndicator != 11) {
                    log.info("OSRS GameObject of type " + specializedTypeIndicator + " named " + def.getName() + " (" + getId() + ") has a width of " + width + " and height of " + height);
                }
            }*/
            if (width != height) {
                Direction direction = getDirection();
                if (direction == null) {
                    return null;
                }
                width = direction.ordinal() % 2 == 0 ? def.getWidth() : def.getHeight();
                height = direction.ordinal() % 2 == 0 ? def.getHeight() : def.getWidth();
            }
            final Coordinate root = getPosition(regionBase);
            return new Area.Rectangular(root, root.derive(width - 1, height - 1));
        }
        return super.getArea(regionBase);
    }

    @Override
    public String toString() {
        String name = null;
        GameObjectDefinition definition = getDefinition();
        if (definition != null) {
            name = definition.getName();
            if ("null".equals(name)) {
                name = null;
            }
            if (name == null) {
                definition = definition.getLocalState();
                if (definition != null) {
                    name = definition.getName();
                    if ("null".equals(name)) {
                        name = null;
                    }
                }
            }
        }
        final Coordinate position = getPosition();
        return (name != null ? name : "#" + getId()) + " [" + (
            position != null ? position.getX() + ", " + position.getY() + ", " + position.getPlane() : ""
        ) + ']';
    }

    @Override
    public int getHighPrecisionOrientation() {
        int orientationInset = 0;
        byte type = getSpecializedTypeIndicator();
        Direction direction = getDirection();
        if (type == 8) {
            //TODO I give up for now
            //orientationInset = -256 * 3;
        } else if (type == 11) {
            orientationInset = 256;
        }

        return direction != null ? direction.getHighPrecisionOrientation() + orientationInset : 0;
    }

    @Override
    public Direction getDirection() {
        //placement is -1 when the object isn't loaded it appears!
        int placement = object.getPlacement();
        int cardinalDirection = placement >> 6;
        if (cardinalDirection < 0 || cardinalDirection >= Direction.values().length) {
            return null;
        }
        return Direction.values()[cardinalDirection];
    }

    @Override
    public int getOrientationAsAngle() {
        Direction direction = getDirection();
        return direction != null ? direction.getAngle() : 0;
    }

    @Override
    public Direction getPrimaryOrientation() {
        return switch (object.getPrimaryOrientation()) {
              case 1 -> Direction.WEST;
              case 2 -> Direction.NORTH;
              case 4 -> Direction.EAST;
              case 8 -> Direction.SOUTH;
              case 16 -> Direction.NORTH_WEST;
              case 32 -> Direction.NORTH_EAST;
              case 64 -> Direction.SOUTH_EAST;
              case 128 -> Direction.SOUTH_WEST;
              default -> null;
        };
    }

    @Override
    public Direction getSecondaryOrientation() {
        return switch (object.getSecondaryOrientation()) {
            case 1 -> Direction.WEST;
            case 2 -> Direction.NORTH;
            case 4 -> Direction.EAST;
            case 8 -> Direction.SOUTH;
            case 16 -> Direction.NORTH_WEST;
            case 32 -> Direction.NORTH_EAST;
            case 64 -> Direction.SOUTH_EAST;
            case 128 -> Direction.SOUTH_WEST;
            default -> null;
        };
    }

    @Override
    public boolean isValid() {
        if (super.isValid()) {
            Area.Rectangular area = getArea();
            if (area != null && area.isLoaded()) {
                return !GameObjects.getLoadedWithin(area, getId()).isEmpty();
            }
        }
        return false;
    }
}
