package com.runemate.game.api.osrs.local.hud;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.structures.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import com.runemate.game.api.osrs.projection.*;
import com.google.common.hash.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import lombok.*;

public final class OSRSModel extends RSModel {

    private final int[] xVertices, yVertices, zVertices, aIndices, bIndices, cIndices;
    private BoundingModel cached_bounding_model;

    public OSRSModel(long uid, final int heightOffset, final LocatableEntity entity, final int[][] points) {
        super(uid, entity, heightOffset);
        aIndices = points[0];
        bIndices = points[1];
        cIndices = points[2];
        xVertices = points[3];
        yVertices = points[4];
        zVertices = points[5];
    }


    public OSRSModel(long uid, final LocatableEntity entity, final int[][] points) {
        this(uid, DEFAULT_HEIGHT_OFFSET, entity, points);
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @NonNull
    @Override
    public List<Triangle> projectTriangles() {
        return projectTrianglesWithin(Projection.getViewport());
    }

    @NonNull
    @Override
    public List<Triangle> projectTrianglesWithin(Shape viewport) {
        final int[] xVertices = this.xVertices.clone();
        final int[] yVertices = this.yVertices.clone();
        final int[] zVertices = this.zVertices.clone();
        int storage = getHighPrecisionOrientation();
        if (storage > 0 && storage < OSRSProjection.SINE.length) {
            final int orientationSin = OSRSProjection.SINE[storage];
            final int orientationCos = OSRSProjection.COSINE[storage];
            for (int index = 0; index < xVertices.length; ++index) {
                storage =
                    zVertices[index] * orientationSin + xVertices[index] * orientationCos >> 16;
                zVertices[index] =
                    zVertices[index] * orientationCos - xVertices[index] * orientationSin >> 16;
                xVertices[index] = storage;
            }
        }
        final Coordinate region_base = Scene.getBase();
        final Coordinate.HighPrecision region_base_hp = region_base.getHighPrecisionPosition();
        final Coordinate.HighPrecision position = getHighPrecisionPosition(region_base);
        if (position != null) {
            ArrayList<Triangle> polygons = new ArrayList<>(aIndices.length);
            int x = position.getX() - region_base_hp.getX();
            int y = position.getY() - region_base_hp.getY();
            Map<String, Object> cache = new HashMap<>();
            //Cache here because we already have it
            cache.put("RegionBase", region_base);
            int verticeLengths = Math.min(zVertices.length, Math.min(xVertices.length, yVertices.length));
            for (int index = 0; index < aIndices.length; ++index) {
                int aIndex = aIndices[index];
                int bIndex = bIndices[index];
                int cIndex = cIndices[index];
                if (aIndex < verticeLengths && bIndex < verticeLengths && cIndex < verticeLengths) {
                    Point[] points = Projection.multiAbsoluteToScreen(floorHeight,
                        new int[] {
                            x + xVertices[aIndex],
                            x + xVertices[bIndex],
                            x + xVertices[cIndex]
                        },
                        new int[] {
                            yVertices[aIndex],
                            yVertices[bIndex],
                            yVertices[cIndex]
                        },
                        new int[] {
                            y + zVertices[aIndex],
                            y + zVertices[bIndex],
                            y + zVertices[cIndex]
                        }, cache
                    );
                    if (points == null) {
                        continue;
                    }
                    Point a = points[0];
                    Point b = points[1];
                    Point c = points[2];
                    if (a != null && b != null && c != null && (viewport == null || viewport.contains(a) && viewport.contains(b) && viewport.contains(c))) {
                        polygons.add(new Triangle(a, b, c));
                    }
                }
            }
            polygons.trimToSize();
            return polygons;
        }
        return Collections.emptyList();
    }

    @Override
    protected int getTriangleCount() {
        return aIndices.length;
    }

    @Override
    public int getHeight() {
        int minY = Integer.MAX_VALUE, maxY = Integer.MIN_VALUE;
        for (int index = 0; index < aIndices.length; ++index) {
            if (yVertices[aIndices[index]] < minY) {
                minY = yVertices[aIndices[index]];
            }
            if (yVertices[bIndices[index]] < minY) {
                minY = yVertices[bIndices[index]];
            }
            if (yVertices[cIndices[index]] < minY) {
                minY = yVertices[cIndices[index]];
            }
            if (yVertices[aIndices[index]] > maxY) {
                maxY = yVertices[aIndices[index]];
            }
            if (yVertices[bIndices[index]] > maxY) {
                maxY = yVertices[bIndices[index]];
            }
            if (yVertices[cIndices[index]] > maxY) {
                maxY = yVertices[cIndices[index]];
            }
        }
        return maxY - minY;
    }

    @Override
    public BoundingModel getBoundingModel() {
        if (cached_bounding_model != null) {
            return cached_bounding_model;
        }
        int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, minZ = Integer.MAX_VALUE,
            maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE, maxZ = Integer.MIN_VALUE;
        for (int index = 0; index < aIndices.length; ++index) {
            if (xVertices[aIndices[index]] < minX) {
                minX = xVertices[aIndices[index]];
            }
            if (xVertices[bIndices[index]] < minX) {
                minX = xVertices[bIndices[index]];
            }
            if (xVertices[cIndices[index]] < minX) {
                minX = xVertices[cIndices[index]];
            }
            if (xVertices[aIndices[index]] > maxX) {
                maxX = xVertices[aIndices[index]];
            }
            if (xVertices[bIndices[index]] > maxX) {
                maxX = xVertices[bIndices[index]];
            }
            if (xVertices[cIndices[index]] > maxX) {
                maxX = xVertices[cIndices[index]];
            }

            if (yVertices[aIndices[index]] < minY) {
                minY = yVertices[aIndices[index]];
            }
            if (yVertices[bIndices[index]] < minY) {
                minY = yVertices[bIndices[index]];
            }
            if (yVertices[cIndices[index]] < minY) {
                minY = yVertices[cIndices[index]];
            }
            if (yVertices[aIndices[index]] > maxY) {
                maxY = yVertices[aIndices[index]];
            }
            if (yVertices[bIndices[index]] > maxY) {
                maxY = yVertices[bIndices[index]];
            }
            if (yVertices[cIndices[index]] > maxY) {
                maxY = yVertices[cIndices[index]];
            }

            if (zVertices[aIndices[index]] < minZ) {
                minZ = zVertices[aIndices[index]];
            }
            if (zVertices[bIndices[index]] < minZ) {
                minZ = zVertices[bIndices[index]];
            }
            if (zVertices[cIndices[index]] < minZ) {
                minZ = zVertices[cIndices[index]];
            }
            if (zVertices[aIndices[index]] > maxZ) {
                maxZ = zVertices[aIndices[index]];
            }
            if (zVertices[bIndices[index]] > maxZ) {
                maxZ = zVertices[bIndices[index]];
            }
            if (zVertices[cIndices[index]] > maxZ) {
                maxZ = zVertices[cIndices[index]];
            }
        }
        return cached_bounding_model =
            new BoundingModel(owner, new int[] { minX, minY, minZ }, new int[] { maxX, maxY, maxZ });
    }

    @Override
    public int hashCode() {
        if (xVertices == null || aIndices == null) {
            return -1;
        }
        final Hasher hasher = Hashing.md5().newHasher();
        for (final int a : aIndices) {
            hasher.putInt(a);
        }
        for (final int b : bIndices) {
            hasher.putInt(b);
        }
        for (final int c : cIndices) {
            hasher.putInt(c);
        }
        for (final int x : xVertices) {
            hasher.putInt(x);
        }
        for (final int y : yVertices) {
            hasher.putInt(y);
        }
        for (final int z : zVertices) {
            hasher.putInt(z);
        }
        return hasher.hash().asInt();
    }

    @Override
    public Set<Color> getDefaultColors() {
        return Collections.emptySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        OSRSModel osrsModel = (OSRSModel) o;

        if (!Arrays.equals(xVertices, osrsModel.xVertices)) {
            return false;
        }
        if (!Arrays.equals(yVertices, osrsModel.yVertices)) {
            return false;
        }
        if (!Arrays.equals(zVertices, osrsModel.zVertices)) {
            return false;
        }
        if (!Arrays.equals(aIndices, osrsModel.aIndices)) {
            return false;
        }
        if (!Arrays.equals(bIndices, osrsModel.bIndices)) {
            return false;
        }
        return Arrays.equals(cIndices, osrsModel.cIndices);
    }
}
