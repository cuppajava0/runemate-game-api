package com.runemate.game.api.osrs.local.sound;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.sound.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;

public class OSRSEmittedSoundEffect implements EmittedSoundEffect {
    private final long uid;

    private OpenSoundEffect soundEffect;

    public OSRSEmittedSoundEffect(long uid) {
        this.uid = uid;
    }

    private OpenSoundEffect soundEffect() {
        if (soundEffect == null) {
            soundEffect = OpenSoundEffect.create(uid);
        }
        return soundEffect;
    }

    @Override
    public int getAmbientSoundId() {
        return soundEffect().getAmbientSoundEffectId();
    }

    @Override
    public int[] getShufflingSoundIds() {
        return soundEffect().getShufflingSoundIds();
    }

    @Override
    public int getAudibleRadius() {
        return soundEffect().getAudibleRadius();
    }

    @Override
    public GameObjectDefinition getEmittingObject() {
        int id = soundEffect().getObjectId();
        if (id != -1) {
            return GameObjectDefinition.get(id);
        }
        return null;
    }

    @Override
    public int getMinimumCyclesBeforeShuffle() {
        return soundEffect().getMinimumCyclesBeforeShuffle();
    }

    @Override
    public int getMaximumCyclesBeforeShuffle() {
        return soundEffect().getMaximumCyclesBeforeShuffle();
    }

    @Override
    public String toString() {
        return "OSRSEmittedSoundEffect{" +
            "ambientSoundId=" + getAmbientSoundId() +
            ", ambientSoundRadius=" + getAudibleRadius() +
            ", shufflingSoundIds=" + Arrays.toString(getShufflingSoundIds()) +
            ", minimumCyclesBeforeShuffle=" + getMinimumCyclesBeforeShuffle() +
            ", maximumCyclesBeforeShuffle=" + getMaximumCyclesBeforeShuffle() +
            ", emittingObject=" + getEmittingObject() +
            '}';
    }
}
