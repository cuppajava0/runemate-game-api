package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.commons.internal.scene.locations.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.*;
import java.util.*;
import lombok.extern.log4j.*;

@Log4j2(topic = "OSRSScene")
@InternalAPI
public final class OSRSScene {
    private OSRSScene() {
    }

    public static void setStoredBase(Coordinate base) {
        final var bot = Environment.getBot();
        if (bot != null) {
            bot.getCache().put("RegionBase", new Coordinate(base.getX(), base.getY(), 0));
            bot.getCache().put("CurrentPlane", base.getPlane());
        }
    }

    public static Coordinate getStoredBase() {
        final var bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        Coordinate base = (Coordinate) bot.getCache().get("RegionBase");
        Integer plane = (Integer) bot.getCache().get("CurrentPlane");
        if (base == null || plane == null) {
            return null;
        }

        return new Coordinate(base.getX(), base.getY(), plane);
    }

    /**
     * Retrieves the cached region base if available, otherwise defers to {@link #getCurrentBase()}
     */
    public static Coordinate getBase() {
        var base = getStoredBase();
        if (base == null) {
            base = getCurrentBase();
            setStoredBase(base);
        }
        return base;
    }

    /**
     * Retrieves the current region base from the game client, rather than the (potentially) cached base from {@link #getBase()}
     */
    public static Coordinate getCurrentBase() {
        OpenWorldView view = OpenWorldView.getTopLevelWorldView();
        return new Coordinate(view.getBaseX(), view.getBaseY(), view.getPlane());
    }

    public static int getCurrentPlane() {
        AbstractBot bot = Environment.getBot();
        Integer plane = (Integer) bot.getCache().get("CurrentPlane");
        if (plane == null) {
            OpenWorldView view = OpenWorldView.getTopLevelWorldView();
            plane = view.getPlane();
            bot.getCache().put("CurrentPlane", plane);
        }
        return plane;
    }


    public static Coordinate getBase(int plane) {
        final AbstractBot bot = Environment.getBot();
        int xBase;
        int yBase;
        if (bot != null) {
            Coordinate base = (Coordinate) bot.getCache().get("RegionBase");
            if (base != null) {
                xBase = base.getX();
                yBase = base.getY();
            } else {
                base = getCurrentBase();
                xBase = base.getX();
                yBase = base.getY();
                bot.getCache().put("RegionBase", new Coordinate(xBase, yBase, 0));
            }
            return new Coordinate(xBase, yBase, plane);
        }

        Coordinate base = getCurrentBase();
        return new Coordinate(base.getX(), base.getY(), plane);
    }

    public static List<Entity> getHoveredEntities() {
        int hoveredEntityUidCount = OpenClient.getHoveredEntityCount();
        if (hoveredEntityUidCount <= 0) {
            return Collections.emptyList();
        }
        List<Entity> hoveredEntities = new ArrayList<>(hoveredEntityUidCount);
        long[] hoveredEntityUids = OpenClient.getHoveredEntityUids();
        if (hoveredEntityUids == null) {
            return Collections.emptyList();
        }
        long previousHoveredEntityUID = -1;
        for (int index = 0; index < hoveredEntityUidCount; index++) {
            long hoveredEntityUid = hoveredEntityUids[index];
            if (hoveredEntityUid != previousHoveredEntityUID) {
                previousHoveredEntityUID = hoveredEntityUid;
                resolveEntities(hoveredEntityUid, hoveredEntities);
            }
        }
        return hoveredEntities;
    }

    public static void resolveEntities(long uid, List<Entity> hoveredEntities) {
        /*
         * 0 = Player
         * 1 = Npc
         * 2 = SceneObject
         * 3 = GroundItem
         */
        int entityType = (int) (uid >>> 14 & 3L);
        int entityIdentifier = (int) (uid >>> 17 & 4294967295L);
        int regionX = (int) (uid & 127L);
        int regionY = (int) (uid >>> 7 & 127L);
                /*
                The entityIdentifier is _blank_ if the entityType is:

                Player = player[] index
                Npc = npc[] index
                SceneObject = id
                GroundItem = ?
                */
        switch (entityType) {
            case 0 -> {
                OpenPlayer player = OpenPlayer.atIndex(entityIdentifier);
                if (player != null) {
                    hoveredEntities.add(new OSRSPlayer(player));
                }
            }
            case 1 -> {
                OpenNpc npc = OpenNpc.atIndex(entityIdentifier);
                if (npc != null) {
                    hoveredEntities.add(new OSRSNpc(npc));
                }
            }
            case 2 -> {
                Coordinate base = getBase();
                TileArea containment = new TileArea(new Tile(regionX, regionY, getCurrentPlane()));
                OpenGameObject.getLoadedWithin(containment).stream()
                    .filter(obj -> obj.getId() == entityIdentifier)
                    .map(obj -> new OSRSGameObject(obj, base.derive(regionX, regionY)))
                    .forEach(hoveredEntities::add);
            }
            case 3 -> {
                Coordinate base = getBase();
                OpenItemNode.getLoadedWithin(new TileArea(new Tile(regionX, regionY, getCurrentPlane()))).stream()
                    .map(node -> new OSRSGroundItem(node, base.derive(regionX, regionY)))
                    .forEach(hoveredEntities::add);
            }
        }
    }
}
