package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public final class OSRSInventory {
    private static final int REGULAR_WIDGET_INDEX = 149;
    private static final int BANK_OPEN_WIDGET_INDEX = 15;
    private static final int DEPOSIT_BOX_OPEN_WIDGET_INDEX = 192;
    private static final int DEPOSIT_BOX_INVENTORY_WIDGET_INDEX = 23;
    private static final int EQUIP_YOUR_CHARACTER_INDEX = 85;
    private static final int GRAND_EXCHANGE_INVENTORY_INDEX = 467;
    private static final int TRADE_INDEX = 336;
    private static final int SLOT_WIDTH_GAP = 9;
    private static final int SLOT_HEIGHT_GAP = 5;
    private static final int SLOT_WIDTH = 33;
    private static final int SLOT_HEIGHT = 31;

    private OSRSInventory() {
    }

    public static List<InteractableRectangle> getSlotBounds() {
        InterfaceComponent component;
        if (Bank.isOpen()) {
            component = Interfaces.newQuery()
                .containers(BANK_OPEN_WIDGET_INDEX).types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(false).grandchildren(28).results().first();
            if (component != null) {
                component = component.getChild(0);
            }
        } else if (OSRSEquipment.isEquipYourCharacterOpen()) {
            component = Interfaces.getAt(EQUIP_YOUR_CHARACTER_INDEX, 0);
        } else if (OSRSGrandExchange.isOpen()) {
            List<InteractableRectangle> bounds = new ArrayList<>(28);
            for (InterfaceComponent child : Interfaces.newQuery()
                .containers(GRAND_EXCHANGE_INVENTORY_INDEX).widths(36).heights(32).grandchildren(28)
                .types(InterfaceComponent.Type.SPRITE).results()) {
                bounds.add(child.getBounds());
            }
            return bounds;
        } else if (DepositBox.isOpen()) {
            List<InteractableRectangle> bounds = new ArrayList<>(28);
            InterfaceComponent depositBoxInventoryWidget =
                Interfaces.getAt(DEPOSIT_BOX_OPEN_WIDGET_INDEX, DEPOSIT_BOX_INVENTORY_WIDGET_INDEX);
            if (depositBoxInventoryWidget != null) {
                for (InterfaceComponent child : depositBoxInventoryWidget.getChildren()) {
                    bounds.add(child.getBounds());
                }
            }
            return bounds;
        } else if (Trade.isOpen()) {
            component = Interfaces.getAt(TRADE_INDEX, 0);
        } else {
            component = Interfaces.getAt(REGULAR_WIDGET_INDEX, 0, 0);
        }
        if (component != null) {
            final Rectangle base = component.getBounds();
            if (base != null) {
                final InteractableRectangle[] bounds = new InteractableRectangle[28];
                for (int x = 0; x < 4; ++x) {
                    for (int y = 0; y < 7; ++y) {
                        bounds[(y << 2) + x] = new InteractableRectangle(
                            (SLOT_WIDTH * x) + (SLOT_WIDTH_GAP * x) + base.x,
                            (SLOT_HEIGHT * y) + (SLOT_HEIGHT_GAP * y) + base.y,
                            SLOT_WIDTH, SLOT_HEIGHT
                        );
                    }
                }
                return Arrays.asList(bounds);
            }
        }
        return Collections.emptyList();
    }

    public static SpriteItem getSelectedItem() {
        if (Inventory.isItemSelected()) {
            final SpriteItem item = Inventory.getItemIn(OpenClient.getLastSelectedComponentId());
            if (item != null && item.getId() == OpenClient.getLastSelectedInventoryItemId()) {
                return item;
            }
        }
        return null;
    }

    public static boolean isInventoryComponent(int uid) {
        return uid >>> 16 == REGULAR_WIDGET_INDEX;
    }
}
