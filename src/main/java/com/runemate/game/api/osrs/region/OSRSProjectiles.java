package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSProjectiles {
    private OSRSProjectiles() {
    }


    public static LocatableEntityQueryResults<Projectile> getLoaded(final Predicate<? super Projectile> filter) {
        List<OpenProjectile> projectiles = OpenWorldView.getTopLevelWorldView().getLoadedProjectiles();
        return new LocatableEntityQueryResults<>(projectiles.stream()
            .map(OSRSProjectile::new)
            .filter(x -> filter == null || filter.test(x))
            .collect(Collectors.toList()));
    }
}
