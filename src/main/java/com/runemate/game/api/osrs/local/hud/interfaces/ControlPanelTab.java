package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.awt.event.*;
import java.util.*;
import lombok.extern.log4j.*;

@Log4j2
public enum ControlPanelTab implements Tab {
    COMBAT_OPTIONS(4675, 0, "Combat Options"),
    SKILLS(4676, 1, "Skills"),
    CHARACTER_SUMMARY(4677, 2, 8168, 0, Actions.PROGRESS_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getProgressTab(0);
            }
            return super.getComponent();
        }
    },
    QUEST_LIST(4677, 2, 8168, 1, Actions.PROGRESS_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getProgressTab(1);
            }
            return super.getComponent();
        }

        @Override
        public boolean isOpenable() {
            InterfaceComponent component = getComponent();
            return component != null && component.isVisible();
        }
    },
    ACHIEVEMENT_DIARIES(4677, 2, 8168, 2, Actions.PROGRESS_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getProgressTab(2);
            }
            return super.getComponent();
        }
    },
    KOUREND_FAVOUR(4677, 2, 8168, 3, Actions.PROGRESS_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getProgressTab(3);
            }
            return super.getComponent();
        }
    },
    INVENTORY(4678, 3, "Inventory") {
        @Override
        public boolean isOpen() {
            return super.isOpen()
                || Bank.isOpen()
                || Shop.isOpen()
                || DepositBox.isOpen()
                || OSRSEquipment.isEquipYourCharacterOpen()
                || Trade.Screen.OFFER.isOpen();
        }
    },
    EQUIPMENT(4679, 4, "Worn Equipment"),
    PRAYER(4680, 5, "Prayer"),
    MAGIC(4682, 6, "Magic"),
    CHAT_CHANNEL(4683, 7, 13071, 0, Actions.SOCIAL_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getSocialTab(0);
            }
            return super.getComponent();
        }
    },
    YOUR_CLAN(4683, 7, 13071, 1, Actions.SOCIAL_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getSocialTab(1);
            }
            return super.getComponent();
        }
    },
    VIEW_ANOTHER_CLAN(4683, 7, 13071, 2, Actions.SOCIAL_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getSocialTab(2);
            }
            return super.getComponent();
        }
    },
    GROUPING(4683, 7, 13071, 3, Actions.SOCIAL_TAB) {
        @Override
        public InterfaceComponent getComponent() {
            if (isParentOpen()) {
                return ControlPanelTab.getSocialTab(3);
            }
            return super.getComponent();
        }
    },
    FRIENDS_LIST(4684, 9, 6516, 0, "Friends List", "Ignore List"),
    ACCOUNT_MANAGEMENT(6517, 8, "Account Management"),
    LOGOUT(4689, 10, "Logout", "Close") {
        @Override
        public boolean isOpen() {
            return super.isOpen() && !WORLD_HOP.isOpen();
        }

        @Override
        public InterfaceComponent getComponent() {
            if (WORLD_HOP.isOpen()) {
                log.debug("World-hop is open, using \"Close\" component");
                return Interfaces.newQuery().containers(69).actions("Close")
                    .types(InterfaceComponent.Type.SPRITE).results().first();
            }
            if (OSRSInterfaceOptions.isViewportResizable() && OptionsTab.isUsingSidePanels()) {
                log.debug("Side-panels are configured, using alternate InterfaceComponent for LOGOUT");
                return Interfaces.newQuery().types(InterfaceComponent.Type.SPRITE)
                    .grandchildren(false).containers(164).actions("Logout").results().first();
            }
            return super.getComponent();
        }

        @Override
        public boolean open(final boolean forceClick) {
            if (isOpen()) {
                return true;
            }
            log.info("Opening {}", name());
            if (!isParentOpen()) {
                if (!forceClick && LOGOUT.openWithHotkey() || LOGOUT.openWithClick()) {
                    if (!Execution.delayUntil(this::isParentOpen, 600)) {
                        log.warn("Failed to open parent {}", name());
                        return false;
                    }
                }
                return isOpen();
            }
            final InterfaceComponent component = getComponent();
            return component != null
                && component.interact(Regex.getPatternForExactStrings(LOGOUT.actions))
                && Execution.delayUntil(this::isOpen, 600);
        }

    },
    /**
     * @deprecated This tab no longer exists.
     * Use {@link ControlPanelTab#SETTINGS} instead.
     */
    @Deprecated
    OPTIONS(4686, Integer.MAX_VALUE, "Options"),
    SETTINGS(4686, 11, "Settings"),
    EMOTES(4687, 12, "Emotes"),
    MUSIC_PLAYER(4688, 13, "Music Player"),
    WORLD_HOP(-1, 10, 8374, 1, "World Hop") {
        @Override
        public boolean isOpen() {
            final InterfaceComponent container = Interfaces.newQuery()
                .containers(69)
                .widths(190)
                .types(InterfaceComponent.Type.BOX)
                .grandchildren(false)
                .heights(204)
                .results()
                .first();
            return container != null && container.isVisible();
        }

        @Override
        public boolean open(boolean force) {
            if (isOpen()) {
                return true;
            }
            log.info("Opening {}", name());
            if (ControlPanelTab.LOGOUT.open(force)) {
                final InterfaceComponent button = Interfaces.newQuery()
                    .containers(182)
                    .grandchildren(false)
                    .actions("World Switcher")
                    .types(InterfaceComponent.Type.CONTAINER)
                    .results()
                    .first();
                return button != null
                    && button.isVisible()
                    && button.interact("World Switcher")
                    && Execution.delayUntil(this::isOpen, 1200, 1800);
            }
            return false;
        }
    };
    private final String[] actions;
    private final int hotkeyVarbit;
    private final int parentTabId, childStateId, childStateVarbit;


    ControlPanelTab(final int hotkeyVarbit, final int parentTabId, final String... actions) {
        this.hotkeyVarbit = hotkeyVarbit;
        this.actions = actions;
        this.parentTabId = parentTabId;
        this.childStateVarbit = -1;
        this.childStateId = -1;
    }

    ControlPanelTab(
        final int hotkeyVarbit, final int parentTabId, final int childStateVarbit,
        final int childStateId, final String... actions
    ) {
        this.hotkeyVarbit = hotkeyVarbit;
        this.actions = actions;
        this.parentTabId = parentTabId;
        this.childStateVarbit = childStateVarbit;
        this.childStateId = childStateId;
    }

    public static ControlPanelTab getOpened() {
        for (final ControlPanelTab tab : values()) {
            if (tab.isOpen()) {
                return tab;
            }
        }
        return null;
    }

    private static int getParentTabOpened() {
        return Varcs.getInt(171);
    }

    private static InterfaceComponent getSocialTab(int index) {
        if (index < 0) {
            return null;
        }
        final InterfaceComponentQueryResults results = Interfaces.newQuery()
            .containers(707)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions(Actions.SOCIAL_TAB)
            .results()
            .sort(Comparator.comparingInt(InterfaceComponent::getIndex));

        if (results.isEmpty() || index >= results.size()) {
            return null;
        }
        return results.get(index);
    }

    private static InterfaceComponent getProgressTab(int index) {
        if (index < 0) {
            return null;
        }
        final InterfaceComponentQueryResults results = Interfaces.newQuery()
            .containers(629)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions(Actions.PROGRESS_TAB)
            .results()
            .sort(Comparator.comparingInt(InterfaceComponent::getIndex));

        if (results.isEmpty() || index >= results.size()) {
            return null;
        }
        return results.get(index);
    }

    @Override
    public boolean open() {
        return open(!PlayerSense.getAsBoolean(PlayerSense.Key.USE_MISC_HOTKEYS));
    }

    public boolean isParentOpen() {
        return getParentTabOpened() == parentTabId;
    }

    @Override
    public boolean isOpen() {
        final int state = getParentTabOpened();
        if (state != parentTabId) {
            return false;
        }
        if (childStateVarbit == -1) {
            return true;
        }
        final Varbit childState = Varbits.load(childStateVarbit);
        return childState != null && childState.getValue() == childStateId;
    }

    public InterfaceComponent getComponent() {
        return Interfaces.newQuery().grandchildren(false)
            .types(InterfaceComponent.Type.SPRITE)
            .containers(InterfaceContainers.getRootIndex()).actions(actions)
            .results().first();
    }

    public boolean open(final boolean forceClick) {
        if (isOpen()) {
            return true;
        }
        log.info("Opening {}", name());
        if (!isParentOpen()) {
            if (!forceClick && openWithHotkey() || openWithClick()) {
                if (!Execution.delayUntil(this::isParentOpen, 600)) {
                    log.warn("Failed to open parent: {}", name());
                    return false;
                }
            }
        }
        if (isOpen()) {
            return true;
        }
        final InterfaceComponent component = getComponent();
        return component != null
            && (component.interact(Regex.getPatternForExactStrings(actions)) || component.click())
            && Execution.delayUntil(this::isOpen, 600);
    }

    private boolean openWithHotkey() {
        final HotKey hotkey;
        if (hotkeyVarbit == -1 || (hotkey = getHotKey()) == null || hotkey == HotKey.NONE) {
            return false;
        }
        return Keyboard.typeKey(hotkey.keyCode);
    }

    private boolean openWithClick() {
        final InterfaceComponent component = getComponent();
        return component != null && component.click();
    }

    private HotKey getHotKey() {
        if (OSRSInterfaceOptions.getTabLayout() != OSRSInterfaceOptions.TabLayout.LINE || !OptionsTab.areSidePanelsClosableByHotkeys()) {
            return null;
        }
        final Varbit hkValue = Varbits.load(hotkeyVarbit);
        return hkValue != null ? HotKey.getKey(hkValue.getValue()) : null;
    }

    @Override
    public String toString() {
        return "ControlPanelTab." + name();
    }

    @Deprecated
    private boolean isOpen(InterfaceComponent component) {
        return component != null && component.getSpriteId() != -1 && !Bank.isOpen();
    }

    public boolean isOpenable() {
        return true;
    }

    private enum HotKey {
        NONE(0, -1),
        F1(1, KeyEvent.VK_F1),
        F2(2, KeyEvent.VK_F2),
        F3(3, KeyEvent.VK_F3),
        F4(4, KeyEvent.VK_F4),
        F5(5, KeyEvent.VK_F5),
        F6(6, KeyEvent.VK_F6),
        F7(7, KeyEvent.VK_F7),
        F8(8, KeyEvent.VK_F8),
        F9(9, KeyEvent.VK_F9),
        F10(10, KeyEvent.VK_F10),
        F11(11, KeyEvent.VK_F11),
        F12(12, KeyEvent.VK_F12),
        ESC(13, KeyEvent.VK_ESCAPE);

        private final int varbitValue;
        private final int keyCode;

        /**
         * @param varbitValue is the value of the key when it is represented as a varbit
         * @param keyCode     is the KeyEvent key code for the key
         */
        HotKey(int varbitValue, int keyCode) {
            this.varbitValue = varbitValue;
            this.keyCode = keyCode;
        }

        /**
         * @param varbitValue Finds the key that belongs to the varbit provided.
         * @return The SettingKey found.
         */
        private static HotKey getKey(int varbitValue) {
            if (varbitValue == -1) {
                return null;
            }
            for (HotKey hk : values()) {
                if (hk.getVarbitValue() == varbitValue) {
                    return hk;
                }
            }
            return null;
        }

        /**
         * @return Returns the varbit value representation of the key
         */
        public int getVarbitValue() {
            return varbitValue;
        }

        /**
         * Gets the Key number to press to activate this SettingKey
         *
         * @return Returns the Key Code
         */
        public int getKeyCode() {
            return keyCode;
        }

    }

    private static class Actions {

        private final static String[] PROGRESS_TAB = new String[] {
                "Character Summary", "Quest List", "Achievement Diaries", "Kourend Tasks", "Kourend Favour"
            };
        private final static String[] SOCIAL_TAB = new String[] { "Chat-channel", "Your Clan", "View another clan", "Grouping" };
    }
}