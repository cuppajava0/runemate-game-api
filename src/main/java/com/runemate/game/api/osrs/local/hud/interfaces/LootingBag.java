package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.function.*;
import javax.annotation.*;

public final class LootingBag {

    private LootingBag() {
    }

    public static boolean isOpen() {
        return Inventories.opened(Inventories.Documented.LOOTING_BAG);
    }

    public static int getUsedSlots() {
        return getItems().size();
    }

    public static int getEmptySlots() {
        return 28 - getUsedSlots();
    }

    public static boolean isEmpty() {
        return getUsedSlots() == 0;
    }

    public static boolean isFull() {
        return getEmptySlots() == 0;
    }

    public static SpriteItemQueryResults getItems() {
        return getItems((Predicate<SpriteItem>) null);
    }

    @Nullable
    public static SpriteItem getItemIn(final int slot) {
        return newQuery().indices(slot).results().first();
    }

    public static SpriteItemQueryResults getItems(Predicate<SpriteItem> filter) {
        return newQuery().filter(filter).results();
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.LOOTING_BAG);
    }
}
