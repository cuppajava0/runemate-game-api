package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public enum Magic implements Spell {
    LUMBRIDGE_HOME_TELEPORT("Lumbridge Home Teleport"),
    WIND_STRIKE("Wind Strike", 1),
    CONFUSE("Confuse"),
    ENCHANT_CROSSBOW_BOLT("Crossbow Bolt Enchantments", "View"),
    WATER_STRIKE("Water Strike", 2),
    LVL_1_ENCHANT("Lvl-1 Enchant", true),
    EARTH_STRIKE("Earth Strike", 3),
    WEAKEN("Weaken"),
    FIRE_STRIKE("Fire Strike", 4),
    BONES_TO_BANANAS("Bones to Bananas"),
    WIND_BOLT("Wind Bolt", 5),
    CURSE("Curse"),
    BIND("Bind"),
    LOW_LEVEL_ALCHEMY("Low Level Alchemy"),
    WATER_BOLT("Water Bolt", 6),
    VARROCK_TELEPORT("Varrock Teleport"),
    GRAND_EXCHANGE_TELEPORT("Varrock Teleport", "Grand Exchange"),
    LVL_2_ENCHANT("Lvl-2 Enchant", true),
    EARTH_BOLT("Earth Bolt", 7),
    LUMBRIDGE_TELEPORT("Lumbridge Teleport"),
    TELEKINETIC_GRAB("Telekinetic Grab"),
    FIRE_BOLT("Fire Bolt", 8),
    FALADOR_TELEPORT("Falador Teleport"),
    CRUMBLE_UNDEAD("Crumble Undead", 17),
    TELEPORT_TO_HOUSE("Teleport to House"),
    TELEPORT_OUTSIDE_HOUSE("Teleport to House", "Outside"),
    WIND_BLAST("Wind Blast", 9),
    SUPERHEAT_ITEM("Superheat Item"),
    CAMELOT_TELEPORT("Camelot Teleport"),
    SEERS_TELEPORT("Camelot Teleport", "Seers'"),
    WATER_BLAST("Water Blast", 10),
    LVL_3_ENCHANT("Lvl-3 Enchant", true),
    KOUREND_CASTLE_TELEPORT("Kourend Castle Teleport"),
    IBAN_BLAST("Iban Blast", 47),
    SNARE("Snare"),
    MAGIC_DART("Magic Dart", 18),
    ARDOUGNE_TELEPORT("Ardougne Teleport"),
    EARTH_BLAST("Earth Blast", 11),
    CIVITAS_ILLA_FORTIS_TELEPORT("Civitas illa Fortis Teleport"),
    HIGH_LEVEL_ALCHEMY("High Level Alchemy"),
    CHARGE_WATER_ORB("Charge Water Orb"),
    LVL_4_ENCHANT("Lvl-4 Enchant", true),
    WATCHTOWER_TELEPORT("Watchtower Teleport"),
    YANILLE_TELEPORT("Watchtower Teleport", "Yanille"),
    FIRE_BLAST("Fire Blast", 12),
    CHARGE_EARTH_ORB("Charge Earth Orb"),
    BONES_TO_PEACHES("Bones to Peaches"),
    SARADOMIN_STRIKE("Saradomin Strike", 52),
    CLAWS_OF_GUTHIX("Claws of Guthix", 19),
    FLAMES_OF_ZAMORAK("Flames of Zamorak", 20),
    TROLLHEIM_TELEPORT("Trollheim Teleport"),
    WIND_WAVE("Wind Wave", 13),
    CHARGE_FIRE_ORB("Charge Fire Orb"),
    @Deprecated
    TELEPORT_TO_APE_ATOLL("Ape Atoll Teleport"),
    APE_ATOLL_TELEPORT("Ape Atoll Teleport"),
    WATER_WAVE("Water Wave", 14),
    CHARGE_AIR_ORB("Charge Air Orb"),
    VULNERABILITY("Vulnerability"),
    LVL_5_ENCHANT("Lvl-5 Enchant", true),
    EARTH_WAVE("Earth Wave", 15),
    ENFEEBLE("Enfeeble"),
    TELEOTHER_LUMBRIDGE("Teleother Lumbridge"),
    FIRE_WAVE("Fire Wave", 16),
    ENTANGLE("Entangle"),
    STUN("Stun"),
    CHARGE("Charge"),
    WIND_SURGE("Wind Surge", 48),
    TELEOTHER_FALADOR("Teleother Falador"),
    WATER_SURGE("Water Surge", 49),
    TELE_BLOCK("Tele Block"),
    @Deprecated
    TELEPORT_TO_BOUNTY_TARGET("Teleport to Target"),
    TELEPORT_TO_TARGET("Teleport to Target"),
    LVL_6_ENCHANT("Lvl-6 Enchant", true),
    TELEOTHER_CAMELOT("Teleother Camelot"),
    EARTH_SURGE("Earth Surge", 50),
    LEVEL_7_ENCHANT("Lvl-7 Enchant", true),
    FIRE_SURGE("Fire Surge", 51);

    private static final int CONTAINER = 218;
    private static final String JEWELLERY_ENCHANTMENTS_VIEW_NAME = "Jewellery Enchantments";
    private static final String JEWELLERY_ENCHANTMENTS_BACK_ACTION = "Back";

    private final String name;
    private final int autocastVarbitValue;
    private final String action;
    private final boolean jewelleryEnchantment;

    Magic(String name) {
        this(name, "Cast");
    }

    Magic(String name, int autocastVarbitValue) {
        this(name, autocastVarbitValue, "Cast");
    }

    Magic(String name, String action) {
        this(name, -1, action);
    }

    Magic(String name, boolean jewelleryEnchantment) {
        this(name, -1, "Cast", jewelleryEnchantment);
    }

    Magic(String name, int autocastVarbitValue, String action) {
        this(name, autocastVarbitValue, action, false);
    }

    Magic(String name, int autocastVarbitValue, String action, boolean jewelleryEnchantment) {
        this.name = name;
        this.autocastVarbitValue = autocastVarbitValue;
        this.action = action;
        this.jewelleryEnchantment = jewelleryEnchantment;
    }

    /**
     * @return the currently selected spell from the enumeration, null if none is selected or an unrecognised spell is selected.
     */
    public static Magic getSelected() {
        InterfaceComponent selected = Interfaces.getSelected();
        if (selected != null) {
            String name = selected.getName();
            if (name != null) {
                for (Magic magic : values()) {
                    if (magic.name.equals(name)) {
                        return magic;
                    }
                }
            }
        }
        return null;
    }

    public static Spell getAutocastingSpell() {
        for (Magic magic : Magic.values()) {
            if (magic.isAutocasting()) {
                return magic;
            }
        }
        for (Lunar lunar : Lunar.values()) {
            if (lunar.isAutocasting()) {
                return lunar;
            }
        }
        for (Ancient ancient : Ancient.values()) {
            if (ancient.isAutocasting()) {
                return ancient;
            }
        }
        for (Arceuus arceuus : Arceuus.values()) {
            if (arceuus.isAutocasting()) {
                return arceuus;
            }
        }
        return null;
    }

    private static boolean isJewelleryEnchantmentMenuOpen() {
        return VarbitID.JEWELLERY_ENCHANTMENT_MENU.getValue() == 1;
    }

    private static boolean openJewelleryEnchantments() {
        if (isJewelleryEnchantmentMenuOpen()) {
            return true;
        }

        final InterfaceComponent button = getComponent(JEWELLERY_ENCHANTMENTS_VIEW_NAME);
        return button != null
            && button.isVisible()
            && button.interact("View");
    }

    private static boolean closeJewelleryEnchantments() {
        if (!isJewelleryEnchantmentMenuOpen()) {
            return true;
        }

        final InterfaceComponent button = Interfaces.newQuery()
            .containers(CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.SPRITE)
            .actions(JEWELLERY_ENCHANTMENTS_BACK_ACTION)
            .results().first();
        return button != null
            && button.isVisible()
            && button.interact("Back");
    }

    /**
     * Implementation detail so no longer exposed.
     *
     * @return an int that should be the sprite id when a spell is castable.
     */
    @Deprecated
    public int getSpriteIdWhenAvailable() {
        return -1;
    }

    /**
     * Implementation detail so no longer exposed.
     *
     * @return an int that should be the sprite id when a spell is not castable.
     */
    @Deprecated
    public int getSpriteIdWhenUnavailable() {
        return -1;
    }

    /**
     * Passes a custom string to Magic#activate() to allow for interaction with spells with more than one action. For example,
     * Camelot Teleport and Varrock Teleport have more than one action upon completion of Achievement Diaries.
     * <p>
     *
     * @param action The action to perform. eg. "Cast" or "Camelot"
     * @return true the spell was successfully interacted with using the String provided.
     */
    public boolean activate(final String action) {
        final InterfaceComponent button;
        log.info("Activating {} with action {}", this, action);
        return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
            && (jewelleryEnchantment ? openJewelleryEnchantments() : closeJewelleryEnchantments())
            && (button = getComponent()) != null
            && button.isVisible()
            && button.interact(action);
    }

    /**
     * Activates the given spell.
     *
     * @return true if the spell was successfully interacted with
     * @see #activate(String)
     */
    public boolean activate() {
        return activate(action);
    }


    /**
     * Deactivates a spell which is currently activated.
     *
     * @return If the spell was successfully deactivated or the spell was already deactivated.
     */
    public boolean deactivate() {
        if (!isSelected()) {
            return true;
        }
        log.info("Deactivating {}", this);
        final InterfaceComponent button;
        return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
            && (button = getComponent()) != null
            && button.isVisible()
            && button.click()
            && Execution.delayWhile(this::isSelected, 600, 1200);
    }

    /**
     * Gets the {@code InterfaceComponent} used to select/deselect the spell
     *
     * @return the {@code InterfaceComponent} used to interact with the spell
     */
    public InterfaceComponent getComponent() {
        return getComponent(name);
    }

    private static InterfaceComponent getComponent(final String name) {
        return Interfaces.newQuery()
            .containers(CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.SPRITE)
            .names(name)
            .results().first();
    }

    /**
     * Determines whether or not a spell is currently selected by the border it has in the spellbook interface. eg. High Level Alchemy has been selected
     * but has not yet been cast on an item.
     *
     * @return true if the spell is currently selected
     */
    public boolean isSelected() {
        InterfaceComponent component = getComponent();
        return component != null && component.getSpriteBorderInset() == 2;
    }

    public boolean isAutocasting() {
        if (autocastVarbitValue != -1) {
            Varbit varbit = Varbits.load(276);
            return varbit != null && varbit.getValue() == autocastVarbitValue;
        }
        return false;
    }

    @Override
    public SpellBook getSpellBook() {
        return Book.STANDARD;
    }

    public enum Ancient implements Spell {
        EDGEVILLE_HOME_TELEPORT("Edgeville Home Teleport"),
        SMOKE_RUSH("Smoke Rush", 31),
        SHADOW_RUSH("Shadow Rush", 32),
        PADDEWWA_TELEPORT("Paddewwa Teleport"),
        BLOOD_RUSH("Blood Rush", 33),
        ICE_RUSH("Ice Rush", 34),
        SENNTISTEN_TELEPORT("Senntisten Teleport"),
        SMOKE_BURST("Smoke Burst", 35),
        SHADOW_BURST("Shadow Burst", 36),
        KHARYRLL_TELEPORT("Kharyrll Teleport"),
        BLOOD_BURST("Blood Burst", 37),
        ICE_BURST("Ice Burst", 38),
        LASSAR_TELEPORT("Lassar Teleport"),
        SMOKE_BLITZ("Smoke Blitz", 39),
        SHADOW_BLITZ("Shadow Blitz", 40),
        DAREEYAK_TELEPORT("Dareeyak Teleport"),
        BLOOD_BLITZ("Blood Blitz", 41),
        ICE_BLITZ("Ice Blitz", 42),
        CARRALLANGAR_TELEPORT("Carrallanger Teleport"),
        TELEPORT_TO_BOUNTY_TARGET("Teleport to Target"),
        SMOKE_BARRAGE("Smoke Barrage", 43),
        SHADOW_BARRAGE("Shadow Barrage", 44),
        ANNAKARL_TELEPORT("Annakarl Teleport"),
        BLOOD_BARRAGE("Blood Barrage", 45),
        ICE_BARRAGE("Ice Barrage", 46),
        GHORROCK_TELEPORT("Ghorrock Teleport");

        private static final int CONTAINER = 218;

        private final String name;
        private final int autocastVarbitValue;

        Ancient(String name) {
            this(name, -1);
        }

        Ancient(String name, int autocastingVarbitValue) {
            this.name = name;
            this.autocastVarbitValue = autocastingVarbitValue;
        }


        public static Ancient getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                String name = selected.getName();
                if (name != null) {
                    for (Ancient magic : values()) {
                        if (magic.name.equals(name)) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        @Deprecated
        public int getSpriteIdWhenAvailable() {
            return -1;
        }

        @Deprecated
        public int getSpriteIdWhenUnavailable() {
            return -1;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact(action);
        }

        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate("Cast");
        }

        /**
         * @return True if the interaction to deactivate the spell was successfully.
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible() && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return getComponent(name);
        }

        private static InterfaceComponent getComponent(final String name) {
            return Interfaces.newQuery()
                .containers(CONTAINER)
                .grandchildren(false)
                .types(InterfaceComponent.Type.SPRITE)
                .names(name)
                .results().first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        public boolean isAutocasting() {
            if (autocastVarbitValue != -1) {
                Varbit varbit = Varbits.load(276);
                return varbit != null && varbit.getValue() == autocastVarbitValue;
            }
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.ANCIENT;
        }
    }

    public enum Arceuus implements Spell {

        ARCEUUS_HOME_TELEPORT("Arceuus Home Teleport"),
        ARCEUUS_LIBRARY_TELEPORT("Arceuus Library Teleport"),
        BASIC_REANIMATION("Basic Reanimation", "Reanimate"),
        DRAYNOR_MANOR_TELEPORT("Draynor Manor Teleport"),
        BATTLEFRONT_TELEPORT("Battlefront Teleport"),
        MIND_ALTAR_TELEPORT("Mind Altar Teleport"),
        RESPAWN_TELEPORT("Respawn Teleport"),
        GHOSTLY_GRASP("Ghostly Grasp", 56),
        RESURRECT_LESSER_GHOST("Resurrect Lesser Ghost"),
        RESURRECT_LESSER_SKELETON("Resurrect Lesser Skeleton"),
        RESURRECT_LESSER_ZOMBIE("Resurrect Lesser Zombie"),
        SALVE_GRAVEYARD_TELEPORT("Salve Graveyard Teleport"),
        ADEPT_REANIMATION("Adept Reanimation", "Reanimate"),
        INFERIOR_DEMONBANE("Inferior Demonbane", 53),
        SHADOW_VEIL("Shadow Veil"),
        FENKENSTRAINS_CASTLE_TELEPORT("Fenkenstrain's Castle Teleport"),
        DARK_LURE("Dark Lure"),
        SKELETAL_GRASP("Skeletal Grasp", 57),
        RESURRECT_SUPERIOR_GHOST("Resurrect Superior Ghost"),
        RESURRECT_SUPERIOR_SKELETON("Resurrect Superior Skeleton"),
        RESURRECT_SUPERIOR_ZOMBIE("Resurrect Superior Zombie"),
        MARK_OF_DARKNESS("Mark of Darkness"),
        WEST_ARDOUGNE_TELEPORT("West Ardougne Teleport"),
        SUPERIOR_DEMONBANE("Superior Demonbane", 54),
        LESSER_CORRUPTION("Lesser Corruption"),
        HARMONY_ISLAND_TELEPORT("Harmony Island Teleport"),
        VILE_VIGOUR("Vile Vigour"),
        DEGRIME("Degrime"),
        CEMETERY_TELEPORT("Cemetery Teleport"),
        EXPERT_REANIMATION("Expert Reanimation", "Reanimate"),
        WARD_OF_ARCEUUS("Ward of Arceuus"),
        RESURRECT_GREATER_GHOST("Resurrect Greater Ghost"),
        RESURRECT_GREATER_SKELETON("Resurrect Greater Skeleton"),
        RESURRECT_GREATER_ZOMBIE("Resurrect Greater Zombie"),
        RESURRECT_CROPS("Resurrect Crops"),
        UNDEAD_GRASP("Undead Grasp", 58),
        DEATH_CHARGE("Death Charge"),
        DARK_DEMONBANE("Dark Demonbane", 55),
        BARROWS_TELEPORT("Barrows Teleport"),
        DEMONIC_OFFERING("Demonic Offering"),
        GREATER_CORRUPTION("Greater Corruption"),
        MASTER_REANIMATION("Master Reanimation", "Reanimate"),
        APE_ATOLL_TELEPORT("Ape Atoll Teleport"),
        SINISTER_OFFERING("Sinister Offering");

        private static final int CONTAINER = 218;

        private final String name;
        private final int autocastVarbitValue;
        private final String action;

        Arceuus(String name) {
            this(name, "Cast");
        }

        Arceuus(String name, int autocastVarbitValue) {
            this(name, autocastVarbitValue, "Cast");
        }

        Arceuus(String name, String action) {
            this(name, -1, action);
        }

        Arceuus(String name, int autocastVarbitValue, String action) {
            this.name = name;
            this.autocastVarbitValue = autocastVarbitValue;
            this.action = action;
        }

        public static Arceuus getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                String name = selected.getName();
                if (name != null) {
                    for (Arceuus magic : values()) {
                        if (magic.name.equals(name)) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        @Deprecated
        public int getSpriteIdWhenAvailable() {
            return -1;
        }

        @Deprecated
        public int getSpriteIdWhenUnavailable() {
            return -1;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open()) &&
                (button = getComponent()) != null && button.isVisible() && button.interact(action);
        }


        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate(action);
        }

        /**
         * @return True if the interaction to deactivate the spell was successfully.
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible() && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return getComponent(name);
        }

        private static InterfaceComponent getComponent(final String name) {
            return Interfaces.newQuery()
                .containers(CONTAINER)
                .grandchildren(false)
                .types(InterfaceComponent.Type.SPRITE)
                .names(name)
                .results().first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        @Override
        public boolean isAutocasting() {
            if (autocastVarbitValue != -1) {
                Varbit varbit = Varbits.load(276);
                return varbit != null && varbit.getValue() == autocastVarbitValue;
            }
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.ARCEUUS;
        }
    }

    public enum Lunar implements Spell {
        LUNAR_HOME_TELEPORT("Lunar Home Teleport"),
        BAKE_PIE("Bake Pie"),
        GEOMANCY("Geomancy"),
        CURE_PLANT("Cure Plant"),
        MONSTER_EXAMINE("Monster Examine"),
        NPC_CONTACT("NPC Contact"),
        CURE_OTHER("Cure Other"),
        HUMIDIFY("Humidify"),
        MOONCLAN_TELEPORT("Moonclan Teleport"),
        OURANIA_TELEPORT("Ourania Teleport"),
        TELE_GROUP_MOONCLAN("Tele Group Moonclan"),
        CURE_ME("Cure Me"),
        HUNTER_KIT("Hunter Kit"),
        WATERBIRTH_TELEPORT("Waterbirth Teleport"),
        TELE_GROUP_WATERBIRTH("Tele Group Waterbirth"),
        CURE_GROUP("Cure Group"),
        STAT_SPY("Stat Spy"),
        BARBARIAN_TELEPORT("Barbarian Teleport"),
        TELE_GROUP_BARBARIAN("Tele Group Barbarian"),
        SUPERGLASS_MAKE("Superglass Make"),
        KHAZARD_TELEPORT("Khazard Teleport"),
        TELE_GROUP_KHAZARD("Tele Group Khazard"),
        DREAM("Dream"),
        STRING_JEWELLERY("String Jewellery"),
        SPIN_FLAX("Spin Flax"),
        STAT_RESTORE_POT_SHARE("Stat Restore Pot Share"),
        MAGIC_IMBUE("Magic Imbue"),
        FERTILE_SOIL("Fertile Soil"),
        BOOST_POTION_SHARE("Boost Potion Share"),
        FISHING_GUILD_TELEPORT("Fishing Guild Teleport"),
        TELE_GROUP_FISHING_GUILD("Tele Group Fishing Guild"),
        PLANK_MAKE("Plank Make"),
        CATHERBY_TELEPORT("Catherby Teleport"),
        TELE_GROUP_CATHERBY("Tele Group Catherby"),
        RECHARGE_DRAGONSTONE("Recharge Dragonstone"),
        ICE_PLATEAU_TELEPORT("Ice Plateau Teleport"),
        TELE_GROUP_ICE_PLATEAU("Tele Group Ice Plateau"),
        ENERGY_TRANSFER("Energy Transfer"),
        HEAL_OTHER("Heal Other"),
        VENGEANCE_OTHER("Vengeance Other"),
        VENGEANCE("Vengeance"),
        HEAL_GROUP("Heal Group"),
        SPELLBOOK_SWAP("Spellbook Swap"),
        TAN_LEATHER("Tan Leather");

        private static final int CONTAINER = 218;

        private final String name;

        Lunar(String name) {
            this.name = name;
        }

        public static Lunar getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                String name = selected.getName();
                if (name != null) {
                    for (Lunar magic : values()) {
                        if (magic.name.equals(name)) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        @Deprecated
        public int getSpriteIdWhenAvailable() {
            return -1;
        }

        @Deprecated
        public int getSpriteIdWhenUnavailable() {
            return -1;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact(action);
        }

        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate("Cast");
        }

        /**
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return getComponent(name);
        }

        private static InterfaceComponent getComponent(final String name) {
            return Interfaces.newQuery()
                .containers(CONTAINER)
                .grandchildren(false)
                .types(InterfaceComponent.Type.SPRITE)
                .names(name)
                .results().first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        @Override
        public boolean isAutocasting() {
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.LUNAR;
        }
    }

    public enum Book implements SpellBook {
        STANDARD,
        ANCIENT,
        LUNAR,
        ARCEUUS;

        /**
         * @return the currently activated spell-book from the enumeration
         */
        public static Book getCurrent() {
            return switch (Varps.getAt(439).getValue() & 0b11) {
                case 0 -> STANDARD;
                case 1 -> ANCIENT;
                case 2 -> LUNAR;
                case 0x3 -> ARCEUUS;
                default -> null;
            };
        }

        public static Book get(int id) {
            for (Book book : values()) {
                if (book.getId() == id) {
                    return book;
                }
            }
            return null;
        }

        @Override
        public int getId() {
            return 439 + ordinal();
        }

        @Override
        public boolean isCurrent() {
            return this == getCurrent();
        }
    }

    @RequiredArgsConstructor
    public enum Filter {
        SHOW_COMBAT_SPELLS(6605),
        SHOW_TELEPORT_SPELLS(6609),
        SHOW_UTILITY_SPELLS(6606),
        SHOW_MISSING_LEVEL(6607),
        SHOW_MISSING_RUNES(6608),
        SHOW_MISSING_REQUIREMENTS(12137),
        ICON_RESIZING(6548);

        private final int varbit;

        public boolean isEnabled() {
            final var varbit = Varbits.load(this.varbit);
            return varbit != null && varbit.getValue() == 0;
        }
    }
}
