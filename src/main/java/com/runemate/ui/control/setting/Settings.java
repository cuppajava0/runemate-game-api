package com.runemate.ui.control.setting;

import com.runemate.game.api.bot.data.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import java.text.*;
import java.util.Objects;
import java.util.*;
import java.util.Optional;
import java.util.regex.*;
import java.util.stream.*;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2(topic = "Settings")
public class Settings implements SettingsListener {

    private final AbstractBot bot;
    private final SettingsManager manager;

    @Getter
    private final Map<Setting, Control> settings = new HashMap<>();

    public Settings(final AbstractBot bot) {
        this.bot = bot;
        this.manager = bot.getSettingsManager();

        var groups = new ArrayList<>(manager.getSettingsDescriptors());
        Collections.reverse(groups);
        var sections = groups.stream().flatMap(group -> group.sections().stream()).collect(Collectors.toSet());
        var controls = new Controls();
        for (var group : groups) {
            for (var setting : group.settings()) {
                var section = sections.stream()
                    .filter(s -> s.key().equals(setting.setting().section()))
                    .findFirst()
                    .orElse(null);

                var desc = new Setting(group, section, setting);
                var control = controls.build(bot, manager, desc);
                if (control == null) {
                    continue;
                }

                if (!Strings.isNullOrEmpty(setting.setting().description())) {
                    control.setTooltip(new Tooltip(setting.setting().description()));
                }

                settings.put(desc, control);
            }
        }

        settings.keySet().forEach(setting -> {
            var previous = manager.get(setting.group(), setting.setting());
            if (previous != null) {
                setting.settingProperty().set(previous);
            }
        });
        bot.getEventDispatcher().addListener(this);
    }

    public Map<Setting, Control> bySection(SettingsSectionDescriptor section) {
        return Maps.filterKeys(settings, setting -> setting.belongsTo(section));
    }

    public static Comparator<Setting> orderedSettings() {
        return (a, b) -> ComparisonChain.start()
            .compare(a.settingOrder(), b.settingOrder())
            .compare(a.setting().title(), b.setting().title())
            .result();
    }

    @Override
    public void onSettingChanged(SettingChangedEvent event) {
        Platform.runLater(() -> {
            //Check dependencies
            settings.forEach((setting, control) -> {
                // Don't want to re-publish user events
                if (setting.accepts(event) && event.getSource() != SettingChangedEvent.Source.USER) {
                    setting.setSettingValue((String) event.getValue(), event.getSource());
                }

                control.setVisible(setting.shouldBeShown());
            });
        });
    }

    @Override
    public void onSettingsConfirmed() {
        //noop
    }

    @Getter
    @Accessors(fluent = true)
    public class Setting {

        private final SettingsDescriptor group;
        private final SettingsSectionDescriptor section;
        private final SettingDescriptor setting;
        private final StringProperty settingProperty;

        private final Collection<Dependency> dependencies;

        public Setting(SettingsDescriptor group, SettingsSectionDescriptor section, SettingDescriptor setting) {
            this.group = group;
            this.section = section;
            this.setting = setting;
            this.settingProperty = new SimpleStringProperty();

            dependencies = new ArrayList<>();
            for (var dependency : setting.dependencies()) {
                dependencies.add(new Dependency(dependency));
            }

            if (section != null) {
                for (var dependency : section.dependencies()) {
                    dependencies.add(new Dependency(dependency));
                }
            }
        }

        public void setSettingValue(String value, SettingChangedEvent.Source source) {
            log.trace("{} updated {}.{} to {}", source.name(), group.name(), setting.key(), value);
            switch (source) {
                //no need to update JavaFX components
                case USER -> {
                    if (Strings.isNullOrEmpty(value)) {
                        manager.remove(group, setting, source);
                    } else {
                        manager.set(group, setting, value, source);
                    }
                }
                //no need to update SettingsManager
                case DEVELOPER, SYSTEM -> settingProperty().setValue(value);
            }
        }

        public int settingOrder() {
            return setting.order();
        }

        public boolean accepts(SettingChangedEvent event) {
            return Objects.equals(group.name(), event.getGroup()) && Objects.equals(setting.key(), event.getKey());
        }

        public boolean belongsTo(SettingsSectionDescriptor section) {
            if (this.section == null) {
                return section == null;
            }

            return Objects.equals(section, this.section);
        }

        public boolean shouldBeShown() {
            if (setting.hidden()) {
                return false;
            }

            //True if empty
            return dependencies.stream().allMatch(Dependency::validate);
        }

        private class Dependency {

            private static final DecimalFormat PRICE_FORMAT = new DecimalFormat("0.00");

            private final String group, key;
            private final Pattern value;

            public Dependency(DependsOn d) {
                group = d.group();
                key = d.key();
                if (Strings.isNullOrEmpty(d.value())) {
                    value = null;
                } else {
                    value = Pattern.compile(d.value());
                }
            }

            public boolean accepts(String string) {
                if (Strings.isNullOrEmpty(string)) {
                    return value == null;
                }

                return value.matcher(string).find();
            }

            public boolean validate() {
                if ("$meta".equals(group)) {
                    BotMetadata meta = bot.getMetadata();
                    if (meta == null) {
                        return false;
                    }

                    String value = switch (key) {
                        case "name" -> meta.getName();
                        case "author" -> meta.getAuthor();
                        case "access" -> meta.getAccess().name();
                        case "version" -> meta.getVersion();
                        case "variant" -> meta.getVariantName();
                        case "price" -> PRICE_FORMAT.format(meta.getPrice());
                        case "trial" -> Boolean.toString(meta.isTrial());
                        case "local" -> Boolean.toString(meta.isLocal());
                        default -> null;
                    };
                    return accepts(value);
                }
                return accepts(manager.get(group, key));
            }
        }
    }
}
