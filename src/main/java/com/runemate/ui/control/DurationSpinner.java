package com.runemate.ui.control;

import com.google.common.base.*;
import java.time.Duration;
import java.util.*;
import javafx.beans.property.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.util.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class DurationSpinner extends Spinner<Duration> {

    private final SimpleObjectProperty<Mode> modeProperty = new SimpleObjectProperty<>(Mode.HOURS);

    public DurationSpinner() {
        setEditable(true);

        final DurationConverter converter = new DurationConverter();
        final DurationFormatter formatter = new DurationFormatter(converter);
        setValueFactory(new DurationSpinnerValueFactory(this));

        final TextField editor = getEditor();
        editor.setTextFormatter(formatter);
        editor.addEventHandler(
            InputEvent.ANY,
            event -> modeProperty.setValue(editor.getCaretPosition() <= editor.getText().indexOf(":") ? Mode.HOURS : Mode.MINUTES)
        );
        editor.setOnScroll(event -> {
            final double dy = event.getDeltaY();
            if (dy != 0 && event.getEventType() == ScrollEvent.SCROLL) {
                if (dy > 0) {
                    getValueFactory().increment(1);
                } else {
                    getValueFactory().decrement(1);
                }
            }
        });

        focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                getValueFactory().setValue(converter.fromString(editor.getText()));
            }
        });

        Tooltip.install(this, new Tooltip("hours:minutes"));
    }

    private Duration increment(Duration time, int steps) {
        return switch (modeProperty.getValue()) {
            case HOURS -> time.plusHours(steps);
            case MINUTES -> time.plusMinutes(steps * 5L);
        };
    }

    private Duration decrement(Duration time, int steps) {
        return increment(time, -steps);
    }

    private void select() {
        final TextField editor = getEditor();
        switch (modeProperty.getValue()) {
            case HOURS -> {
                final int index = editor.getText().indexOf(":");
                editor.selectRange(0, index);
            }
            case MINUTES -> {
                final int index = editor.getText().indexOf(":") + 1;
                editor.selectRange(index, editor.getText().length());
            }
        }
    }

    private enum Mode {
        HOURS,
        MINUTES
    }

    private static class DurationSpinnerValueFactory extends SpinnerValueFactory<Duration> {

        private final DurationSpinner spinner;

        public DurationSpinnerValueFactory(DurationSpinner spinner) {
            this.spinner = spinner;
            setConverter(new DurationConverter());
        }

        @Override
        public void increment(final int steps) {
            setValue(spinner.increment(getValue(), steps));
            spinner.select();
        }

        @Override
        public void decrement(final int steps) {
            final Duration decremented = spinner.decrement(getValue(), steps);
            if (decremented.isNegative()) {
                return;
            }
            setValue(decremented);
            spinner.select();
        }
    }

    private static class DurationFormatter extends TextFormatter<Duration> {

        public DurationFormatter(final StringConverter<Duration> valueConverter) {
            super(valueConverter, Duration.ZERO, c -> c.getControlNewText().matches("[0-9]{0,2}:[0-9]{0,2}") ? c : null);
        }

    }

    private static class DurationConverter extends StringConverter<Duration> {

        @Override
        public String toString(final Duration object) {
            final long minutes = object.toMinutes();
            return String.format("%d:%02d", minutes / 60, minutes % 60);
        }

        @Override
        public Duration fromString(final String string) {
            final String[] split = string.split(":");
            if (split.length != 2 || Arrays.stream(split).anyMatch(Strings::isNullOrEmpty)) {
                return Duration.ZERO;
            }
            return Duration.ofHours(Long.parseLong(split[0])).plusMinutes(Long.parseLong(split[1]));
        }

    }

}
