package com.runemate.ui.control;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.binding.*;
import com.runemate.ui.tracker.*;
import java.awt.image.*;
import java.net.*;
import java.util.*;
import javafx.application.*;
import javafx.beans.binding.*;
import javafx.embed.swing.*;
import javafx.fxml.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import lombok.*;

@InternalAPI
public class ItemControl extends HBox implements Initializable {

    @FXML
    private Label eachValueText;

    @FXML
    private ImageView imageView;

    @FXML
    private Label itemNameText;

    @FXML
    private Label itemQuantityText;

    @FXML
    private Label totalValueText;

    private final DefaultUI parent;

    @Getter
    private final InventoryTracker.ItemTracker tracker;

    public ItemControl(@NonNull DefaultUI parent, @NonNull InventoryTracker.ItemTracker tracker) {
        this.parent = parent;
        this.tracker = tracker;

        FXUtil.loadFxml(this, "/fxml/item_tracker.fxml");
        FXUtil.loadCss(this, "/css/item_tracker.css");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        setPadding(new Insets(4));
        parent.bot().getPlatform().invokeLater(() -> {
            //Create a dummy SpriteItem so we can get the item image
            SpriteItem item = new SpriteItem(tracker.getId(), 1);
            BufferedImage image = item.getImage().get(10);
            if (image != null) {
                Platform.runLater(() -> imageView.setImage(SwingFXUtils.toFXImage(image, null)));
            }
        });

        itemNameText.setText(tracker.getName());
        itemQuantityText.textProperty().bind(new MetricBinding(tracker.getQuantity()));
        eachValueText.setText("Each: " + StringFormat.metricFormat(tracker.getValue()) + "gp");
        totalValueText.textProperty().bind(new MetricBinding(totalValueProperty()).map(text -> "Total: " + text + "gp"));

        managedProperty().bind(visibleProperty());
        visibleProperty().bind(tracker.getShowing());
    }

    public NumberExpression totalValueProperty() {
        return tracker.getQuantity().multiply(tracker.getValue());
    }
}
